import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Mode_Player extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pn, ps, pc;
	private JLabel ln, ls;
	private JButton b1, b3, bDeuxPerso;
	private static boolean IsPlayer, IsDeuxPerso, IsDevPlayer;

	public Mode_Player() {
		setTitle("Mode Joueur");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pn = new JPanel();
		this.add(pn, "North");
		ln = new JLabel(new ImageIcon(getClass().getResource("quiRb.png")));
		pn.add(ln);
		pn.setBackground(new Color(197, 202, 233));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		b1 = new JButton("Nouvelle partie");
		b1.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(b1);
		b1.setBackground(new Color(121, 134, 203));
		b1.setForeground(Color.white);
		b1.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b1.setToolTipText("Cliquez ici pour commencer une partie !!");
		b1.setBounds(280, 110, 320, 40);
		b1.setFocusPainted(false);
		b1.addActionListener(this);

		bDeuxPerso = new JButton("Deux Personnages A Trouver");
		bDeuxPerso.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bDeuxPerso);
		bDeuxPerso.setBackground(new Color(121, 134, 203));
		bDeuxPerso.setForeground(Color.white);
		bDeuxPerso.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bDeuxPerso.setToolTipText("Cliquez ici pour commencer une partie avec deux perso!!");
		bDeuxPerso.setBounds(280, 170, 320, 40);
		bDeuxPerso.setFocusPainted(false);
		bDeuxPerso.addActionListener(this);

		b3 = new JButton("Retour");
		pc.add(b3);
		b3.setFont(new Font("Montserrat", Font.BOLD, 16));
		b3.setBackground(new Color(121, 134, 203));
		b3.setForeground(Color.white);
		b3.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b3.setToolTipText("Retour");

		b3.setBounds(280, 230, 320, 40);
		b3.setFocusPainted(false);
		b3.addActionListener(this);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		ls = new JLabel("@2022");
		ps.add(ls);
		ls.setFont(new Font("Montserrat", Font.BOLD, 12));
		ls.setForeground(Color.white);

	}

	public static void setIsPlayer(boolean b) {
		IsPlayer = b;
	}

	public static boolean getIsPlayer() {
		return IsPlayer;
	}

	public static boolean getIsDevPlayer() {
		return IsDevPlayer;
	}

	public static void setIsDevPlayer(boolean isDevPlayer) {
		IsDevPlayer = isDevPlayer;
	}

	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == b1) {
			if (IsDevPlayer || IsPlayer) {
				this.dispose();
				Niveau n = new Niveau();
				n.show();
			}
		}
		if (e.getSource() == bDeuxPerso) {
			if(ChoixDev.getIsDevBase()) {
			if(Mode_Dev.getNbrColonne()*Mode_Dev.getNbrLigne()>=3) {
			this.dispose();
			IsDeuxPerso = true;
			GameDeuxPerso g = null;
			try {
				g = new GameDeuxPerso();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			g.show();
			}
			else {
				JOptionPane.showMessageDialog(this, "Veuillez selectionner au moins 3 personnages pour continuer !");
			}
			}else {
				this.dispose();
				IsDeuxPerso = true;
				GameDeuxPerso g = null;
				try {
					g = new GameDeuxPerso();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				g.show();
				}
			}
		
		if (e.getSource() == b3) {
			if (getIsDevPlayer()) {
				Mode_Dev md = new Mode_Dev();
				md.setIsDev(true);
				Mode_Player.setIsPlayer(false);
				this.dispose();
				md.show();
			} else {
				this.dispose();
				Accueil a = null;
				try {
					a = new Accueil();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				a.show();
			}
		}

	}

	public static void main(String[] args) {
		Mode_Player mp = new Mode_Player();
		mp.setVisible(true);

	}

}