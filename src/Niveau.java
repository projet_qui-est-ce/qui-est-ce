import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Niveau extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pn, pc, ps;
	private JLabel lImage, ls;
	private JButton bFacile, bDifficile, bretour, bTresFacile,bDeuxPerso;
	private static boolean IsFacile = false, IsDifficile = false, IsTresFacile = false,IsDeuxPerso=false;

	public Niveau() {
		setTitle("Niveau de jeu");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pn = new JPanel();
		this.add(pn, "North");
		lImage = new JLabel(new ImageIcon(getClass().getResource("quiRb.png")));
		pn.add(lImage);
		pn.setBackground(new Color(197, 202, 233));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		bTresFacile = new JButton("Tres Facile");
		bTresFacile.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bTresFacile);
		bTresFacile.setBackground(new Color(121, 134, 203));
		bTresFacile.setForeground(Color.white);
		bTresFacile.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bTresFacile.setToolTipText("Niveau tres facile");
		bTresFacile.setBounds(300, 110, 288, 40);
		bTresFacile.setFocusPainted(false);
		bTresFacile.addActionListener(this);

		bFacile = new JButton("Facile");
		bFacile.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bFacile);
		bFacile.setBackground(new Color(121, 134, 203));
		bFacile.setForeground(Color.white);
		bFacile.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bFacile.setToolTipText("Niveau facile");
		bFacile.setBounds(300, 170, 288, 40);
		bFacile.setFocusPainted(false);
		bFacile.addActionListener(this);

		bDifficile = new JButton("Difficile");
		bDifficile.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bDifficile);
		bDifficile.setBackground(new Color(121, 134, 203));
		bDifficile.setForeground(Color.white);
		bDifficile.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bDifficile.setToolTipText("Niveau difficile");
		bDifficile.setBounds(300, 230, 288, 40);
		bDifficile.setFocusPainted(false);
		bDifficile.addActionListener(this);
		
		bDeuxPerso = new JButton("Deux Personnages a trouver");
        bDeuxPerso.setFont(new Font("Montserrat", Font.BOLD, 16));
        bDeuxPerso.setBackground(new Color(121, 134, 203));
        bDeuxPerso.setForeground(Color.white);
        bDeuxPerso.setCursor(new Cursor(Cursor.HAND_CURSOR));
        bDeuxPerso.setToolTipText("Niveau difficile");
        bDeuxPerso.setBounds(300, 290, 320, 40);
        bDeuxPerso.setFocusPainted(false);
        bDeuxPerso.addActionListener(this);

		bretour = new JButton("Retour");
		pc.add(bretour);
		bretour.setFont(new Font("Montserrat", Font.BOLD, 16));
		bretour.setBackground(new Color(121, 134, 203));
		bretour.setForeground(Color.white);
		bretour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		if (Mode_Player.getIsPlayer()) {
			bretour.setToolTipText("Retour a Mode-player");
		} else if (Mode_Dev.getIsDev()) {
			bretour.setToolTipText("Retour a Mode-dev");
		} else if(ChoixGen.getIsCreer()){
			bretour.setToolTipText("Retour a Generateur");
		}else if(ChoixGen.getIsChoix()) {
			bretour.setToolTipText("Retour a importer/choisir une partie");
		}
		bretour.setBounds(300, 290, 288, 40);
		bretour.setFocusPainted(false);
		bretour.addActionListener(this);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));
		ls = new JLabel("@2022");
		ps.add(ls);
		ls.setFont(new Font("Montserrat", Font.BOLD, 12));
		ls.setForeground(Color.white);
		
		
		if(ChoixDev.getIsGen()) {
            pc.add(bDeuxPerso);
            pc.add(bDeuxPerso);
            bTresFacile.setBounds(300, 65, 320, 40);
            bFacile.setBounds(300, 125, 320, 40);
            bDifficile.setBounds(300, 185, 320, 40);
            bDeuxPerso.setBounds(300, 245, 320, 40);
            bretour.setBounds(300, 305, 320, 40);
        }
	}

	public static void main(String[] args) {
		Niveau n = new Niveau();
		n.setVisible(true);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bTresFacile) {
			if (ChoixDev.getIsGen()) {
				this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Accueil.getJg().setNiveau(1);
							GameGenerateur jeu = new GameGenerateur();
							jeu.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else {
				this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Accueil.getAp().setNiveau(1);
							Game2 jeu = new Game2();
							jeu.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}

			IsTresFacile = true;
			IsFacile=false;
			IsDifficile=false;
			IsDeuxPerso=false;
		}

		if (e.getSource() == bFacile) {
			IsFacile = true;
			IsTresFacile = false;
			IsDifficile=false;
			IsDeuxPerso=false;
			this.dispose();
			if (ChoixDev.getIsGen()) {
				this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Accueil.getJg().setNiveau(2);
							GameGenerateur jeu = new GameGenerateur();
							jeu.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else {
				EventQueue.invokeLater(new Runnable() {

					public void run() {
						try {
							Accueil.getAp().setNiveau(2);
							Game jeu = new Game();
							jeu.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		}

		if (e.getSource() == bDifficile) {
			IsDifficile = true;
			IsFacile = false;
			IsTresFacile = false;
			IsDeuxPerso=false;
			if (ChoixDev.getIsGen()) {
				this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Accueil.getJg().setNiveau(3);
							GameGenerateur jeu = new GameGenerateur();
							jeu.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else {
				this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Accueil.getAp().setNiveau(3);
							Game jeu = new Game();
							jeu.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		}
		if (e.getSource() == bretour){
			if (Mode_Player.getIsPlayer() || Mode_Player.getIsDevPlayer()) {
				this.dispose();
				Mode_Player mp = new Mode_Player();
				mp.show();
			} else if (Mode_Dev.getIsDev()) {
				this.dispose();
				Mode_Dev md = new Mode_Dev();
				md.show();
			} else if(ChoixGen.getIsCreer()){
				this.dispose();
				Generateform gf = new Generateform();
				Generateform.setNbrattr();
				gf.setLnbrAttr();
				gf.show();
			}
			else if(ChoixGen.getIsChoix()) {
				this.dispose();
				ChoixPartie cp = null;
				try {
					cp = new ChoixPartie();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				cp.show();
			}else if(ChoixSave.getIsSaisie()){
			this.dispose();
			Accueil a = null;
			try {
				a = new Accueil();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			Generateform.setNbrattr();
			a.show();
		}
	}
		
		if(e.getSource() == bDeuxPerso) {
            IsDeuxPerso=true;
			IsFacile = false;
			IsTresFacile = false;
			IsDifficile=false;
            this.dispose();
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                    	Accueil.getJg().setNiveau(3);
                        GameGenerateur2perso jeu = new GameGenerateur2perso();
                        jeu.setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

	}

	public static boolean getFacile() {
		return IsFacile;
	}

	public static boolean getDifficile() {
		return IsDifficile;
	}

	public static void setFacile(boolean b) {
		IsFacile = b;
	}

	public static void setDifficile(boolean b) {
		IsDifficile = b;
	}

	public static boolean isIsTresFacile() {
		return IsTresFacile;
	}

	public static void setIsTresFacile(boolean b) {
		IsTresFacile = b;
	}
	public static boolean isIsDeuxPerso() {
        return IsDeuxPerso;
    }

    public static void setIsDeuxPerso(boolean isDeuxPerso) {
        IsDeuxPerso = isDeuxPerso;
    }
}