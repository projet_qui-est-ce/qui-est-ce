import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class Accueil extends JFrame implements ActionListener {
	/** 
	 *  
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pn, ps, pc;
	private JLabel ln, ls;
	private JButton b1, b2, b3, bs, bload;
	private static boolean isClicked = false;
	private static JsonGenerateur jg;
	private static AttributionPersonnage ap;

	public Accueil() throws Exception {
		jg = new JsonGenerateur();
		ap = new AttributionPersonnage();

		setTitle("Qui est-ce ?");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pn = new JPanel();
		this.add(pn, "North");
		ln = new JLabel(new ImageIcon(getClass().getResource("quiRb.png")));
		pn.add(ln);
		pn.setBackground(new Color(197, 202, 233));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		b1 = new JButton("Mode Developpeur");
		b1.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(b1);
		b1.setBackground(new Color(121, 134, 203));
		b1.setForeground(Color.white);
		b1.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b1.setToolTipText("Cliquez ici pour commencer en mode dev !!");
		b1.setBounds(300, 90, 288, 40);
		b1.setFocusPainted(false);
		b1.addActionListener(this);

		b2 = new JButton("Mode Joueur");
		pc.add(b2);
		b2.setFont(new Font("Montserrat", Font.BOLD, 16));
		b2.setBackground(new Color(121, 134, 203));
		b2.setForeground(Color.white);
		b2.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b2.setToolTipText("Cliquez ici pour commencer en mode player !!");
		b2.setBounds(300, 150, 288, 40);
		b2.setFocusPainted(false);
		b2.addActionListener(this);

		bload = new JButton("Continuer");
		pc.add(bload);
		bload.setFont(new Font("Montserrat", Font.BOLD, 16));
		bload.setBackground(new Color(121, 134, 203));
		bload.setForeground(Color.white);
		bload.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bload.setToolTipText("Cliquez ici pour charger une partie deja enregistree");
		bload.setBounds(300, 210, 288, 40);
		bload.setFocusPainted(false);
		bload.addActionListener(this);

		b3 = new JButton("QUITTER");
		pc.add(b3);
		b3.setFont(new Font("Montserrat", Font.BOLD, 16));
		b3.setBackground(new Color(121, 134, 203));
		b3.setForeground(Color.white);
		b3.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b3.setToolTipText("Cliquez ici pour quitter le jeu");
		b3.setBounds(300, 270, 288, 40);
		b3.setFocusPainted(false);
		b3.addActionListener(this);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		ls = new JLabel("@2022");
		ps.add(ls);
		ls.setFont(new Font("Montserrat", Font.BOLD, 12));
		ls.setForeground(Color.white);

		bs = new JButton("A propos ");
		ps.add(bs);
		bs.setFocusPainted(false);
		bs.setContentAreaFilled(false);
		bs.setOpaque(false);
		bs.setBorder(new BorderRadius(3));
		bs.setFont(new Font("Montserrat", Font.BOLD, 10));
		bs.setBackground(new Color(121, 134, 203));
		bs.setForeground(Color.black);
		bs.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bs.setToolTipText("Cliquez ici pour en savoir plus sur ce jeu");
		bs.addActionListener(this);
		
		ChoixDev.setIsGen(false);
		ChoixDev.setIsDevBase(false);
		ChoixSave.setIsPartie(false);
		ChoixSave.setIsSaisie(false);
		ChoixSave.setIsCont(false);
		ChoixGen.setIsChoix(false);
		ChoixGen.setIsCreer(false);
		Niveau.setDifficile(false);
		Niveau.setFacile(false);
		Niveau.setIsTresFacile(false);
		Niveau.setIsDeuxPerso(false);

	}

	public static boolean isClicked() {
		return isClicked;
	}

	public static void setClicked(boolean isClicked) {
		Accueil.isClicked = isClicked;
	}

	public static void main(String[] args) throws Exception {

		Accueil f2 = new Accueil();
		f2.setVisible(true);

	}

	public static JsonGenerateur getJg() {
		return jg;
	}

	public static AttributionPersonnage getAp() {
		return ap;
	}

	private static class BorderRadius implements Border {

		private int radius;

		BorderRadius(int radius) {
			this.radius = radius;
		}

		public Insets getBorderInsets(Component c) {
			return new Insets(this.radius + 1, this.radius + 1, this.radius + 2, this.radius);
		}

		public boolean isBorderOpaque() {
			return true;
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			g.drawRoundRect(x, y, width - 1, height - 1, radius, radius);
		}

	}

	@SuppressWarnings({ "deprecation" })
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == b2) {
			Mode_Player mp = new Mode_Player();
			isClicked = false;
			this.dispose();
			mp.show();
			Mode_Player.setIsPlayer(true);
			Mode_Dev.setIsDev(false);
			Mode_Player.setIsDevPlayer(false);
		}
		if (e.getSource() == b1) {
			ChoixDev md = new ChoixDev();
			isClicked = false;
			this.dispose();
			md.show();
			Mode_Player.setIsPlayer(false);
			Mode_Player.setIsDevPlayer(false);
		}

		if (e.getSource() == bs) {
			JOptionPane.showMessageDialog(this,
					"Devlopped by:\n-Fadia ALLANI\n-Melissa OUADA\n-Ranya KARMANI\n-Saddek OUYAHIA\n\nCONTACT: projet-programmation@gmail.com");
		}

		if (e.getSource() == b3) {
			isClicked = false;
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment quitter!!", "Confirmation",
					JOptionPane.YES_NO_OPTION);

			if (option == 0) { 
				System.exit(0);
			}
		}
		
		if(e.getSource()==bload) {
			isClicked = true;
			this.dispose();
			ChoixSave cs = null;
			try {
				cs = new ChoixSave();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			cs.show();
		}

	}

}