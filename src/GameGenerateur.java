import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.*;
import javax.swing.*;

public class GameGenerateur extends JFrame implements ActionListener, ItemListener {

	private static final long serialVersionUID = 1L;
	private JPanel pc, ps, ps1;
	private JMenuBar mb;
	private JMenu mg, mh;
	private JMenuItem nw, sv, hlp, qt;
	private static JButton quitter;
	private JButton bsValide, bsAjouter1, bsEnlever, bsChoix, bvalide2;
	private JComboBox<String> cLogique;
	private JComboBox<String> cquestion1, cquestion2, cchoix1, cchoix2;
	private String[] ListeChoix;
	private JLabel aide, lnbr, lreponse, lquestion;
	private int nbr = 0;
	private JToggleButton tp[];
	private boolean b1, b2;
	private Partie p;

	private int nbrLigne, nbrColonne;
	private ArrayList<String> choix;

	private static HashMap<String, AtribueG> gamegenerateurPerso = new HashMap<>();
	private static HashMap<String, AtribueG> tquestion1Generateur = new HashMap<>();
	private static HashMap<String, AtribueG> tquestion2Generateur = new HashMap<>();
	private ArrayList<String> question1 = new ArrayList<>();
	private ArrayList<String> question2 = new ArrayList<>();
	private ArrayList<String> difG = new ArrayList<>();
	private static HashMap<String, Personnage> tquestion1 = new HashMap<>();
	private static HashMap<String, Personnage> tquestion2 = new HashMap<>();

	private static AtribueG ag[];
	private static JLabel lag[];
	private HashMap<String, ArrayList<String>> hQuestionGenerateur = new HashMap<>();
	@SuppressWarnings("unused")
	private HashMap<String, ArrayList<String>> hQuestion2Generateur = new HashMap<>();
	private static AtribueG persoGenerateur;

	@SuppressWarnings("unused")
	public GameGenerateur() throws Exception {
		setTitle("Qui est-ce ?");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));
		Accueil.getJg().setPage(1);

		mb = new JMenuBar();
		setJMenuBar(mb);
		mb.setBackground(new Color(197, 202, 233));
		mb.setOpaque(true);

		mg = new JMenu("Game");
		mb.add(mg);
		mg.setFont(new Font("Montserrat", Font.BOLD, 14));
		mg.setForeground(Color.BLACK);

		nw = new JMenuItem("New");
		mg.add(nw);
		nw.setFont(new Font("Montserrat", NORMAL, 14));
		nw.setForeground(Color.BLACK);
		nw.addActionListener(this);

		sv = new JMenuItem("Save");
		mg.add(sv);
		sv.setFont(new Font("Montserrat", NORMAL, 14));
		sv.setForeground(Color.BLACK);
		sv.addActionListener(this);
		mg.addSeparator();

		qt = new JMenuItem("Quit");
		mg.add(qt);
		qt.setFont(new Font("Montserrat", NORMAL, 14));
		qt.setForeground(Color.BLACK);
		qt.addActionListener(this);

		mh = new JMenu("Help?");
		mb.add(mh);
		mh.setFont(new Font("Montserrat", Font.BOLD, 14));
		mh.setForeground(Color.BLACK);

		hlp = new JMenuItem("savoir +");
		mh.add(hlp);
		hlp.setFont(new Font("Montserrat", NORMAL, 14));
		hlp.setForeground(Color.BLACK);
		hlp.addActionListener(this);

		pc = new JPanel();
		this.add(pc, "Center");

		tp = new JToggleButton[20];

		nbrLigne = (int) Accueil.getJg().getLigne();
		nbrColonne = (int) Accueil.getJg().getColonne();

		ag = new AtribueG[20];
		lag = new JLabel[20];

		pc.setLayout(new GridLayout(nbrLigne, nbrColonne, 28, 4));
		pc.setBackground(new Color(232, 234, 246));

		persoGenerateur = Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver());

		if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {

				tp[i] = new JToggleButton(new ImageIcon(Accueil.getJg().getListAG().get(i).getFicher()));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(Accueil.getJg().getListAG().get(i).getNom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				if (!Accueil.getJg().getListAG().get(i).getEstCocher()) {
					gamegenerateurPerso.put(Accueil.getJg().getListAG().get(i).getNom(),
							Accueil.getJg().getListAG().get(i));
				}
				pc.add(tp[i]);
				lag[i] = new JLabel(new ImageIcon(getClass().getResource("croix.png")));
				lag[i].setLocation(tp[i].getLocation());
				tp[i].add(lag[i]);
				lag[i].setVisible(false);

				if (Accueil.getJg().getListAG().get(i).getEstCocher()) {
					tp[i].setSelected(true);
					lag[i].setVisible(true);
				}
				switch ((int) Accueil.getJg().getNiveau()) {
				case 1:
					Niveau.setIsTresFacile(true);
					break;
				case 2:
					Niveau.setFacile(true);
					break;
				case 3:
					Niveau.setDifficile(true);
					break;
				}

			}
		}

		else {
			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {

				ag[i] = Accueil.getJg().getListAG().get(i);
				tp[i] = new JToggleButton(new ImageIcon(ag[i].getFicher()));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(ag[i].getNom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				gamegenerateurPerso.put(ag[i].getNom(), ag[i]);
				pc.add(tp[i]);
				lag[i] = new JLabel(new ImageIcon(getClass().getResource("croix.png")));
				lag[i].setLocation(tp[i].getLocation());
				tp[i].add(lag[i]);
				lag[i].setVisible(false);

			}
		}

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));

		ps1 = new JPanel();
		ps.add(ps1);
		ps1.setPreferredSize(new Dimension(840, 100));
		ps1.setBackground(new Color(232, 234, 246));
		ps1.setBorder(BorderFactory.createRaisedBevelBorder());
		ps1.setLayout(null);

		quitter = new JButton();
		ps1.add(quitter);
		quitter.setVisible(false);
		quitter.addActionListener(this);

		bsAjouter1 = new JButton("Ajouter");
		ps1.add(bsAjouter1);
		bsAjouter1.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsAjouter1.setBackground(new Color(121, 134, 203));
		bsAjouter1.setForeground(Color.white);
		bsAjouter1.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsAjouter1.setToolTipText("Cliquez ici pour ajouter une deuxieme question");
		bsAjouter1.setFocusPainted(false);
		bsAjouter1.addActionListener(this);
		bsAjouter1.setBounds(7, 10, 100, 25);
		bsAjouter1.addActionListener(this);
		bsAjouter1.setVisible(true);

		bsEnlever = new JButton("Enlever");
		ps1.add(bsEnlever);
		bsEnlever.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsEnlever.setBackground(new Color(121, 134, 203));
		bsEnlever.setForeground(Color.white);
		bsEnlever.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsEnlever.setToolTipText("Cliquez ici pour enlever la deuxieme question");
		bsEnlever.setFocusPainted(false);
		bsEnlever.setBounds(7, 65, 100, 25);
		bsEnlever.setVisible(false);
		bsEnlever.addActionListener(this);

		cquestion1 = new JComboBox<String>();
		ps1.add(cquestion1);
		for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
			cquestion1.addItem(Accueil.getJg().getAttribut()[i]);
		}
		cquestion1.setFont(new Font("Montserrat", NORMAL, 15));
		cquestion1.setBackground(Color.white);
		cquestion1.setForeground(Color.black);
		cquestion1.setBounds(200, 10, 100, 25);
		cquestion1.addItemListener(this);
		cquestion1.setVisible(true);

		cquestion2 = new JComboBox<String>();
		ps1.add(cquestion2);
		for (int i = 1; i < Accueil.getJg().getAttribut().length; i++) {
			cquestion2.addItem(Accueil.getJg().getAttribut()[i]);
		}
		cquestion2.setFont(new Font("Montserrat", NORMAL, 15));
		cquestion2.setBackground(Color.white);
		cquestion2.setForeground(Color.black);
		cquestion2.setBounds(200, 65, 100, 25);
		cquestion2.setVisible(false);
		cquestion2.addItemListener(this);

		cchoix1 = new JComboBox<String>();
		ps1.add(cchoix1);
		cchoix1.setBounds(330, 10, 100, 25);
		cchoix1.addActionListener(this);
		cchoix1.setVisible(true);
		cchoix1.setBackground(Color.white);
		cchoix1.setForeground(Color.black);

		String item = (String) cquestion1.getSelectedItem();
		ListeChoix = Accueil.getJg().getAttribut();
		choix = new ArrayList<String>();
		for (int i = 0; i < ListeChoix.length; i++) {
			for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
				if (ListeChoix[i].equals(item)) {
					if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
						choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
						cchoix1.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
					}
					hQuestionGenerateur.put(ListeChoix[i], choix);

				}
			}
		}

		cchoix2 = new JComboBox<String>();
		ps1.add(cchoix2);
		cchoix2.setBounds(330, 65, 100, 25);
		cchoix2.addActionListener(this);
		cchoix2.setVisible(false);
		cchoix2.setBackground(Color.white);
		cchoix2.setForeground(Color.black);

		bsValide = new JButton("Valider");
		ps1.add(bsValide);
		bsValide.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsValide.setBackground(new Color(121, 134, 203));
		bsValide.setForeground(Color.white);
		bsValide.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsValide.setToolTipText("Cliquez ici pour valider la/les question(s) choisie(s)");
		bsValide.setFocusPainted(false);
		bsValide.setBounds(732, 10, 100, 25);
		bsValide.addActionListener(this);
		bsValide.setVisible(true);

		bvalide2 = new JButton("Valider");
		ps1.add(bvalide2);
		bvalide2.setFont(new Font("Montserrat", Font.BOLD, 15));
		bvalide2.setBackground(new Color(121, 134, 203));
		bvalide2.setForeground(Color.white);
		bvalide2.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bvalide2.setToolTipText("Cliquez ici pour valider la/les question(s) choisie(s)");
		bvalide2.setFocusPainted(false);
		bvalide2.setBounds(732, 10, 100, 25);
		bvalide2.addActionListener(this);

		bsChoix = new JButton("Valider choix");
		ps1.add(bsChoix);
		bsChoix.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsChoix.setBackground(new Color(121, 134, 203));
		bsChoix.setForeground(Color.white);
		bsChoix.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsChoix.setToolTipText("Cliquez ici pour valider votre choix de selection de personne");
		bsChoix.setFocusPainted(false);
		bsChoix.setBounds(693, 65, 140, 25);
		bsChoix.addActionListener(this);

		cLogique = new JComboBox<String>();
		ps1.add(cLogique);
		cLogique.setFont(new Font("Montserrat", NORMAL, 16));
		cLogique.setBackground(Color.white);
		cLogique.setForeground(Color.black);
		cLogique.setBounds(138, 40, 60, 20);
		cLogique.addItem("Or");
		cLogique.addItem("And");
		cLogique.setVisible(false);
		cLogique.addItemListener(this);

		lquestion = new JLabel("La reponse est: ");
		ps1.add(lquestion);
		lquestion.setFont(new Font("Montserrat", Font.BOLD, 14));
		lquestion.setBounds(440, 10, 230, 35);
		lquestion.setForeground(Color.black);
		lquestion.setVisible(false);

		lreponse = new JLabel();
		ps1.add(lreponse);
		lreponse.setFont(new Font("Montserrat", Font.BOLD, 15));
		lreponse.setBounds(573, 10, 230, 35);
		lreponse.setForeground(Color.black);
		lreponse.setVisible(false);

		aide = new JLabel("Nombre de case a cocher: ");
		ps1.add(aide);
		aide.setFont(new Font("Montserrat", Font.BOLD, 14));
		aide.setBounds(440, 65, 230, 35);
		aide.setForeground(new Color(215, 215, 215));
		aide.setVisible(false);

		lnbr = new JLabel();
		ps1.add(lnbr);
		lnbr.setFont(new Font("Montserrat", Font.BOLD, 14));
		lnbr.setBounds(650, 65, 230, 35);
		lnbr.setForeground(new Color(215, 215, 215));
		lnbr.setVisible(false);

		if (Niveau.isIsTresFacile()) {
			bsAjouter1.setVisible(false);
			cquestion1.setBounds(200, 40, 100, 25);
			cchoix1.setBounds(330, 40, 100, 25);
			aide.setText("Nombre de case cocher: ");

		}

		p = new Partie();
		if (ChoixGen.getIsCreer()||ChoixSave.isIsCont()) {
			JsonGenerateur jg2 = new JsonGenerateur(Accueil.getJg().getTheme(), "personnages",
					Accueil.getJg().getLigne(), Accueil.getJg().getColonne(), Accueil.getJg().getPage(),
					Accueil.getJg().getnbrPerso(), Accueil.getJg().getPersoATrouver(),
					Accueil.getJg().getPersoATrouver(), Accueil.getJg().getNiveau(), Accueil.getJg().getSave(),
					Accueil.getJg().getSaveSaisie(), Accueil.getJg().getAttribut(), Accueil.getJg().getListAG(),
					Accueil.getJg().getValeursPossible());
	
				p.addToList(new PartieObj(Accueil.getJg().getTheme(), Accueil.getJg().getSave()));
			
		} else if (ChoixGen.getIsChoix()) {
			Random obj = new Random();

			int num = obj.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne() - 1);
			persoGenerateur = Accueil.getJg().getListAG().get(num);
			try {
				Accueil.getJg().setPersoATrouver(num);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		addWindowListener((WindowListener) new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter la partie?",
						"Confirmation", JOptionPane.YES_NO_OPTION);

				if (confirmed == JOptionPane.YES_OPTION) {
					try {
						Accueil.getJg().setSaveSaisie(false);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						Accueil.getJg().setGame(Accueil.getJg().getTheme());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					System.exit(0);
				}
			}
		});
	}

	public int getNbrLigne() {
		return nbrLigne;
	}

	public void setNbrLigne(int nbrLigne) {
		this.nbrLigne = nbrLigne;
	}

	public int getNbrColonne() {
		return nbrColonne;
	}

	public void setNbrColonne(int nbrColonne) {
		this.nbrColonne = nbrColonne;
	}

	public static HashMap<String, AtribueG> getGamegenerateurPerso() {
		return gamegenerateurPerso;
	}

	public static void quitter() {
		quitter.doClick();
	}

	public AtribueG[] getAg() {
		return ag;
	}

	public static void setAg(AtribueG ar, int i) {
		ag[i] = ar;
	}

	public static HashMap<String, AtribueG> getTquestion1Generateur() {
		return tquestion1Generateur;
	}

	public static void setTquestion1Generateur(HashMap<String, AtribueG> tquestion1Generateur) {
		GameGenerateur.tquestion1Generateur = tquestion1Generateur;
	}

	public static HashMap<String, AtribueG> getTquestion2Generateur() {
		return tquestion2Generateur;
	}

	public static void setTquestion2Generateur(HashMap<String, AtribueG> tquestion2Generateur) {
		GameGenerateur.tquestion2Generateur = tquestion2Generateur;
	}

	public static JLabel[] getLag() {
		return lag;
	}

	public static void setLag(int i) {
		GameGenerateur.lag[i].setVisible(false);
	}

	public static AtribueG getPersoGenerateur() {
		return persoGenerateur;
	}

	public static void setPersoGenerateur(AtribueG persoGenerateur) {
		GameGenerateur.persoGenerateur = persoGenerateur;
	}

	public static void main(String[] args) throws Exception {
		GameGenerateur gg = new GameGenerateur();
		gg.setVisible(true);
	}

	@SuppressWarnings({ "deprecation" })
	@Override
	public void actionPerformed(ActionEvent e) {
		for (int i = 0; i < nbrColonne * nbrLigne; i++) {
			if (e.getSource() == tp[i]) {

				if (tp[i].isSelected()) {
					lag[i].setVisible(true);
					lag[i].setLocation(tp[i].getLocation());

					if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

						try {
							Accueil.getJg().getListAG().get(i).setEstCocher(true);
							gamegenerateurPerso.remove(Accueil.getJg().getListAG().get(i).getNom());
							Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}

					} else {
						ag[i].setEstCocher(true);
						gamegenerateurPerso.remove(ag[i].getNom());
						try {
							Accueil.getJg().updateToList(ag[i], i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}

				} else {
					lag[i].setVisible(false);

					if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
						Accueil.getJg().getListAG().get(i).setEstCocher(false);
						gamegenerateurPerso.put(Accueil.getJg().getListAG().get(i).getNom(),
								Accueil.getJg().getListAG().get(i));
						try {
							Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}

					} else {
						ag[i].setEstCocher(false);
						gamegenerateurPerso.put(ag[i].getNom(), ag[i]);
						try {
							Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}

					}

				}

			}
		}

		if (e.getSource() == nw) {
			for (int i = 0; i < Accueil.getJg().getListAG().size(); i++) {
				Accueil.getJg().getListAG().get(i).setEstCocher(false);
				try {
					Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				lag[i].setVisible(false);
			}
			gamegenerateurPerso.clear();
			tquestion1Generateur.clear();
			tquestion2Generateur.clear();

			Random obj = new Random();
			int num = obj.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne() - 1);
			persoGenerateur = Accueil.getJg().getListAG().get(num);
			try {
				Accueil.getJg().setPersoATrouver(num);
				Accueil.getJg().setGame(Accueil.getJg().getTheme());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			this.dispose();
			GameGenerateur jeu = null;
			try {
				jeu = new GameGenerateur();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			jeu.setVisible(true);
		}
		if (e.getSource() == sv) {
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous sauvgarder cette partie ?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) {

				try {
					Accueil.getJg().setSave(true);
					for (int i = 0; i < p.getListpartie().size(); i++) {
						if (p.getListpartie().get(i).getNom().equals(Accueil.getJg().getTheme())) {
							p.updateToList(new PartieObj(Accueil.getJg().getTheme(), true), i);
						}
					}
					Accueil.getJg().setGame(Accueil.getJg().getTheme());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		if (e.getSource() == hlp) {

			JOptionPane.showMessageDialog(this,
					"Le but du jeu :\nIdentifier le personnage choisis aleatoirement par l'ordinateur en procedant par elimination en posant une ou deux questions");

		}
		if (e.getSource() == quitter) {
			Niveau.setFacile(false);
			Niveau.setDifficile(false);
			Niveau.setIsTresFacile(false);
			Mode_Dev.setIsDev(false);
			Mode_Player.setIsPlayer(false);
			Mode_Player.setIsDevPlayer(false);
			ChoixDev.setIsGen(false);
			Accueil.setClicked(false);
			try {
				Accueil.getJg().setSaveSaisie(false);
				this.dispose();
				gamegenerateurPerso.clear();
				tquestion1Generateur.clear();
				tquestion2Generateur.clear();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
			if (e.getSource() == qt) {

				int option = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment quitter la partie?",
						"Confirmation", JOptionPane.YES_NO_OPTION);
				if (option == 0) {
					Niveau.setFacile(false);
					Niveau.setDifficile(false);
					Niveau.setIsTresFacile(false);
					Mode_Dev.setIsDev(false);
					Mode_Player.setIsPlayer(false);
					Mode_Player.setIsDevPlayer(false);
					ChoixDev.setIsGen(false);
					Accueil.setClicked(false);
					try {
						Accueil.getJg().setSaveSaisie(false);

						gamegenerateurPerso.clear();
						tquestion1Generateur.clear();
						tquestion2Generateur.clear();
						this.dispose();
						Accueil a = null;
						Accueil.setClicked(false);
						try {
							a = new Accueil();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						a.show();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
				if (e.getSource() == bsAjouter1) {
					cquestion2.setVisible(true);
					bsEnlever.setVisible(true);
					cLogique.setVisible(true);
					cchoix2.setVisible(true);
					String item1 = (String) cquestion2.getSelectedItem();
					ListeChoix = Accueil.getJg().getAttribut();
					choix = new ArrayList<String>();
					cchoix2.removeAllItems();
					for (int i = 0; i < ListeChoix.length; i++) {
						for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
							if (ListeChoix[i].equals(item1)) {
								if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
									choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
									cchoix2.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
								}

							}
						}
						hQuestionGenerateur.put(ListeChoix[i], choix);
					}
					bsAjouter1.setVisible(false);
					cquestion1.removeAllItems();
					for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
						if (!Accueil.getJg().getAttribut()[i].equals(cquestion2.getSelectedItem())) {
							cquestion1.addItem(Accueil.getJg().getAttribut()[i]);
						}
					}
				}

				if (e.getSource() == bsEnlever) {
					bsEnlever.setVisible(false);
					cLogique.setVisible(false);
					cquestion2.setVisible(false);
					cchoix2.setVisible(false);
					bsAjouter1.setVisible(true);

					cquestion1.removeAllItems();
					for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
						cquestion1.addItem(Accueil.getJg().getAttribut()[i]);
					}
					cchoix1.removeAllItems();
					String item = (String) cquestion1.getSelectedItem();
					ListeChoix = Accueil.getJg().getAttribut();
					choix = new ArrayList<String>();
					choix.clear();
					for (int i = 0; i < ListeChoix.length; i++) {
						for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
							if (ListeChoix[i].equals(item)) {
								if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
									choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
									cchoix1.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
								}
							}

							hQuestionGenerateur.put(ListeChoix[i], choix);
						}
					}
				}

				if (e.getSource() == bsChoix) {
					if (!gamegenerateurPerso.isEmpty()) {
						ValiderGenerateur v = new ValiderGenerateur();
						v.show();
					} else {
						JOptionPane.showMessageDialog(null, "Veuillez ne pas cocher tous les personnages, faut avoir au moins un qui n'est pas coche !");
					}
				}

				if (e.getSource() == bsValide) {
					lquestion.setVisible(true);
					lreponse.setVisible(true);
					aide.setForeground(Color.black);
					lnbr.setForeground(Color.black);
					
					if (Accueil.getJg().getNiveau()==3) {
						aide.setVisible(false);
						lnbr.setVisible(false);
						lquestion.setBounds(440, 65, 230, 35);
						lreponse.setBounds(570, 65, 230, 35);
					}else if (Accueil.getJg().getNiveau()==2) {
						aide.setVisible(true);
						lnbr.setVisible(true);
					}else if (Accueil.getJg().getNiveau()==1) {
						aide.setVisible(true);
						lnbr.setVisible(true);
						bsAjouter1.setVisible(false);
					}
					
					nbr = 0;
					tquestion1.clear();
					tquestion2.clear();
					bsChoix.setVisible(true);
					question1.clear();
					question2.clear();

					if ((cquestion1.isVisible()) && (cquestion2.isVisible())) {
						lquestion.setVisible(true);
						lreponse.setVisible(true);
						aide.setForeground(Color.black);
						lnbr.setForeground(Color.black);

						for (int i = 0; i < ListeChoix.length; i++) {
							if ((((String) cquestion1.getSelectedItem()).equals(ListeChoix[i]))) {
								if (((String) cchoix1.getSelectedItem()).equals(persoGenerateur.getAtr()[i])) {

									b1 = true;
									for (AtribueG k : gamegenerateurPerso.values()) {
										if (!((k.getAtr()[i]).equals(((String) cchoix1.getSelectedItem())))) {
											question1.add(k.getNom());
										}

									}
								} else {
									b1 = false;
									for (AtribueG k : gamegenerateurPerso.values()) {
										if (((k.getAtr()[i]).equals(((String) cchoix1.getSelectedItem())))) {
											question1.add(k.getNom());
										}
									}

								}

							}

						}

						for (int i = 0; i < ListeChoix.length; i++) {
							if ((((String) cquestion2.getSelectedItem()).equals(ListeChoix[i]))) {
								if (((String) cchoix2.getSelectedItem()).equals(persoGenerateur.getAtr()[i])) {

									b2 = true;
									for (AtribueG k : gamegenerateurPerso.values()) {
										if (!((k.getAtr()[i]).equals(((String) cchoix2.getSelectedItem())))) {
											question2.add(k.getNom());
										}

									}
								} else {
									b2 = false;

									for (AtribueG k : gamegenerateurPerso.values()) {

										if (((k.getAtr()[i]).equals(((String) cchoix2.getSelectedItem())))) {
											question2.add(k.getNom());

										}
									}

								}

							}

						}

						if (((String) cLogique.getSelectedItem()).equals("Or")) {

							if (b1 || b2) {
								lreponse.setText("Vrai");
								for (String a : question1) {
									for (String b : question2) {
										if (a.equals(b)) {
											nbr++;
										}
									}
								}
								lnbr.setText((question1.size() + question2.size() - nbr) + "");
							} else {
								lreponse.setText("Faux");
								for (String a : question1) {
									for (String b : question2) {
										if (a.equals(b)) {
											nbr++;
										}
									}
								}
								lnbr.setText((question1.size() + question2.size() - nbr) + "");

							}
						} else {
							if (b1 && b2) {
								lreponse.setText("Vrai");
								for (String a : question1) {
									for (String b : question2) {
										if (a.equals(b)) {
											nbr++;
										}
									}
								}

								lnbr.setText((question1.size() + question2.size() - nbr) + "");

							} else {
								lreponse.setText("Faux");
								if (b1 == false) {
									if (b2) {
										for (String a : question1) {
											for (String b : question2) {
												if (a.equals(b)) {
													nbr++;

												}
											}

										}
										lnbr.setText(nbr + "");
									} else {
										for (String a : question1) {
											for (String b : question2) {
												if (a.equals(b)) {
													nbr++;
												}
											}
										}
										lnbr.setText(nbr + "");
									}

								} else {
									if (b2 == false) {
										for (String c : gamegenerateurPerso.keySet()) {
											if (!(question1.contains(c))) {
												difG.add(c);
											}
										}

										for (String a : question2) {
											for (String b : question1) {
												if (a.equals(b)) {
													nbr++;
												}
											}

										}
										lnbr.setText(nbr + "");
									}

								}
								lnbr.setText(nbr + "");
							}
						}

					}

					else if ((cquestion1.isVisible()) && (!(cquestion2.isVisible()))) {
						nbr = 0;
						int ind = 0;
						difG.clear();

						for (int i = 0; i < ListeChoix.length; i++) {
							nbr = 0;
							ind = 0;
							if (((String) cquestion1.getSelectedItem()).equals(ListeChoix[i])) {

								nbr = 0;
								ind = 0;

								for (int j = 0; j < persoGenerateur.getAtr().length; j++) {

									ind = 0;
									if (((String) cchoix1.getSelectedItem()).equals(persoGenerateur.getAtr()[i])) {
										lreponse.setText("Vrai");
										nbr = 0;
										ind = 0;
										for (AtribueG k : gamegenerateurPerso.values()) {
											for (int m = 0; m < ListeChoix.length; m++) {
												if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
													if (k.getNom()
															.equals(Accueil.getJg().getListAG().get(m).getNom())) {
														ind = m;
													}
												} else {
													if (k.getNom().equals(ag[m].getNom())) {
														ind = m;
													}

												}
											}

											if (!((k.getAtr()[i]).equals(((String) cchoix1.getSelectedItem())))) {
												nbr++;
												if (Niveau.isIsTresFacile()) {
													tp[ind].setSelected(true);
													difG.add(k.getNom());
													lag[ind].setVisible(true);

													if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
														Accueil.getJg().getListAG().get(ind).setEstCocher(true);
														try {
															Accueil.getJg().updateToList(
																	Accueil.getJg().getListAG().get(ind), ind);
														} catch (Exception e1) {
															e1.printStackTrace();
														}
														try {
															Accueil.getJg().setGame(Accueil.getJg().getTheme());
														} catch (Exception e2) {
															e2.printStackTrace();
														}
													} else {
														ag[ind].setEstCocher(true);
														try {
															Accueil.getJg().updateToList(ag[ind], ind);
														} catch (Exception e1) {
															e1.printStackTrace();
														}
														try {
															Accueil.getJg().setGame(Accueil.getJg().getTheme());
														} catch (Exception e2) {
															e2.printStackTrace();
														}
													}
												}
											}

											ind++;
										}
										if (Niveau.isIsTresFacile()) {
											for (String m : difG) {
												gamegenerateurPerso.remove(m);
											}
											lnbr.setText(difG.size() + "");
											break;
										} else {
											lnbr.setText(nbr + "");
										}
										difG.clear();

									} else {
										lreponse.setText("Faux");
										nbr = 0;
										ind = 0;
										difG.clear();

										for (AtribueG k : gamegenerateurPerso.values()) {
											for (int m = 0; m < ListeChoix.length; m++) {
												if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

													if (k.getNom()
															.equals(Accueil.getJg().getListAG().get(m).getNom())) {
														ind = m;
													}

												} else {
													if (k.getNom().equals(ag[m].getNom())) {
														ind = m;
													}

												}

											}

											if (((k.getAtr()[i]).equals((String) cchoix1.getSelectedItem()))) {
												nbr++;
												if (Niveau.isIsTresFacile()) {
													tp[ind].setSelected(true);
													difG.add(k.getNom());
													lag[ind].setVisible(true);

													if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
														Accueil.getJg().getListAG().get(ind).setEstCocher(true);
														try {
															Accueil.getJg().updateToList(
																	Accueil.getJg().getListAG().get(ind), ind);
														} catch (Exception e1) {
															e1.printStackTrace();
														}
														try {
															Accueil.getJg().setGame(Accueil.getJg().getTheme());
														} catch (Exception e2) {
															e2.printStackTrace();
														}
													} else {
														ag[ind].setEstCocher(true);
														try {
															Accueil.getJg().updateToList(ag[ind], ind);
														} catch (Exception e1) {
															e1.printStackTrace();
														}
														try {
															Accueil.getJg().setGame(Accueil.getJg().getTheme());
														} catch (Exception e2) {
															e2.printStackTrace();
														}
													}
												}

											}
											ind++;
										}
										if (Niveau.isIsTresFacile()) {
											for (String m : difG) {
												gamegenerateurPerso.remove(m);
											}
											lnbr.setText(difG.size() + "");
											break;
										} else {
											lnbr.setText(nbr + "");
										}

									}
								}

							}

						}
					}
				}
			}
		//}

	//}

	@Override
	public void itemStateChanged(ItemEvent e) {
		int cq;
		if ((e.getSource() == cquestion1 && cquestion1.isPopupVisible())) {
			cq = cquestion2.getSelectedIndex();
			cquestion2.removeAllItems();
			for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
				if (!Accueil.getJg().getAttribut()[i].equals(cquestion1.getSelectedItem())) {
					cquestion2.addItem(Accueil.getJg().getAttribut()[i]);
				}
			}
			cquestion2.setSelectedIndex(cq);
			cchoix1.removeAllItems();
			String item1 = (String) cquestion2.getSelectedItem();
			ListeChoix = Accueil.getJg().getAttribut();
			choix = new ArrayList<String>();
			choix.clear();
			cchoix2.removeAllItems();
			for (int i = 0; i < ListeChoix.length; i++) {
				for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
					if (ListeChoix[i].equals(item1)) {
						if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
							choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
							cchoix2.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
						}
						hQuestionGenerateur.put(ListeChoix[i], choix);

					}
				}

			}
			String item = (String) cquestion1.getSelectedItem();
			ListeChoix = Accueil.getJg().getAttribut();
			choix = new ArrayList<String>();
			for (int i = 0; i < ListeChoix.length; i++) {
				for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
					if (ListeChoix[i].equals(item)) {
						if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
							choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
							cchoix1.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
						}
						hQuestionGenerateur.put(ListeChoix[i], choix);

					}
				}

			}
		}

		if ((e.getSource() == cquestion2 && cquestion2.isPopupVisible())) {
			cq = cquestion1.getSelectedIndex();
			cquestion1.removeAllItems();
			for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
				if (!Accueil.getJg().getAttribut()[i].equals(cquestion2.getSelectedItem())) {
					cquestion1.addItem(Accueil.getJg().getAttribut()[i]);
				}
			}
			cquestion1.setSelectedIndex(cq);
			cchoix2.removeAllItems();
			String item1 = (String) cquestion2.getSelectedItem();
			ListeChoix = Accueil.getJg().getAttribut();
			choix = new ArrayList<String>();
			choix.clear();
			for (int i = 0; i < ListeChoix.length; i++) {
				for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
					if (ListeChoix[i].equals(item1)) {
						if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
							choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
							cchoix2.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
						}
						hQuestionGenerateur.put(ListeChoix[i], choix);

					}
				}

			}
			cchoix1.removeAllItems();

			String item = (String) cquestion1.getSelectedItem();
			ListeChoix = Accueil.getJg().getAttribut();
			choix = new ArrayList<String>();
			choix.clear();
			for (int i = 0; i < ListeChoix.length; i++) {
				for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
					if (ListeChoix[i].equals(item)) {
						if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
							choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
							cchoix1.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
						}
						hQuestionGenerateur.put(ListeChoix[i], choix);
					}
				}

			}

		}
	}
}