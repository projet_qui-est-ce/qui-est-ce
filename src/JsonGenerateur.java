import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;

public class JsonGenerateur {

	private String images,Theme;
	private long ligne, colonne, Niveau, Page, PersoATrouver,PersoATrouver1,nbrPerso;
	private boolean save,saveSaisie;
	private ArrayList<AtribueG> listAG;
	private JSONParser jsp;
	private JSONObject se, pos, tabl;
	private JSONArray possibilites, attribut, valeursPossible;

	public JsonGenerateur() throws Exception {

		jsp = new JSONParser();
		se = (JSONObject) jsp.parse(new FileReader("./PersonnageGenerateur.json"));
		images = (String) se.get("images");
		Theme= (String) se.get("Theme");
		ligne = (long) se.get("ligne");
		colonne = (long) se.get("colonne");
		save = (Boolean) se.get("save");
		nbrPerso = (long) se.get("nbrPerso");
		saveSaisie = (Boolean) se.get("saveSaisie");
		Page = (long) se.get("Page");
		PersoATrouver = (long) se.get("PersoATrouver");
		PersoATrouver1 = (long) se.get("PersoATrouver1");
		Niveau = (long) se.get("Niveau");
		attribut = (JSONArray) se.get("attribut");
		possibilites = (JSONArray) se.get("possibilites");
		valeursPossible = (JSONArray) se.get("valeursPossible");
		listAG = new ArrayList<AtribueG>();

		for (int i = 0; i < possibilites.size(); i++) {
			pos = (JSONObject) possibilites.get(i);
			String[] table = new String[attribut.size()];
			for (int j = 0; j < table.length; j++) {
				table[j] = (String) pos.get(attribut.get(j));
			}
			listAG.add(new AtribueG((String) pos.get("nom"), (String) pos.get("ficher"), (Boolean) pos.get("EstCocher"),
					table));
		}
	}
	@SuppressWarnings("unchecked")
	public JsonGenerateur(String Theme,String imgpath,long ligne,long colonne,long page,long nbrPerso,long personageAtrouver,long personageAtrouver1,long niveau,boolean save,boolean saveSaisie,String[] atrbut,ArrayList<AtribueG> listAg,HashMap<String, ArrayList<String>> valeurpos) throws Exception {
		se=new JSONObject();
		se.put("Theme", Theme);
		se.put("images", imgpath);
		se.put("ligne", ligne);
		se.put("colonne", colonne);
		se.put("Page", page);
		se.put("nbrPerso",nbrPerso);
		se.put("PersoATrouver", personageAtrouver);
		se.put("PersoATrouver1", personageAtrouver1);
		se.put("Niveau", niveau);
		se.put("save", save);
		se.put("saveSaisie",saveSaisie);
		attribut=new JSONArray();
		for (String string : atrbut) {
			attribut.add(string);
		}
		se.put("attribut", attribut);
		possibilites=new JSONArray();
		listAG=new ArrayList<>();
		for (AtribueG ag : listAg) {
			tabl = new JSONObject();
			listAG.add(ag);
			switch (attribut.size()) {
				case 1:
					tabl.put("nom", ag.getNom());
					tabl.put("ficher", ag.getFicher());
					tabl.put("EstCocher", ag.getEstCocher());
					tabl.put(attribut.get(0), ag.getAtr()[0]);
					break;
				case 2:
					tabl.put("nom", ag.getNom());
					tabl.put("ficher", ag.getFicher());
					tabl.put("EstCocher", ag.getEstCocher());
					tabl.put(attribut.get(0), ag.getAtr()[0]);
					tabl.put(attribut.get(1), ag.getAtr()[1]);
					break;
				case 3:
					tabl.put("nom", ag.getNom());
					tabl.put("ficher", ag.getFicher());
					tabl.put("EstCocher", ag.getEstCocher());
					tabl.put(attribut.get(0), ag.getAtr()[0]);
					tabl.put(attribut.get(1), ag.getAtr()[1]);
					tabl.put(attribut.get(2), ag.getAtr()[2]);
					break;
				case 4:
					tabl.put("nom", ag.getNom());
					tabl.put("ficher", ag.getFicher());
					tabl.put("EstCocher", ag.getEstCocher());
					tabl.put(attribut.get(0), ag.getAtr()[0]);
					tabl.put(attribut.get(1), ag.getAtr()[1]);
					tabl.put(attribut.get(2), ag.getAtr()[2]);
					tabl.put(attribut.get(3), ag.getAtr()[3]);
					break;
				case 5:
					tabl.put("nom", ag.getNom());
					tabl.put("ficher", ag.getFicher());
					tabl.put("EstCocher", ag.getEstCocher());
					tabl.put(attribut.get(0), ag.getAtr()[0]);
					tabl.put(attribut.get(1), ag.getAtr()[1]);
					tabl.put(attribut.get(2), ag.getAtr()[2]);
					tabl.put(attribut.get(3), ag.getAtr()[3]);
					tabl.put(attribut.get(4), ag.getAtr()[4]);
					break;
			}
			possibilites.add(possibilites.size(), tabl);
		}
		se.put("possibilites", possibilites);
		valeursPossible=new JSONArray();
		for (int i = 0; i < attribut.size(); i++) {
			JSONArray ar1 = new JSONArray();
			for (int j = 0; j < valeurpos.get(attribut.get(i)).size(); j++) {
				ar1.add(valeurpos.get(attribut.get(i)).get(j));
			}
			valeursPossible.add(ar1);
		}
		se.put("valeursPossible", valeursPossible);
		write(Theme);
	}
	public void getGame(String path) throws Exception {
		se = (JSONObject) jsp.parse(new FileReader(path+".json"));
		images = (String) se.get("images");
		ligne = (long) se.get("ligne");
		colonne = (long) se.get("colonne");
		save = (Boolean) se.get("save");
		nbrPerso = (long) se.get("nbrPerso");
		saveSaisie = (Boolean) se.get("saveSaisie");
		Page = (long) se.get("Page");
		PersoATrouver = (long) se.get("PersoATrouver");
		PersoATrouver1 = (long) se.get("PersoATrouver1");
		Niveau = (long) se.get("Niveau");
		attribut = (JSONArray) se.get("attribut");
		possibilites = (JSONArray) se.get("possibilites");
		valeursPossible = (JSONArray) se.get("valeursPossible");
		listAG = new ArrayList<AtribueG>();
		for (int i = 0; i < possibilites.size(); i++) {
			pos = (JSONObject) possibilites.get(i);
			String[] table = new String[attribut.size()];
			for (int j = 0; j < table.length; j++) {
				table[j] = (String) pos.get(attribut.get(j));
			}
			listAG.add(new AtribueG((String) pos.get("nom"), (String) pos.get("ficher"), (Boolean) pos.get("EstCocher"),table));
		}
		write();
	}
	public void setGame(String Theme) throws Exception {
//		se = (JSONObject) jsp.parse(new FileReader("./PersonnageGenerateur.json"));
		write(Theme);
	}
	
	private void write(String path) throws Exception {
		try (FileWriter fw = new FileWriter(path+".json")) {
			String json = se.toJSONString(), towrite = "", tab = "";
			int j = 0;
			for (int i = 0; i < json.length(); i++) {
				if (json.charAt(i) == '{' || json.charAt(i) == '[') {
					tab += "\t";
					towrite += json.substring(j, i + 1) + "\n" + tab;
					j = i + 1;
				}
				if (json.charAt(i) == ',') {
					towrite += json.substring(j, i + 1) + "\n" + tab;
					j = i + 1;
				}
				if (json.charAt(i) == '}' || json.charAt(i) == ']') {
					tab = tab.substring(1, tab.length());
					towrite += json.substring(j, i) + "\n" + tab;
					j = i;
				}
			}
			towrite += json.charAt(json.length() - 1);
			fw.write(towrite);
			fw.flush();
		}
	}
	private void write() throws Exception {
		try (FileWriter fw = new FileWriter("./PersonnageGenerateur.json")) {
			String json = se.toJSONString(), towrite = "", tab = "";
			int j = 0;
			for (int i = 0; i < json.length(); i++) {
				if (json.charAt(i) == '{' || json.charAt(i) == '[') {
					tab += "\t";
					towrite += json.substring(j, i + 1) + "\n" + tab;
					j = i + 1;
				}
				if (json.charAt(i) == ',') {
					towrite += json.substring(j, i + 1) + "\n" + tab;
					j = i + 1;
				}
				if (json.charAt(i) == '}' || json.charAt(i) == ']') {
					tab = tab.substring(1, tab.length());
					towrite += json.substring(j, i) + "\n" + tab;
					j = i;
				}
			}
			towrite += json.charAt(json.length() - 1);
			fw.write(towrite);
			fw.flush();
		}
	}
	
	public String getImages() {
		return images;
	}

	@SuppressWarnings("unchecked")
	public void setImages(String images) throws Exception {
		this.images = images;
		se.put("images", images);
		write();
	}

	public long getnbrPerso() {
		return nbrPerso;
	}

	@SuppressWarnings("unchecked")
	public void setnbrPerso(long nbrPerso) throws Exception {
		nbrPerso = nbrPerso;
		se.put("nbrPerso", nbrPerso);
		write();
	}
	
	public long getPage() {
		return Page;
	}

	@SuppressWarnings("unchecked")
	public void setPage(long page) throws Exception {
		Page = page;
		se.put("Page", page);
		write();
	}

	public long getLigne() {
		return ligne;
	}
	
	public String getTheme() {
		return Theme;
	}
	@SuppressWarnings("unchecked")
	public void setTheme(String theme) throws Exception {
		Theme = theme;
		se.put("Theme", theme);
		write();
	}
	
	@SuppressWarnings("unchecked")
	public void setLigne(long l) throws Exception {
		ligne = l;
		se.put("ligne", ligne);
		write();
	}

	public long getColonne() {
		return colonne;
	}

	public boolean getSave() {
		return save;
	}

	@SuppressWarnings("unchecked")
	public void setSave(boolean s) throws Exception {
		this.save = s;
		se.put("save", s);
		write();
	}
	
	public boolean getSaveSaisie() {
		return saveSaisie;
	}

	@SuppressWarnings("unchecked")
	public void setSaveSaisie(boolean s) throws Exception {
		this.saveSaisie = s;
		se.put("saveSaisie", s);
		write();
	}

	public long getNiveau() {
		return Niveau;
	}

	@SuppressWarnings("unchecked")
	public void setNiveau(long niveau) throws Exception {
		Niveau = niveau;
		se.put("Niveau", Niveau);
		write();
	}

	public long getPersoATrouver() {
		return PersoATrouver;
	}

	@SuppressWarnings("unchecked")
	public void setPersoATrouver(long perso) throws Exception {
		PersoATrouver = perso;
		se.put("PersoATrouver", PersoATrouver);
		write();
	}

	public long getPersoATrouver1() {
		return PersoATrouver1;
	}

	@SuppressWarnings("unchecked")
	public void setPersoATrouver1(long perso) throws Exception {
		PersoATrouver1 = perso;
		se.put("PersoATrouver1", PersoATrouver1);
		write();
	}
	
	@SuppressWarnings("unchecked")
	public void setColonne(long colonne) throws Exception {
		this.colonne = colonne;
		se.put("colonne", colonne);
		write();
	}

	public HashMap<String, ArrayList<String>> getValeursPossible() {

		HashMap<String, ArrayList<String>> hAttrVal = new HashMap<>();
		
		for (int i = 0; i < attribut.size(); i++) {
			ArrayList<String> list = new ArrayList<String>();
			JSONArray Jarray = (JSONArray) valeursPossible.get(i);
			for (int j = 0; j < Jarray.size(); j++) {
				list.add((String) Jarray.get(j));
			}
			hAttrVal.put((String) attribut.get(i), list);
		}
		return hAttrVal;
	}

	@SuppressWarnings("unchecked")
	public void RemplirValeursP(HashMap<String, ArrayList<String>> hVal) throws Exception {
		for (int i = 0; i < attribut.size(); i++) {
			JSONArray ar1 = new JSONArray();
			for (int j = 0; j < hVal.get(attribut.get(i)).size(); j++) {
				ar1.add(hVal.get(attribut.get(i)).get(j));
			}
			valeursPossible.add(ar1);
		}
	}

	public ArrayList<AtribueG> getListAG() {
		return listAG;
	}

	public String[] getAttribut() {
		String atr[] = new String[attribut.size()];
		for (int i = 0; i < atr.length; i++) {
			atr[i] = (String) attribut.get(i);
		}
		return atr;
	}

	@SuppressWarnings("unchecked")
	public void addAttribut(String atr) throws Exception {
		attribut.add(atr);
		write();
	}

	@SuppressWarnings("unchecked")
	public void addToList(AtribueG ag) throws Exception {
		tabl = new JSONObject();
		listAG.add(ag);
		switch (attribut.size()) {
		case 1:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			break;
		case 2:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			break;
		case 3:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			tabl.put(attribut.get(2), ag.getAtr()[2]);
			break;
		case 4:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			tabl.put(attribut.get(2), ag.getAtr()[2]);
			tabl.put(attribut.get(3), ag.getAtr()[3]);
			break;
		case 5:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			tabl.put(attribut.get(2), ag.getAtr()[2]);
			tabl.put(attribut.get(3), ag.getAtr()[3]);
			tabl.put(attribut.get(4), ag.getAtr()[4]);
			break;
		}
		possibilites.add(possibilites.size(), tabl);
		write();
	}

	@SuppressWarnings("unchecked")
	public void updateToList(AtribueG ag, int i) throws Exception {
		tabl = new JSONObject();
		listAG.set(i, ag);
		switch (attribut.size()) {
		case 1:
			tabl.put("nom", ag.getNom());// il modifie le tab
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			break;
		case 2:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			break;
		case 3:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			tabl.put(attribut.get(2), ag.getAtr()[2]);
			break;
		case 4:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			tabl.put(attribut.get(2), ag.getAtr()[2]);
			tabl.put(attribut.get(3), ag.getAtr()[3]);
			break;
		case 5:
			tabl.put("nom", ag.getNom());
			tabl.put("ficher", ag.getFicher());
			tabl.put("EstCocher", ag.getEstCocher());
			tabl.put(attribut.get(0), ag.getAtr()[0]);
			tabl.put(attribut.get(1), ag.getAtr()[1]);
			tabl.put(attribut.get(2), ag.getAtr()[2]);
			tabl.put(attribut.get(3), ag.getAtr()[3]);
			tabl.put(attribut.get(4), ag.getAtr()[4]);
			break;
		}
		possibilites.set(i, tabl);// elle modifie le json si on revient en arriere par exemple>>
		write();
	}

	public void clearList() throws Exception {
		possibilites.clear();
		attribut.clear();
		valeursPossible.clear();
		listAG.clear();
		write();
	}

}
