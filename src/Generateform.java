import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;

public class Generateform extends JFrame implements ActionListener, ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pc, ps, pc1;
	private JLabel lc1, lc2, lc3, tc, lattr, Lval, LTheme;
	private JLabel[] nbrVal;
	private JLabel lnbrAttr;
	private static JComboBox<Integer> c1;
	private static JComboBox<Integer> c2;
	private JButton bRetour, bValider, bRemplirVal, bplus, bmoins;
	private static JTextField Tabattributs[];
	private JLabel Attributs[];
	private JTextField TabValeurs[], TTheme;
	private JButton bValiderVal[];
	private static int nbrattr = 1;
	private int dim;
	private HashMap<String, ArrayList<String>> hValeurs = new HashMap<>();

	public Generateform() {

		setTitle("Generateur");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		lc1 = new JLabel("Nombre de lignes: ");
		pc.add(lc1);
		lc1.setBounds(60, 38, 230, 20);
		lc1.setFont(new Font("Montserrat", Font.BOLD, 16));
		lc1.setForeground(new Color(121, 134, 203));

		c1 = new JComboBox<Integer>();
		pc.add(c1);
		c1.setFont(new Font("Montserrat", NORMAL, 17));
		c1.setBounds(260, 38, 80, 20);
		c1.addItem(1);
		c1.addItem(2);
		c1.addItem(3);
		c1.addItem(4);
		c1.addItem(5);
		c1.setSelectedItem(5);
		c1.addItemListener(this);

		lc2 = new JLabel("Nombre de colonnes: ");
		pc.add(lc2);
		lc2.setBounds(60, 88, 230, 20);
		lc2.setFont(new Font("Montserrat", Font.BOLD, 16));
		lc2.setForeground(new Color(121, 134, 203));

		c2 = new JComboBox<Integer>();
		pc.add(c2);
		c2.setFont(new Font("Montserrat", NORMAL, 17));
		c2.setBounds(260, 88, 80, 20);
		c2.addItem(1);
		c2.addItem(2);
		c2.addItem(3);
		c2.addItem(4);
		c2.setSelectedItem(4);
		c2.addItemListener(this);

		lc3 = new JLabel("Nombre de personnages:  ");
		pc.add(lc3);
		lc3.setFont(new Font("Montserrat", Font.BOLD, 17));
		lc3.setForeground(new Color(121, 134, 203));
		lc3.setBounds(490, 60, 280, 25);

		tc = new JLabel();
		pc.add(tc);
		tc.setText("20");
		tc.setHorizontalAlignment(JTextField.CENTER);
		tc.setBackground(new Color(232, 234, 246));
		tc.setFont(new Font("Montserrat", Font.BOLD, 18));
		tc.setForeground(Color.black);
		tc.setBounds(725, 60, 40, 25);

		LTheme = new JLabel("Theme: ");
		pc.add(LTheme);
		LTheme.setFont(new Font("Montserrat", Font.BOLD, 17));
		LTheme.setForeground(new Color(121, 134, 203));
		LTheme.setBounds(60, 135, 100, 25);

		TTheme = new JTextField();
		pc.add(TTheme);
		TTheme.setFont(new Font("Montserrat", Font.BOLD, 13));
		TTheme.setForeground(Color.black);
		TTheme.setCursor(new Cursor(Cursor.HAND_CURSOR));
		TTheme.setToolTipText("Veuillez saisir le theme de votre partie");
		TTheme.setBounds(260, 135, 155, 27);

		DocumentFilter f = new UppercaseJTextField();
		AbstractDocument doc = (AbstractDocument) TTheme.getDocument();
		doc.setDocumentFilter(f);

		pc1 = new JPanel();
		pc.add(pc1);
		pc1.setBackground(new Color(232, 234, 246));
		pc1.setBounds(130, 200, 630, 300);
		pc1.setBorder(BorderFactory.createRaisedBevelBorder());
		pc1.setLayout(null);

		lattr = new JLabel("Attributs");
		pc1.add(lattr);
		lattr.setForeground(new Color(121, 134, 203));
		lattr.setFont(new Font("Montserrat", Font.BOLD, 18));
		lattr.setBounds(275, 7, 100, 25);

		bmoins = new JButton("-");
		pc1.add(bmoins);
		bmoins.setHorizontalAlignment(JTextField.CENTER);
		setBackground(new Color(215, 215, 215));
		bmoins.setFont(new Font("Montserrat", Font.BOLD, 18));
		bmoins.setForeground(Color.white);
		bmoins.setBounds(230, 40, 50, 25);
		bmoins.setEnabled(false);
		bmoins.setFocusPainted(false);
		bmoins.addActionListener(this);

		lnbrAttr = new JLabel(nbrattr + "");
		pc1.add(lnbrAttr);
		lnbrAttr.setHorizontalAlignment(JTextField.CENTER);
		lnbrAttr.setBackground(new Color(232, 234, 246));
		lnbrAttr.setFont(new Font("Montserrat", Font.BOLD, 15));
		lnbrAttr.setForeground(Color.black);
		lnbrAttr.setBounds(290, 40, 40, 25);

		bplus = new JButton("+");
		pc1.add(bplus);
		bplus.setHorizontalAlignment(JTextField.CENTER);
		bplus.setBackground(new Color(121, 134, 203));
		bplus.setFont(new Font("Montserrat", Font.BOLD, 15));
		bplus.setForeground(Color.white);
		bplus.setBounds(350, 40, 50, 25);
		bplus.setFocusPainted(false);
		bplus.addActionListener(this);

		Tabattributs = new JTextField[5];

		Tabattributs[0] = new JTextField();
		pc1.add(Tabattributs[0]);
		Tabattributs[0].setBounds(230, 90, 170, 25);
		dim = 90;

		for (int i = 1; i < 5; i++) {
			dim = dim + 40;
			Tabattributs[i] = new JTextField();
			pc1.add(Tabattributs[i]);
			Tabattributs[i].setBounds(230, dim, 170, 25);
			Tabattributs[i].setVisible(false);
		}

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));
		bRetour = new JButton("<< Retour");
		ps.add(bRetour);
		bRetour.setFont(new Font("Montserrat", Font.BOLD, 15));
		bRetour.setBackground(new Color(121, 134, 203));
		bRetour.setForeground(Color.white);
		bRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bRetour.setToolTipText("Cliquez ici pour se rendre au menu");
		bRetour.setBounds(16, 360, 120, 30);
		bRetour.setFocusPainted(false);
		bRetour.addActionListener(this);

		bRemplirVal = new JButton("Remplir les valeurs");
		ps.add(bRemplirVal);
		bRemplirVal.setFont(new Font("Montserrat", Font.BOLD, 15));
		bRemplirVal.setBackground(new Color(121, 134, 203));
		bRemplirVal.setForeground(Color.white);
		bRemplirVal.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bRemplirVal.setToolTipText("Cliquez ici pour remplir les valeurs possibles des attributs");
		bRemplirVal.setBounds(750, 360, 180, 30);
		bRemplirVal.setFocusPainted(false);
		bRemplirVal.addActionListener(this);

		bValider = new JButton("Valider >>");
		ps.add(bValider);
		bValider.setFont(new Font("Montserrat", Font.BOLD, 15));
		bValider.setBackground(new Color(121, 134, 203));
		bValider.setForeground(Color.white);
		bValider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bValider.setToolTipText("Cliquez ici pour remplir les valeurs des attributs pour chaque personnage");
		bValider.setBounds(750, 360, 120, 30);
		bValider.setFocusPainted(false);
		bValider.setVisible(false);
		bValider.addActionListener(this);

	}

	class UppercaseJTextField extends DocumentFilter {
		@Override
		public void insertString(DocumentFilter.FilterBypass fb, int offset, String text, AttributeSet attr)
				throws BadLocationException {
			if (fb.getDocument().getLength() == 0) {
				StringBuilder sb = new StringBuilder(text);
				sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
				text = sb.toString().replaceAll(" ", "_");
			}
			fb.insertString(offset, text, attr);
		}

		@Override
		public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
				throws BadLocationException {
			StringBuilder sb = new StringBuilder(text);
			if (fb.getDocument().getLength() == 0) {
				sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
			}
			text = sb.toString().replaceAll(" ", "_");
			fb.replace(offset, length, text, attrs);
		}
	}

	public static int getNbrLigne() {
		return ((Integer) c1.getSelectedItem());
	}

	public static int getNbrColonne() {
		return ((Integer) c2.getSelectedItem());
	}

	public static int getNbrattr() {
		return nbrattr;
	}

	public static void setNbrattr() {
		nbrattr = 1;
	}

	public static String getJTextFieldText(int i) {
		return (Tabattributs[i].getText());
	}

	public void setLnbrAttr() {
		lnbrAttr.setText(1 + "");
	}

	public String getTTheme() {
		return TTheme.getText();
	}

	public static void main(String[] args) {
		Generateform md = new Generateform();
		md.setVisible(true);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == bRetour) {

			if (bValider.isVisible()) {
				Lval.setVisible(false);
				lattr.setText("Attributs");
				lattr.setBounds(275, 7, 100, 25);
				lnbrAttr.setVisible(true);
				bmoins.setVisible(true);
				bplus.setVisible(true);

				for (int i = 0; i < nbrattr; i++) {
					Tabattributs[i].setVisible(true);
					Attributs[i].setVisible(false);
					TabValeurs[i].setVisible(false);
					bValiderVal[i].setVisible(false);
					nbrVal[i].setText("");
					nbrVal[i].setVisible(false);
				}

				bRetour.setText("<<Retour");
				bValider.setVisible(false);
				bRemplirVal.setVisible(true);

			} else if (bRemplirVal.isVisible()) {
				Tabattributs[0].setText("");
				for (int i = 1; i < 5; i++) {
					Tabattributs[i].setText("");
					Tabattributs[i].setVisible(false);
				}
				nbrattr = 1;
				lnbrAttr.setText(nbrattr + "");
				TTheme.setText("");
				this.dispose();
				ChoixGen a = new ChoixGen();
				a.show();
			}
		}

		if (e.getSource() == bRemplirVal) {

			for (int i = 0; i < nbrattr; i++) {
				ArrayList<String> ListValeur = new ArrayList<>();
				hValeurs.put(Tabattributs[i].getText(), ListValeur);
			}
			boolean passe = true;
			for (int i = 0; i < nbrattr; i++) {
				if (Tabattributs[i].getText().equals("")) {
					passe = false;
					break;
				}
			}

			if (passe) {

				lattr.setText("Valeurs des attributs");
				lattr.setBounds(215, 7, 240, 25);
				lnbrAttr.setVisible(false);
				bmoins.setVisible(false);
				bplus.setVisible(false);
				for (int i = 0; i < nbrattr; i++) {
					Tabattributs[i].setVisible(false);
				}

				bRemplirVal.setVisible(false);
				bValider.setVisible(true);
				bRetour.setText("Retour");
				bRetour.setToolTipText("Cliquez ici pour revenir a la saisie des attributs");

				Attributs = new JLabel[5];
				TabValeurs = new JTextField[5];
				bValiderVal = new JButton[5];
				nbrVal = new JLabel[5];
				Lval = new JLabel("Nombre de valeurs");
				pc1.add(Lval);
				Lval.setForeground(new Color(121, 134, 203));
				Lval.setFont(new Font("Montserrat", Font.BOLD, 10));
				Lval.setBounds(493, 45, 150, 25);

				dim = 75;

				for (int i = 0; i < nbrattr; i++) {

					Attributs[i] = new JLabel(Tabattributs[i].getText());
					TabValeurs[i] = new JTextField();
					bValiderVal[i] = new JButton("ajouter");
					nbrVal[i] = new JLabel();

					pc1.add(TabValeurs[i]);
					pc1.add(Attributs[i]);
					pc1.add(bValiderVal[i]);
					pc1.add(nbrVal[i]);

					Attributs[i].setBounds(110, dim, 170, 25);
					TabValeurs[i].setBounds(220, dim, 170, 25);
					bValiderVal[i].setBounds(405, dim, 80, 25);
					nbrVal[i].setBounds(540, dim, 75, 25);

					bValiderVal[i].setFocusPainted(false);
					bValiderVal[i].setContentAreaFilled(false);
					bValiderVal[i].setOpaque(false);
					bValiderVal[i].setFont(new Font("Montserrat", Font.BOLD, 10));
					bValiderVal[i].setBackground(new Color(121, 134, 203));
					bValiderVal[i].setForeground(Color.black);
					bValiderVal[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
					bValiderVal[i].setToolTipText("Enregistrer la valeur saisie");
					bValiderVal[i].addActionListener(this);

					dim = dim + 40;
				}
			} else {
				JFrame JFrame = null;
				JOptionPane.showMessageDialog(JFrame, "Veuillez remplir le(s) champ(s) disponible !!");
			}
		}

		for (int i = 0; i < nbrattr; i++) {
			if (bValider.isVisible() && e.getSource() == bValiderVal[i]) {
				if (TabValeurs[i].getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Aucune valeur saisie !!");
				} else {
					try {
						if (hValeurs.get(Attributs[i].getText()).contains(TabValeurs[i].getText())) {
							JOptionPane.showMessageDialog(null, "Cette valeur existe deja !!");
						} else {
							hValeurs.get(Attributs[i].getText()).add(TabValeurs[i].getText());
							nbrVal[i].setText(hValeurs.get(Attributs[i].getText()).size() + "");
							TabValeurs[i].setText("");
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}

		if (e.getSource() == bValider) {
			Partie p = null;
			try {
				p = new Partie();
			} catch (Exception e2) {
				e2.printStackTrace();
			}

			for (int i = 0; i < nbrattr; i++) {
				if (TTheme.getText().equals("")) {

					JOptionPane.showMessageDialog(null, "Veuillez saisir le theme de votre partie pour valider !");
					break;

				} else if (hValeurs.get(Attributs[i].getText()).isEmpty()) {

					JOptionPane.showMessageDialog(null, "Veuillez saisir au moins une valeur pour chaque attribut !");
					break;
				} else {
					boolean trouve = false;
					for (int j = 0; j < p.getListpartie().size(); j++) {
						if (p.getListpartie().get(j).getNom().equals(TTheme.getText())||TTheme.getText().equals("Partie_de_base")) {
							trouve = true;
							break;
						}
					}

					if (trouve) {
						JOptionPane.showMessageDialog(null,
								"Veuillez saisir un autre Theme celui-ci est deja utiliser(vous pouvez ajouter juste un chiffre *)");
					} else {
						this.dispose();
						try {
							Accueil.getJg().clearList();

							for (int j = 0; j < nbrattr; j++) {
								Accueil.getJg().addAttribut(Tabattributs[j].getText());
							}
							Accueil.getJg().setColonne((getNbrColonne()));
							Accueil.getJg().setLigne(getNbrLigne());
							Accueil.getJg().RemplirValeursP(hValeurs);
							Accueil.getJg().setSave(false);
							Accueil.getJg().setSaveSaisie(false);
							Accueil.getJg().setTheme(TTheme.getText());
							PersonnageFormTest pf = new PersonnageFormTest();
							pf.show();
							break;
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		}

		if (e.getSource() == bplus)

		{
			bmoins.setEnabled(true);
			bmoins.setBackground(new Color(121, 134, 203));
			Tabattributs[nbrattr].setVisible(true);
			nbrattr++;
			lnbrAttr.setText(nbrattr + "");
			if (nbrattr == 5) {
				bplus.setBackground(new Color(215, 215, 215));
				bplus.setEnabled(false);
			}
		}

		if (e.getSource() == bmoins) {
			bplus.setEnabled(true);
			bplus.setBackground(new Color(121, 134, 203));
			nbrattr--;
			lnbrAttr.setText(nbrattr + "");
			Tabattributs[nbrattr].setVisible(false);
			Tabattributs[nbrattr].setText("");

			if (nbrattr == 1) {
				bmoins.setBackground(new Color(215, 215, 215));
				bmoins.setEnabled(false);
			}
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == c1 || e.getSource() == c2) {
			int ligne = (int) c1.getSelectedItem(), colone = (int) c2.getSelectedItem();
			tc.setText(((ligne) * (colone)) + "");
			if ((int) c1.getSelectedItem() == 1) {
				c2.removeItem(1);
			} else {
				if (!(c2.getItemAt(0) == 1)) {
					c2.insertItemAt(1, 0);
				}
			}
			if ((int) c2.getSelectedItem() == 1) {
				c1.removeItem(1);
			} else {
				if (!(c1.getItemAt(0) == 1)) {
					c1.insertItemAt(1, 0);
				}
			}
		}
	}

}