import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ChoixGen extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pn, pc, ps;
	private JLabel ln, ls;
	private JButton bcreer,bimporter, bretour;
	private static boolean IsCreer,IsChoix;

	public ChoixGen() {

		setTitle("Mode Devloppeur");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pn = new JPanel();
		this.add(pn, "North");
		ln = new JLabel(new ImageIcon(getClass().getResource("quiRb.png")));
		pn.add(ln);
		pn.setBackground(new Color(197, 202, 233));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		bcreer = new JButton("Generer une partie");
		bcreer.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bcreer);
		bcreer.setBackground(new Color(121, 134, 203));
		bcreer.setForeground(Color.white);
		bcreer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bcreer.setToolTipText("Generer un nouveau fichier json");
		bcreer.setBounds(300, 110, 288, 40);
		bcreer.setFocusPainted(false);
		bcreer.addActionListener(this);

		bimporter = new JButton("Importer/Choisir une partie");
		bimporter.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bimporter);
		bimporter.setBackground(new Color(121, 134, 203));
		bimporter.setForeground(Color.white);
		bimporter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bimporter.setToolTipText("Importer votre propre partie");
		bimporter.setBounds(300, 170, 288, 40);
		bimporter.setFocusPainted(false);
		bimporter.addActionListener(this);

		bretour = new JButton("Retour");
		bretour.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bretour);
		bretour.setBackground(new Color(121, 134, 203));
		bretour.setForeground(Color.white);
		bretour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bretour.setToolTipText("Retour a Mode dev");
		bretour.setBounds(300, 230, 288, 40);
		bretour.setFocusPainted(false);
		bretour.addActionListener(this);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		ls = new JLabel("@2022");
		ps.add(ls);
		ls.setFont(new Font("Montserrat", Font.BOLD, 12));
		ls.setForeground(Color.white);

	}

	public static void setIsCreer(boolean b) {
		IsCreer = b;
	}

	public static boolean getIsCreer() {
		return IsCreer;
	}

	public static void setIsChoix(boolean b) {
		IsChoix = b;
	}

	public static boolean getIsChoix() {
		return IsChoix;
	}
	
	public static void main(String[] args) {
		ChoixGen cd = new ChoixGen();
		cd.setVisible(true);
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == bcreer) {
			IsCreer=true;
			IsChoix=false;
			Generateform g = new Generateform();
			g.setNbrattr();
			g.setLnbrAttr();
			this.dispose();
			g.show();
		}

		if (e.getSource() == bimporter) {
			IsChoix=true;
			IsCreer=false;
			this.dispose();
			ChoixPartie gf = null;
			try {
				gf = new ChoixPartie();
				gf.show();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}

		if (e.getSource() == bretour) {
			IsChoix=false;
			IsCreer=false;
			this.dispose();
			ChoixDev cd= null;
			try {
				cd = new ChoixDev();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			cd.show();
		}

	}

}
