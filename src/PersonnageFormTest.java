import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class PersonnageFormTest extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JFrame JFrame;

	private JPanel pc, pc1, ps;
	private JLabel lval, lAttribut[], limage, lprenom;
	private JButton bValider, bSuivant, bParcourir, bRetour, bRetourG;
	private int j, indiceMax;
	private static JTextField textNom;
	private static JComboBox<String> TValeur[];
	private int hauteur = 130, result = 5;
	private static AtribueG persoGenerateur, persoGenerateur1;
	private HashMap<String, ArrayList<String>> hAttrVal = new HashMap<>();
	private String path;
	private Partie p;

	@SuppressWarnings({ "static-access", "unchecked" })
	public PersonnageFormTest() throws Exception {
		setTitle("Formulaire Personnages");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		pc1 = new JPanel();
		pc.add(pc1);
		pc1.setBackground(new Color(232, 234, 246));
		pc1.setBounds(35, 50, 820, 450);
		pc1.setBorder(BorderFactory.createRaisedBevelBorder());
		pc1.setLayout(null);

		if (ChoixSave.getIsSaisie()) {
			j = ((int) Accueil.getJg().getnbrPerso());
			indiceMax = ((int) Accueil.getJg().getnbrPerso());
		} else {
			j = 1;
			indiceMax = 1;
		}

		lval = new JLabel("Personnage numero  " + j);
		pc1.add(lval);
		lval.setForeground(new Color(121, 134, 180));
		lval.setFont(new Font("Montserrat", Font.BOLD, 19));
		lval.setBounds(300, 8, 300, 25);

		lprenom = new JLabel("Nom");
		pc1.add(lprenom);
		lprenom.setBounds(60, 85, 180, 20);
		lprenom.setFont(new Font("Montserrat", Font.BOLD, 16));
		lprenom.setForeground(new Color(121, 134, 203));

		textNom = new JTextField();
		textNom.setBounds(220, 85, 180, 20);
		pc1.add(textNom);
		textNom.setToolTipText("Inserez le nom de votre personnage");

		lAttribut = new JLabel[Accueil.getJg().getAttribut().length];
		TValeur = new JComboBox[Accueil.getJg().getAttribut().length];

		for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {

			lAttribut[i] = new JLabel(Accueil.getJg().getAttribut()[i]);
			pc1.add(lAttribut[i]);
			lAttribut[i].setBounds(60, hauteur, 180, 20);
			lAttribut[i].setFont(new Font("Montserrat", Font.BOLD, 17));
			lAttribut[i].setForeground(new Color(121, 134, 203));
			TValeur[i] = new JComboBox<String>();
			TValeur[i].setBounds(220, hauteur, 180, 20);
			pc1.add(TValeur[i]);
			hAttrVal = Accueil.getJg().getValeursPossible();
			for (int j = 0; j < hAttrVal.get(lAttribut[i].getText()).size(); j++) {
				TValeur[i].addItem(hAttrVal.get(lAttribut[i].getText()).get(j));
			}
			hauteur += 45;
		}

		limage = new JLabel();
		limage.setBounds(10, 10, 365, 290);
		pc1.add(limage);
		limage.setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		limage.setBounds(600, 30, 180, 180);
		limage.setVisible(false);

		bParcourir = new JButton(new ImageIcon(getClass().getResource("ajouter-image-96.png")));
		pc1.add(bParcourir);
		bParcourir.setOpaque(false);
		bParcourir.setContentAreaFilled(false);
		bParcourir.setFocusPainted(false);
		bParcourir.setBorderPainted(false);
		bParcourir.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bParcourir.setToolTipText("Cliquez ici pour inserer une image de votre personnage");
		bParcourir.setBounds(665, 330, 96, 96);
		bParcourir.addActionListener(this);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		bRetourG = new JButton("<< Retour au Generateur");
		ps.add(bRetourG);
		bRetourG.setFont(new Font("Montserrat", Font.BOLD, 15));
		bRetourG.setBackground(new Color(121, 134, 203));
		bRetourG.setForeground(Color.white);
		bRetourG.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bRetourG.setToolTipText("Cliquez ici pour revenir au generateur");
		bRetourG.setBounds(20, 400, 240, 30);
		bRetourG.setFocusPainted(false);
		bRetourG.addActionListener(this);
		bRetourG.setVisible(true);

		bRetour = new JButton("<< Retour");
		ps.add(bRetour);
		bRetour.setFont(new Font("Montserrat", Font.BOLD, 15));
		bRetour.setBackground(new Color(121, 134, 203));
		bRetour.setForeground(Color.white);
		bRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bRetour.setToolTipText("Cliquez ici pour revenir au personnage precedent");
		bRetour.setBounds(20, 400, 150, 30);
		bRetour.setFocusPainted(false);
		bRetour.addActionListener(this);
		bRetour.setVisible(false);

		bSuivant = new JButton("Suivant >>");
		ps.add(bSuivant);
		bSuivant.setFont(new Font("Montserrat", Font.BOLD, 15));
		bSuivant.setBackground(new Color(121, 134, 203));
		bSuivant.setForeground(Color.white);
		bSuivant.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bSuivant.setToolTipText("Cliquez ici pour passer au prochain personnage");
		bSuivant.setBounds(650, 400, 150, 30);
		bSuivant.setFocusPainted(false);
		bSuivant.addActionListener(this);

		bValider = new JButton("Valider");
		ps.add(bValider);
		bValider.setFont(new Font("Montserrat", Font.BOLD, 15));
		bValider.setBackground(new Color(121, 134, 203));
		bValider.setForeground(Color.white);
		bValider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bValider.setToolTipText("Cliquez ici pour valider vos personnages");
		bValider.setBounds(650, 400, 150, 30);
		bValider.setFocusPainted(false);
		bValider.addActionListener(this);
		bValider.setVisible(false);

		addWindowListener((WindowListener) new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(null, "Voulez-vous sauvegarder la saisie ?",
						"Confirmation", JOptionPane.YES_NO_OPTION);

				if (confirmed == JOptionPane.YES_OPTION) {
					ChoixDev.setIsGen(false);
					ChoixGen.setIsCreer(false);
					ChoixGen.setIsChoix(false);
					try {
						Accueil.getJg().setnbrPerso((int) getJ());
					} catch (Exception e2) {
						e2.printStackTrace();
					}

					if ((!textNom.getText().equals("")) && path != null) {
						try {
							Accueil.getJg().setnbrPerso((int) getJ()+1);
						} catch (Exception e2) {
							e2.printStackTrace();
						}
						String[] tab = new String[Generateform.getNbrattr()];
						for (int j = 0; j < Generateform.getNbrattr(); j++) {
							tab[j] = (String) TValeur[j].getSelectedItem();
						}
						try {
							Accueil.getJg().addToList(new AtribueG(textNom.getText(), path, false, tab));
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}

					try {
						Accueil.getJg().setSaveSaisie(true);
						Accueil.getJg().setGame(Accueil.getJg().getTheme());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		if (ChoixSave.getIsSaisie()) {
			if (j != 0) {
				if (j < (Accueil.getJg().getLigne() * Accueil.getJg().getColonne()) && j > 1) {
					bRetourG.setVisible(false);
					bRetour.setVisible(true);
					bSuivant.setVisible(true);
				} else if (j == (Accueil.getJg().getLigne() * Accueil.getJg().getColonne())) {
					bRetourG.setVisible(false);
					bSuivant.setVisible(false);
					bRetour.setVisible(true);
					bValider.setVisible(true);
				} else if (j == 1) {
					bRetourG.setVisible(true);
					bRetour.setVisible(false);
					bValider.setVisible(false);
					bSuivant.setVisible(true);
				}
			}
		}

	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public static AtribueG getPersoGenerateur() {
		return persoGenerateur;
	}

	@SuppressWarnings({ "unused", "deprecation" })
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == bSuivant) {

			for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {

				if (textNom.getText().equals("") || path == null) {
					JOptionPane.showMessageDialog(JFrame, "Veuillez saisir le nom et inserer une image!!");
					break;
				}

				else {
					lval.setText("personnage numero " + (j + 1) + "");
					try {
						Accueil.getJg().setnbrPerso(j);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						String[] tab = new String[Accueil.getJg().getAttribut().length];
						for (int j = 0; j < Accueil.getJg().getAttribut().length; j++) {
							tab[j] = (String) TValeur[j].getSelectedItem();
						}

						if (j < indiceMax) {
							Accueil.getJg().updateToList(new AtribueG(textNom.getText(), path, false, tab), j - 1);
						} else {
							Accueil.getJg().addToList(new AtribueG(textNom.getText(), path, false, tab));
							indiceMax++;
						}
						setJ(j + 1);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					textNom.setText("");
					for (int j = 0; j < Accueil.getJg().getAttribut().length; j++) {
						bRetour.setVisible(true);
						bRetourG.setVisible(false);
						limage.setVisible(false);
					}
					if (j < indiceMax) {
						for (int k = 0; k < Accueil.getJg().getAttribut().length; k++) {
							TValeur[k].setSelectedItem(Accueil.getJg().getListAG().get(j - 1).getAtr()[k]);
							;
						}
						textNom.setText(Accueil.getJg().getListAG().get(j - 1).getNom());
						path = Accueil.getJg().getListAG().get(j - 1).getFicher();
						limage.setIcon(RedimentionImage(Accueil.getJg().getListAG().get(j - 1).getFicher()));
						limage.setVisible(true);
					} else {
						path = null;
					}
					break;
				}
			}
		}
		if (e.getSource() == bValider) {
			boolean b = false;
			try {
			} catch (Exception e2) {
				e2.printStackTrace();
			}

			for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
				if (textNom.getText().equals("") || path == null) {
					b = false;
					JOptionPane.showMessageDialog(JFrame, "Veuillez completer toutes les cases et inserer une image!!");
					break;
				} else {
					b = true;
					try {
						String[] tab = new String[Accueil.getJg().getAttribut().length];
						for (int j = 0; j < Accueil.getJg().getAttribut().length; j++) {
							tab[j] = (String) TValeur[j].getSelectedItem();
						}
						Accueil.getJg().addToList(new AtribueG(textNom.getText(), path, false, tab));
						break;

					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
			if (b) {
				Random obj = new Random();
				Random obj1 = new Random();
				int num = obj.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne());
				int num1 = obj1.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne());

				persoGenerateur = Accueil.getJg().getListAG().get(num);
				persoGenerateur1 = Accueil.getJg().getListAG().get(num1);
				while (persoGenerateur.getNom().equals(persoGenerateur1.getNom())) {
					num1 = obj1.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne());
					persoGenerateur1 = Accueil.getJg().getListAG().get(num1);
				}
				try {
					Accueil.getJg().setPersoATrouver(num);
					Accueil.getJg().setPersoATrouver1((long) num1);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				this.dispose();
				Niveau n = new Niveau();
				n.show();

			}

		}
		if (Accueil.getJg().getLigne() * Accueil.getJg().getColonne() == this.getJ()) {
			bSuivant.setVisible(false);
			bValider.setVisible(true);
		}

		if (e.getSource() == bRetour) {
			setJ(j - 1);
			if (j == 1) {
				bRetour.setVisible(false);
				bRetourG.setVisible(true);
			}
			textNom.setText(Accueil.getJg().getListAG().get(j - 1).getNom());
			limage.setVisible(true);
			limage.setIcon(RedimentionImage(Accueil.getJg().getListAG().get(j - 1).getFicher()));
			path = Accueil.getJg().getListAG().get(j - 1).getFicher();
			String[] AttributsP = null;
			AttributsP = Accueil.getJg().getListAG().get(j - 1).getAtr();
			for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
				TValeur[i].setSelectedItem(AttributsP[i]);
			}
			lval.setText("personnage numero " + j);
			if (bValider.isVisible()) {
				bValider.setVisible(false);
				bSuivant.setVisible(true);
			}
		}

		if (e.getSource() == bRetourG) {
			if(ChoixGen.getIsCreer()) {
			this.dispose();
			Generateform gf = new Generateform();
			Generateform.setNbrattr();
			gf.setLnbrAttr();
			try {
				Accueil.getJg().clearList();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			gf.show();
			}else if(ChoixSave.getIsSaisie()) {
				bRetourG.setText("<< Retour a l'accueil");
				bRetourG.setToolTipText("Cliquez ici pour revenir a l'accueil");
				this.dispose();
				Accueil a = null;
				try {
					a = new Accueil();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				a.show();
			}
		}

		if (e.getSource() == bParcourir) {
			JFileChooser file = new JFileChooser();
			file.setCurrentDirectory(new File(System.getProperty("user.home")));

			FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
			file.addChoosableFileFilter(filter);
			result = file.showSaveDialog(null);

			if (result == JFileChooser.APPROVE_OPTION) {
				File selectedFile = file.getSelectedFile();
				path = selectedFile.getAbsolutePath();
				limage.setVisible(true);
				limage.setIcon(RedimentionImage(path));
			}
		}
	}

	public ImageIcon RedimentionImage(String ImagePath) {
		ImageIcon MyImage = new ImageIcon(ImagePath);
		Image img = MyImage.getImage();
		Image newImg = img.getScaledInstance(limage.getWidth(), limage.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(newImg);
		return image;
	}

	public static void main(String[] args) throws Exception {

		PersonnageFormTest f2 = new PersonnageFormTest();
		f2.setVisible(true);

	}
}
