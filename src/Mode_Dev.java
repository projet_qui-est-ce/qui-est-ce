import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Mode_Dev extends JFrame implements ActionListener, ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pn, pc, ps;
	private JLabel ln, lc1, lc2, lc3, ls;
	private static JComboBox<Integer> c1;
	private static JComboBox<Integer> c2;
	private JButton b1, b2;
	private JLabel tc;
	private static boolean IsDev;

	public Mode_Dev() {

		setTitle("Mode Dev");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pn = new JPanel();
		this.add(pn, "North");
		ln = new JLabel(new ImageIcon(getClass().getResource("quiRb.png")));
		pn.add(ln);
		pn.setBackground(new Color(197, 202, 233));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		lc1 = new JLabel("Nombre de lignes: ");
		pc.add(lc1);
		lc1.setBounds(300, 60, 270, 20);
		lc1.setFont(new Font("Montserrat", Font.BOLD, 16));
		lc1.setForeground(new Color(121, 134, 203));

		c1 = new JComboBox<Integer>();
		pc.add(c1);
		c1.setFont(new Font("Montserrat", NORMAL, 17));
		c1.setBounds(505, 60, 80, 20);
		c1.addItem(1);
		c1.addItem(2);
		c1.addItem(3);
		c1.addItem(4);
		c1.addItem(5);
		c1.setSelectedItem(5);
		c1.addItemListener(this);

		lc2 = new JLabel("Nombre de colonnes: ");
		pc.add(lc2);
		lc2.setBounds(300, 120, 270, 20);
		lc2.setFont(new Font("Montserrat", Font.BOLD, 16));
		lc2.setForeground(new Color(121, 134, 203));

		c2 = new JComboBox<Integer>();
		pc.add(c2);
		c2.setFont(new Font("Montserrat", NORMAL, 17));
		c2.setBounds(505, 120, 80, 20);
		c2.addItem(1);
		c2.addItem(2);
		c2.addItem(3);
		c2.addItem(4);
		c2.setSelectedItem(4);
		c2.addItemListener(this);

		lc3 = new JLabel("Nombre de personnages:  ");
		pc.add(lc3);
		lc3.setFont(new Font("Montserrat", Font.BOLD, 18));
		lc3.setForeground(new Color(121, 134, 203));
		lc3.setBounds(300, 220, 400, 40);

		tc = new JLabel();
		pc.add(tc);
		tc.setText("20");
		tc.setHorizontalAlignment(JTextField.CENTER);
		tc.setBackground(new Color(232, 234, 246));
		tc.setFont(new Font("Montserrat", Font.BOLD, 18));
		tc.setForeground(Color.black);
		tc.setBounds(560, 200, 40, 80);

		b1 = new JButton("<< Retour");
		pc.add(b1);
		b1.setFont(new Font("Montserrat", Font.BOLD, 15));
		b1.setBackground(new Color(121, 134, 203));
		b1.setForeground(Color.white);
		b1.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b1.setToolTipText("Cliquez ici pour se rendre au menu");
		b1.setBounds(16, 360, 130, 30);
		b1.setFocusPainted(false);
		b1.addActionListener(this);

		b2 = new JButton("Valider >>");
		pc.add(b2);
		b2.setFont(new Font("Montserrat", Font.BOLD, 15));
		b2.setBackground(new Color(121, 134, 203));
		b2.setForeground(Color.white);
		b2.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b2.setToolTipText("Cliquez ici pour commencer la partie");
		b2.setBounds(740, 360, 130, 30);
		b2.setFocusPainted(false);
		b2.addActionListener(this);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		ls = new JLabel("@2022");
		ps.add(ls);
		ls.setFont(new Font("Montserrat", Font.BOLD, 12));
		ls.setForeground(Color.white);

	}

	public static void setIsDev(boolean b) {
		IsDev = b;
	}

	public static boolean getIsDev() {
		return IsDev;
	}

	public static int getNbrLigne() {
		return ((Integer) c1.getSelectedItem());
	}

	public static int getNbrColonne() {
		return ((Integer) c2.getSelectedItem());
	}

	public static void main(String[] args) {
		Mode_Dev md = new Mode_Dev();
		md.setVisible(true);

	}

	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == b1) {
			this.dispose();
			ChoixDev a = new ChoixDev();
			a.show();
		}

		if (e.getSource() == b2) {
			this.dispose();
			Mode_Player mp = new Mode_Player();
			mp.setIsDevPlayer(true);
			mp.show();
		}

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == c1 || e.getSource() == c2) {
			int ligne = (int) c1.getSelectedItem(), colone = (int) c2.getSelectedItem();
			tc.setText(((ligne) * (colone)) + "");
			if ((int) c1.getSelectedItem() == 1) {
				c2.removeItem(1);
			} else {
				if (!(c2.getItemAt(0) == 1)) {
					c2.insertItemAt(1, 0);
				}
			}
			if ((int) c2.getSelectedItem() == 1) {
				c1.removeItem(1);
			} else {
				if (!(c1.getItemAt(0) == 1)) {
					c1.insertItemAt(1, 0);
				}
			}
		}
	}
}