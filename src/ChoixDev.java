import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ChoixDev extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pn, pc, ps;
	private JLabel ln, ls;
	private JButton bbase, bgenerateur, bretour;
	private static boolean IsGen=false,IsDevBase=false;

	public ChoixDev() {

		setTitle("Mode Devloppeur");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pn = new JPanel();
		this.add(pn, "North");
		ln = new JLabel(new ImageIcon(getClass().getResource("quiRb.png")));
		pn.add(ln);
		pn.setBackground(new Color(197, 202, 233));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		bbase = new JButton("Jeu de base");
		bbase.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bbase);
		bbase.setBackground(new Color(121, 134, 203));
		bbase.setForeground(Color.white);
		bbase.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bbase.setToolTipText("Mode dev du jeu de base");
		bbase.setBounds(300, 110, 288, 40);
		bbase.setFocusPainted(false);
		bbase.addActionListener(this);

		bgenerateur = new JButton("Generateur");
		bgenerateur.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bgenerateur);
		bgenerateur.setBackground(new Color(121, 134, 203));
		bgenerateur.setForeground(Color.white);
		bgenerateur.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bgenerateur.setToolTipText("Mode developpeur du generateur");
		bgenerateur.setBounds(300, 170, 288, 40);
		bgenerateur.setFocusPainted(false);
		bgenerateur.addActionListener(this);

		bretour = new JButton("Retour");
		bretour.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bretour);
		bretour.setBackground(new Color(121, 134, 203));
		bretour.setForeground(Color.white);
		bretour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bretour.setToolTipText("Retour a l'accueil");
		bretour.setBounds(300, 230, 288, 40);
		bretour.setFocusPainted(false);
		bretour.addActionListener(this);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		ls = new JLabel("@2022");
		ps.add(ls);
		ls.setFont(new Font("Montserrat", Font.BOLD, 12));
		ls.setForeground(Color.white);

	}

	public static void setIsGen(boolean b) {
		IsGen = b;
	}

	public static boolean getIsDevBase() {
		return IsDevBase;
	}
	public static void setIsDevBase(boolean b) {
		IsDevBase = b;
	}

	public static boolean getIsGen() {
		return IsGen;
	}

	public static void main(String[] args) {
		ChoixDev cd = new ChoixDev();
		cd.setVisible(true);
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == bbase) {
			Mode_Dev md = new Mode_Dev();
			md.setIsDev(true);
			Mode_Player.setIsPlayer(false);
			Mode_Player.setIsDevPlayer(false);
			this.dispose();
			md.show();
			IsGen = false;
			IsDevBase=true;
		}

		if (e.getSource() == bgenerateur) {
			this.dispose();
			Mode_Player.setIsPlayer(false);
			Mode_Dev.setIsDev(false);
			Mode_Player.setIsDevPlayer(false);
			ChoixGen cg = new ChoixGen();
			cg.show();
			IsGen = true;
			IsDevBase=false;
		}

		if (e.getSource() == bretour) {
			this.dispose();
			IsGen = false;
			IsDevBase=false;
			Accueil a = null;
			try {
				a = new Accueil();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			a.show();
		}

	}

}