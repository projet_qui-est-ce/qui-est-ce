import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.*;

public class Valider2 extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HashMap<String, Personnage> h = new HashMap<>();

	private JPanel pc, ps, ps1;
	private JLabel lreponse, lperso, lphoto;
	private JButton p[], bcancel, bquitter, bquitter2;
	private Personnage pe[];
	private JMenuBar mb;
	private JMenu mg, mh;
	private JMenuItem qt, hlp;
	private int i = 0, grid, nbrLigne = 0, nbrColonne = 0;
	private String reponse;

	public Valider2() {
		setTitle("Valider choix");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		mb = new JMenuBar();
		setJMenuBar(mb);
		mb.setBackground(new Color(197, 202, 233));
		mb.setOpaque(true);
		mb.setOpaque(true);

		mg = new JMenu("Game");
		mb.add(mg);
		mg.setFont(new Font("Montserrat", Font.BOLD, 14));
		mg.setForeground(Color.BLACK);

		mg.addSeparator();

		qt = new JMenuItem("Quit");
		mg.add(qt);
		qt.setFont(new Font("Montserrat", NORMAL, 14));
		qt.setForeground(Color.BLACK);
		qt.addActionListener(this);

		mh = new JMenu("Help?");
		mb.add(mh);
		mh.setFont(new Font("Montserrat", Font.BOLD, 14));
		mh.setForeground(Color.BLACK);

		hlp = new JMenuItem("savoir +");
		mh.add(hlp);
		hlp.setFont(new Font("Montserrat", NORMAL, 14));
		hlp.setForeground(Color.BLACK);
		hlp.addActionListener(this);

		h = Game2.getGamePerso();

		pc = new JPanel();
		add(pc, "Center");

		grid = h.size();

		switch (grid) {
		case 1:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 2:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 3:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 4:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 5:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 6:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 7:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 8:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 9:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 10:
			nbrColonne = 4;
			nbrLigne = 3;
			break;
		case 11:
			nbrColonne = 4;
			nbrLigne = 3;
			break;
		case 12:
			nbrColonne = 4;
			nbrLigne = 3;
			break;
		case 13:
			nbrColonne = 3;
			nbrLigne = 5;
			break;
		case 14:
			nbrColonne = 3;
			nbrLigne = 5;
			break;
		case 15:
			nbrColonne = 3;
			nbrLigne = 5;
			break;
		case 16:
			nbrColonne = 4;
			nbrLigne = 4;
			break;
		case 17:
			nbrColonne = 4;
			nbrLigne = 5;
			break;
		case 18:
			nbrColonne = 4;
			nbrLigne = 5;
			break;
		case 19:
			nbrColonne = 4;
			nbrLigne = 5;
			break;
		case 20:
			nbrColonne = 4;
			nbrLigne = 5;
			break;

		}

		pc.setLayout(new GridLayout(nbrColonne, nbrLigne, 28, 4));
		pc.setBackground(new Color(197, 202, 233));

		p = new JButton[20];
		pe = new Personnage[20];

		for (String e : h.keySet()) {

			p[i] = new JButton(new ImageIcon(getClass().getResource(h.get(e).getFichier())));
			p[i].setBackground(Color.white);
			p[i].setFocusPainted(false);
			p[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
			p[i].setToolTipText(e);
			pe[i] = h.get(e);
			pc.add(p[i]);
			p[i].addActionListener(this);
			i++;
		}

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));

		ps1 = new JPanel();
		ps.add(ps1);
		ps1.setPreferredSize(new Dimension(840, 150));
		ps1.setBackground(new Color(232, 234, 246));
		ps1.setBorder(BorderFactory.createRaisedBevelBorder());
		ps1.setLayout(null);

		lreponse = new JLabel();
		lreponse.setBounds(258, 10, 380, 25);
		ps1.add(lreponse);

		lperso = new JLabel("Le personnage a deviner est :");
		lperso.setFont(new Font("Montserrat", NORMAL, 15));
		lperso.setVisible(false);
		lperso.setBounds(10, 90, 230, 25);
		ps1.add(lperso);

		lphoto = new JLabel(new ImageIcon(getClass().getResource(Game2.getPersoGame().getFichier())));
		lphoto.setVisible(false);
		lphoto.setBounds(240, 57, 137, 85);
		ps1.add(lphoto);

		bcancel = new JButton("Annuler");
		bcancel.setFont(new Font("Montserrat", Font.BOLD, 15));
		bcancel.setBackground(new Color(121, 134, 203));
		bcancel.setForeground(Color.white);
		bcancel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bcancel.setToolTipText("Retour a la partie");
		bcancel.setFocusPainted(false);
		bcancel.setBounds(695, 10, 137, 25);
		bcancel.addActionListener(this);
		ps1.add(bcancel);

		bquitter = new JButton("Quitter", new ImageIcon(getClass().getResource("Exit.png")));
		bquitter.setFont(new Font("Montserrat", Font.BOLD, 15));
		bquitter.setBackground(new Color(121, 134, 203));
		bquitter.setForeground(Color.white);
		bquitter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bquitter.setToolTipText("Quitter la partie");
		bquitter.setFocusPainted(false);
		bquitter.setBounds(695, 110, 137, 25);
		bquitter.addActionListener(this);
		ps1.add(bquitter);

		bquitter2 = new JButton("Quitter", new ImageIcon(getClass().getResource("Exit.png")));
		bquitter2.setFont(new Font("Montserrat", Font.BOLD, 15));
		bquitter2.setBackground(new Color(121, 134, 203));
		bquitter2.setForeground(Color.white);
		bquitter2.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bquitter2.setToolTipText("Nouvelle partie/Quitter la partie");
		bquitter2.setFocusPainted(false);
		bquitter2.setBounds(695, 110, 137, 25);
		bquitter2.setVisible(false);
		bquitter2.addActionListener(this);
		ps1.add(bquitter2);

	}

	public static void main(String[] args) {
		Valider v = new Valider();
		v.setVisible(true);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == hlp) {
			JOptionPane.showMessageDialog(this, "Cliquer sur une photo pour le resultat final ");
		}
		if (e.getSource() == (bquitter) || e.getSource() == (qt)) {
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment quitter la partie?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == 0) {
				Game2.getGamePerso().clear();
				Game2.getTquestion1().clear();
				Game2.getTquestion2().clear();
				Game2.quitter();
				Niveau.setFacile(false);
				Niveau.setDifficile(false);
				Niveau.setIsTresFacile(false);
				Mode_Dev.setIsDev(false);
				Mode_Player.setIsPlayer(false);
				Mode_Player.setIsDevPlayer(false);
				ChoixDev.setIsGen(false);
				Accueil.setClicked(false);
				this.dispose();
				Accueil a = null;
				try {
					a = new Accueil();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				a.show();
			}
		}
		if (e.getSource() == bquitter2) {
			String[] options = { "Nouvelle Partie", "Quitter" };

			int x = JOptionPane.showOptionDialog(null, reponse, "Qui est-ce ?", JOptionPane.DEFAULT_OPTION,
					JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

			if (x == 0) {
				this.dispose();
				Game2.getGamePerso().clear();
				Game2.getTquestion1().clear();
				Game2.getTquestion2().clear();
				Game2.quitter();
				Accueil.setClicked(false);

				Game2 g = null;
				try {
					g = new Game2();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				g.show();
			} else {
				this.dispose();
				Game2.getGamePerso().clear();
				Game2.getTquestion1().clear();
				Game2.getTquestion2().clear();
				Game2.quitter();
				Niveau.setFacile(false);
				Niveau.setDifficile(false);
				Niveau.setIsTresFacile(false);
				Mode_Dev.setIsDev(false);
				Mode_Player.setIsPlayer(false);
				Mode_Player.setIsDevPlayer(false);
				ChoixDev.setIsGen(false);
				Accueil.setClicked(false);
				Accueil a = null;
				try {
					a = new Accueil();
				} catch (Exception e1) {

					e1.printStackTrace();
				}
				a.show();

			}
		}

		if (e.getSource() == bcancel) {
			this.dispose();
		}

		for (int i = 0; i < h.size(); i++) {
			if (e.getSource() == p[i]) {
				bcancel.setVisible(false);
				bquitter.setVisible(false);
				bquitter2.setVisible(true);

				if (pe[i].getPrenom().equals(Game2.getPersoGame().getPrenom())) {

					reponse = "Felicitations ! vous avez gagne la partie ";
					lreponse.setText(reponse);
					lreponse.setFont(new Font("Montserrat", Font.BOLD, 16));
					ps1.setBackground(new Color(218, 248, 188));
					lperso.setVisible(true);
					lphoto.setVisible(true);

					for (int j = 0; j < h.size(); j++) {
						p[j].setEnabled(false);
						p[j].setBackground(new Color(215, 215, 215));
					}

				} else {
					reponse = "Game over !";
					lreponse.setText(reponse);
					lreponse.setFont(new Font("Montserrat", Font.BOLD, 16));
					ps1.setBackground(new Color(255, 166, 166));
					lperso.setVisible(true);
					lphoto.setVisible(true);

					for (int j = 0; j < h.size(); j++) {
						p[j].setEnabled(false);
						p[j].setBackground(new Color(215, 215, 215));
					}

				}

			}
		}

	}

}
