import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

public class GameDeuxPerso extends JFrame implements ActionListener, ItemListener {
	/** 
	 *  
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pc, ps, ps1;
	private JMenuBar mb;
	private JMenu mg, mh;
	private JMenuItem nw, sv, hlp, qt;
	private static JButton quitter;
	private JButton bsValide, bsChoix;
	private JComboBox<String> cNombre;
	private JComboBox<String> cquestion1, cchoix1;
	private String[] ListeChoix;
	private JLabel aide, lnbr, lreponse, lquestion;
	private int nbr = 0;
	private JToggleButton tp[];

	private int nbrLigne = 0, nbrColonne = 0;
	private Personnage ap[];

	private static HashMap<String, Personnage> gamePerso = new HashMap<>();
	private static HashMap<String, Personnage> tquestion1 = new HashMap<>();

	private static Personnage persoGame, persoGameDeux;
	private Personnage p;

	public GameDeuxPerso() throws Exception {
		setTitle("Qui est-ce ?");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));
		Accueil.getAp().setSave(false);

		mb = new JMenuBar();
		setJMenuBar(mb);
		mb.setBackground(new Color(197, 202, 233));
		mb.setOpaque(true);

		mg = new JMenu("Game");
		mb.add(mg);
		mg.setFont(new Font("Montserrat", Font.BOLD, 14));
		mg.setForeground(Color.BLACK);

		nw = new JMenuItem("New");
		mg.add(nw);
		nw.setFont(new Font("Montserrat", NORMAL, 14));
		nw.setForeground(Color.BLACK);
		nw.addActionListener(this);

		sv = new JMenuItem("Save");
		mg.add(sv);
		sv.setFont(new Font("Montserrat", NORMAL, 14));
		sv.setForeground(Color.BLACK);
		sv.addActionListener(this);
		mg.addSeparator();

		qt = new JMenuItem("Quit");
		mg.add(qt);
		qt.setFont(new Font("Montserrat", NORMAL, 14));
		qt.setForeground(Color.BLACK);
		qt.addActionListener(this);

		mh = new JMenu("Help?");
		mb.add(mh);
		mh.setFont(new Font("Montserrat", Font.BOLD, 14));
		mh.setForeground(Color.BLACK);

		hlp = new JMenuItem("savoir +");
		mh.add(hlp);
		hlp.setFont(new Font("Montserrat", NORMAL, 14));
		hlp.setForeground(Color.BLACK);
		hlp.addActionListener(this);

		pc = new JPanel();
		this.add(pc, "Center");

		pc.setBackground(new Color(232, 234, 246));

		tp = new JToggleButton[20];

		if (Accueil.isClicked()|| ChoixGen.getIsChoix()) {
			setNbrLigne((int) Accueil.getAp().getLigne());
			setNbrColonne((int) Accueil.getAp().getColonne());

			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {
				tp[i] = new JToggleButton(
						new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichier())));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(Accueil.getAp().getList().get(i).getPrenom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				pc.add(tp[i]);
				p = Accueil.getAp().getList().get(i);
				if (!Accueil.getAp().getList().get(i).getEstCocher()) {
					gamePerso.put(Accueil.getAp().getList().get(i).getPrenom(), p);
				}
				if (Accueil.getAp().getList().get(i).getEstCocher()) {
					tp[i].setSelected(true);
					tp[i].setIcon(
							new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichierBarre())));
				}
				persoGame = Accueil.getAp().getList().get((int) Accueil.getAp().getPersoATrouver());
				persoGameDeux = Accueil.getAp().getList().get((int) Accueil.getAp().getPersoATrouver1());

				switch ((int) Accueil.getAp().getNiveau()) {
				case 1:
					Niveau.setIsTresFacile(true);
					break;
				case 2:
					Niveau.setFacile(true);
					break;
				case 3:
					Niveau.setDifficile(true);
					break;
				}
			}

		} else {
			
			Partie pt=new Partie();
			for (int i = 0; i < pt.getListpartie().size(); i++) {
				if(pt.getListpartie().get(i).getNom().equals("Partie_de_base")) {
					pt.updateToList(new PartieObj("Partie_de_base", false), i);
					break;
				}
			}
			
			ap = new Personnage[20];
			if (Mode_Player.getIsPlayer()) {
				nbrLigne = 4;
				nbrColonne = 5;
			} else if (Mode_Dev.getIsDev()) {
				nbrLigne = Mode_Dev.getNbrLigne();
				nbrColonne = Mode_Dev.getNbrColonne();
			} else {
				setNbrLigne((int) Accueil.getAp().getLigne());
				setNbrColonne((int) Accueil.getAp().getColonne());
			}

			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {

				ap[i] = Accueil.getAp().getList().get(i);
				tp[i] = new JToggleButton(new ImageIcon(getClass().getResource(ap[i].getFichier())));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(ap[i].getPrenom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				pc.add(tp[i]);
				p = new Personnage(ap[i].getFichier(), ap[i].getFichierBarre(), ap[i].getGenre(), ap[i].getPrenom(),
						ap[i].getCheveux(), ap[i].getYeux(), ap[i].getSexe(), ap[i].getDessinAnnime(),
						ap[i].isAccessoires(), false);
				gamePerso.put(ap[i].getPrenom(), ap[i]);
				ap[i].setEstCocher(false);
				Accueil.getAp().update(p, i);
				persoGame = new Personnage(((nbrColonne * nbrLigne) - 1));
				persoGameDeux = new Personnage(((nbrColonne * nbrLigne) - 1));

				while (persoGame.getPrenom().equals(persoGameDeux.getPrenom())) {
					persoGameDeux = new Personnage(((nbrColonne * nbrLigne) - 1));
				}
				Accueil.getAp().setPersoATrouver(persoGame.getNum());
				Accueil.getAp().setPersoATrouver1(persoGameDeux.getNum());
			}

		}
		Accueil.getAp().setLigne(nbrLigne);
		Accueil.getAp().setColonne(nbrColonne);
		pc.setLayout(new GridLayout(nbrLigne, nbrColonne, 28, 4));

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));

		ps1 = new JPanel();
		ps.add(ps1);
		ps1.setPreferredSize(new Dimension(840, 100));
		ps1.setBackground(new Color(232, 234, 246));
		ps1.setBorder(BorderFactory.createRaisedBevelBorder());
		ps1.setLayout(null);

		quitter = new JButton("q");
		ps1.add(quitter);
		quitter.setVisible(false);
		quitter.addActionListener(this);

		bsValide = new JButton("Valider");
		ps1.add(bsValide);
		bsValide.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsValide.setBackground(new Color(121, 134, 203));
		bsValide.setForeground(Color.white);
		bsValide.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsValide.setToolTipText("Cliquez ici pour valider la/les question(s) choisie(s)");
		bsValide.setFocusPainted(false);
		bsValide.setBounds(732, 10, 100, 25);
		bsValide.addActionListener(this);

		bsChoix = new JButton("Valider choix");
		ps1.add(bsChoix);
		bsChoix.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsChoix.setBackground(new Color(121, 134, 203));
		bsChoix.setForeground(Color.white);
		bsChoix.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsChoix.setToolTipText("Cliquez ici pour valider votre choix de selection de personne");
		bsChoix.setFocusPainted(false);
		bsChoix.setBounds(693, 65, 140, 25);
		bsChoix.addActionListener(this);

		cNombre = new JComboBox<String>();
		ps1.add(cNombre);
		cNombre.setFont(new Font("Montserrat", NORMAL, 16));
		cNombre.setBackground(Color.white);
		cNombre.setForeground(Color.black);
		cNombre.setBounds(7, 40, 130, 20);
		cNombre.addItem("Au moins un");
		cNombre.addItem("Les deux");
		cNombre.setVisible(true);
		cNombre.addItemListener(this);

		cquestion1 = new JComboBox<String>();
		ps1.add(cquestion1);
		cquestion1.addItem("Genre");
		cquestion1.addItem("Cheveux");
		cquestion1.addItem("Yeux");
		cquestion1.addItem("Sexe");
		cquestion1.addItem("Accessoires");
		cquestion1.setFont(new Font("Montserrat", NORMAL, 15));
		cquestion1.setBackground(Color.white);
		cquestion1.setForeground(Color.black);
		cquestion1.setBounds(200, 40, 100, 25);
		cquestion1.addItemListener(this);
		cquestion1.setVisible(true);

		cchoix1 = new JComboBox<String>();
		ps1.add(cchoix1);
		cchoix1.setBounds(330, 40, 100, 25);
		cchoix1.addActionListener(this);
		cchoix1.setVisible(true);
		cchoix1.setBackground(Color.white);
		cchoix1.setForeground(Color.black);

		String item = (String) cquestion1.getSelectedItem();
		Question q1 = new Question();
		cchoix1.removeAllItems();
		q1.setCaracteristique(item);
		ListeChoix = q1.getItemC();
		for (int i = 0; i < ListeChoix.length; i++) {
			cchoix1.addItem(ListeChoix[i]);
		}

		lquestion = new JLabel("La reponse est: ");
		ps1.add(lquestion);
		lquestion.setFont(new Font("Montserrat", Font.BOLD, 14));
		lquestion.setBounds(480, 40, 230, 35);
		lquestion.setForeground(Color.black);
		lquestion.setVisible(false);

		lreponse = new JLabel();
		ps1.add(lreponse);
		lreponse.setFont(new Font("Montserrat", Font.BOLD, 15));
		lreponse.setBounds(613, 40, 230, 35);
		lreponse.setForeground(Color.black);
		lreponse.setVisible(true);

		aide = new JLabel("Nombre de case a cocher: ");
		ps1.add(aide);
		aide.setFont(new Font("Montserrat", Font.BOLD, 14));
		aide.setBounds(440, 65, 230, 35);
		aide.setForeground(new Color(215, 215, 215));
		aide.setVisible(false);

		lnbr = new JLabel();
		ps1.add(lnbr);
		lnbr.setFont(new Font("Montserrat", Font.BOLD, 14));
		lnbr.setBounds(640, 65, 230, 35);
		lnbr.setForeground(new Color(215, 215, 215));
		lnbr.setVisible(false);

	}

	public static HashMap<String, Personnage> getGamePerso() {
		return gamePerso;
	}

	public int getNbrLigne() {
		return nbrLigne;
	}

	public void setNbrLigne(int nbrLigne) {
		this.nbrLigne = nbrLigne;
	}

	public int getNbrColonne() {
		return nbrColonne;
	}

	public void setNbrColonne(int nbrColonne) {
		this.nbrColonne = nbrColonne;
	}

	public static void quitter() {
		quitter.doClick();
	}

	public static Personnage getPersoGame() {
		return persoGame;
	}

	public static Personnage getPersoGameDeux() {
		return persoGameDeux;
	}

	public String[] Getitem(String q) {
		String[] list = null;
		switch (q) {
		case "Genre":

			list = new String[] { "Cheveux", "Yeux", "Sexe", "Accessoires" };
			break;

		case "Cheveux":

			list = new String[] { "Genre", "Yeux", "Sexe", "Accessoires" };
			break;

		case "Yeux":

			list = new String[] { "Genre", "Cheveux", "Sexe", "Accessoires" };
			break;

		case "Sexe":

			list = new String[] { "Genre", "Cheveux", "Yeux", "Accessoires" };
			break;
		case "Accessoires":

			list = new String[] { "Genre", "Cheveux", "Yeux", "Sexe" };
			break;

		default:
			list = new String[] { "Genre", "Cheveux", "Yeux", "Sexe", "Accessoires" };
			break;

		}
		return list;
	}

	public static void main(String[] args) throws Exception {

		GameDeuxPerso jeu = new GameDeuxPerso();
		jeu.setVisible(true);

	}

	@SuppressWarnings({ "deprecation" })
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == nw) {
			gamePerso.clear();

			this.dispose();
			GameDeuxPerso jeu = null;
			try {
				jeu = new GameDeuxPerso();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			jeu.setVisible(true);
		}

		if (e.getSource() == sv) {
			try {
				Accueil.getAp().setPage(3);
			} catch (Exception e3) {
				e3.printStackTrace();
			}
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous sauvgarder cette partie ?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) {
				try {
					Accueil.getJg().setSave(false);
				} catch (Exception e2) {

					e2.printStackTrace();
				}
				try {
					Accueil.getAp().setSave(true);
				} catch (Exception e1) {

					e1.printStackTrace();
				}
			} else {
				try {
					Accueil.getAp().setSave(false);
				} catch (Exception e1) {

					e1.printStackTrace();
				}
			}
		}

		if (e.getSource() == hlp) {

			JOptionPane.showMessageDialog(this,
					"Le but du jeu :\nIdentifier les personnages choisis aleatoirement par l'ordinateur en procedant par elimination en posant des questions");

		}
		if (e.getSource() == quitter) {
			Niveau.setFacile(false);
			Niveau.setDifficile(false);
			Niveau.setIsTresFacile(false);
			Mode_Dev.setIsDev(false);
			Mode_Player.setIsPlayer(false);
			Mode_Player.setIsDevPlayer(false);
			ChoixDev.setIsGen(false);
			Accueil.setClicked(false);
			gamePerso.clear();
			tquestion1.clear();
			this.dispose();
		}
		if (e.getSource() == qt) {
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment quitter la partie?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == 0) {
				Niveau.setFacile(false);
				Niveau.setDifficile(false);
				Niveau.setIsTresFacile(false);
				Mode_Dev.setIsDev(false);
				Mode_Player.setIsPlayer(false);
				Mode_Player.setIsDevPlayer(false);
				ChoixDev.setIsGen(false);
				Accueil.setClicked(false);
				gamePerso.clear();
				tquestion1.clear();
				this.dispose();
				Accueil a = null;
				try {
					a = new Accueil();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				a.show();
			}
		}


		for (int i = 0; i < nbrColonne * nbrLigne; i++) {
			if (e.getSource() == tp[i]) {
				if (tp[i].isSelected()) {

					if (Accueil.isClicked()|| ChoixGen.getIsChoix()) {
						tp[i].setIcon(new ImageIcon(
								getClass().getResource(Accueil.getAp().getList().get(i).getFichierBarre())));

						Accueil.getAp().getList().get(i).setEstCocher(true);

						try {
							Accueil.getAp().update(Accueil.getAp().getList().get(i), i);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
					} else {
						tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
						ap[i].setEstCocher(true);
						try {
							Accueil.getAp().update(ap[i], i);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						gamePerso.remove(ap[i].getPrenom());
					}

				} else {

					if (Accueil.isClicked()|| ChoixGen.getIsChoix()) {
						tp[i].setIcon(
								new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichier())));

						Accueil.getAp().getList().get(i).setEstCocher(false);

						try {
							Accueil.getAp().update(Accueil.getAp().getList().get(i), i);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						gamePerso.put(Accueil.getAp().getList().get(i).getPrenom(), Accueil.getAp().getList().get(i));
					} else {
						tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichier())));
						ap[i].setEstCocher(false);
						try {
							Accueil.getAp().update(ap[i], i);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						gamePerso.put(ap[i].getPrenom(), ap[i]);
					}

				}
			}
		}

		if (e.getSource() == bsChoix) {
			if (gamePerso.size() >= 2) {
				ValiderDeuxPerso v = new ValiderDeuxPerso();
				v.show();
			} else {
				JOptionPane.showMessageDialog(this,
						"Veuillez ne pas cocher tous les personnages, faut avoir au moins deux qui sont pas coches !");
			}

		}

		if (e.getSource() == bsValide) {

			if (cquestion1.isVisible()) {
				if ((Niveau.getDifficile())) {
					lquestion.setBounds(440, 65, 230, 35);
					lreponse.setBounds(560, 65, 230, 35);
					aide.setVisible(false);
					lnbr.setVisible(false);
				}
				else {
					aide.setVisible(false);
					lnbr.setVisible(false);
				}
				lquestion.setVisible(true);
				lreponse.setVisible(true);
				aide.setForeground(Color.black);
				lnbr.setForeground(Color.black);
			}
			nbr = 0;
			tquestion1.clear();

			if (cNombre.isVisible()) {
				if (((String) cNombre.getSelectedItem()).equals("Au moins un")) {

					nbr = 0;
					switch ((String) cquestion1.getSelectedItem()) {
					case "Genre":
						if ((persoGame.getGenre().equals((String) cchoix1.getSelectedItem()))
								|| (persoGameDeux.getGenre().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");

						} else {
							lreponse.setText("Faux");

							for (Personnage v : gamePerso.values()) {
								if ((v.getGenre().equals((String) cchoix1.getSelectedItem()))) {

									nbr++;
								}
								lnbr.setText(nbr + "");

							}
						}

						break;
					case "Cheveux":
						if ((persoGame.getCheveux().equals((String) cchoix1.getSelectedItem()))
								|| (persoGameDeux.getCheveux().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");

						} else {
							lreponse.setText("Faux");

							for (Personnage v : gamePerso.values()) {
								if ((v.getCheveux().equals((String) cchoix1.getSelectedItem()))) {

									nbr++;
								}

							}
						}

						break;
					case "Yeux":
						if ((persoGame.getYeux().equals((String) cchoix1.getSelectedItem()))
								|| (persoGameDeux.getYeux().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");

						} else {
							lreponse.setText("Faux");

							for (Personnage v : gamePerso.values()) {
								if ((v.getYeux().equals((String) cchoix1.getSelectedItem()))) {

									nbr++;
								}

							}
						}

						break;
					case "Sexe":
						if ((persoGame.getSexe().equals((String) cchoix1.getSelectedItem()))
								|| (persoGameDeux.getSexe().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");

						} else {
							lreponse.setText("Faux");

							for (Personnage v : gamePerso.values()) {
								if ((v.getSexe().equals((String) cchoix1.getSelectedItem()))) {

									nbr++;
								}

							}
						}

						break;
					case "Accessoires":
						boolean b;
						if (((String) cchoix1.getSelectedItem()).equals("true")) {
							b = true;
						} else {
							b = false;
						}
						if ((persoGame.isAccessoires() == b) || (persoGameDeux.isAccessoires() == b)) {
							lreponse.setText("Vrai");

						} else {
							lreponse.setText("Faux");

							for (Personnage v : gamePerso.values()) {
								if (v.isAccessoires() == b) {

									nbr++;
								}

							}
						}

						break;

					}

				}

				else if (((String) cNombre.getSelectedItem()).equals("Les deux")) {

					nbr = 0;
					switch ((String) cquestion1.getSelectedItem()) {
					case "Genre":
						if ((persoGame.getGenre().equals((String) cchoix1.getSelectedItem()))
								&& (persoGameDeux.getGenre().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");
							for (Personnage v : gamePerso.values()) {

								if (!(v.getGenre().equals(persoGame.getGenre()))) {
									nbr++;
								}

							}

						} else {
							lreponse.setText("Faux");

						}

						break;
					case "Cheveux":
						if ((persoGame.getCheveux().equals((String) cchoix1.getSelectedItem()))
								&& (persoGameDeux.getCheveux().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");
							for (Personnage v : gamePerso.values()) {

								if (!(v.getCheveux().equals(persoGame.getCheveux()))) {
									nbr++;
								}

							}

						} else {
							lreponse.setText("Faux");

						}

						break;
					case "Yeux":
						if ((persoGame.getYeux().equals((String) cchoix1.getSelectedItem()))
								&& (persoGameDeux.getYeux().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");
							for (Personnage v : gamePerso.values()) {

								if (!(v.getYeux().equals(persoGame.getYeux()))) {
									nbr++;
								}

							}

						} else {
							lreponse.setText("Faux");

						}

						break;
					case "Sexe":
						if ((persoGame.getSexe().equals((String) cchoix1.getSelectedItem()))
								&& (persoGameDeux.getSexe().equals((String) cchoix1.getSelectedItem()))) {
							lreponse.setText("Vrai");
							for (Personnage v : gamePerso.values()) {

								if (!(v.getSexe().equals(persoGame.getSexe()))) {
									nbr++;
								}

							}

						} else {
							lreponse.setText("Faux");

						}

						break;
					case "Accessoires":
						boolean b;
						if (((String) cchoix1.getSelectedItem()).equals("true")) {
							b = true;
						} else {
							b = false;
						}
						if ((persoGame.isAccessoires() == b) && (persoGameDeux.isAccessoires() == b)) {
							lreponse.setText("Vrai");
							for (Personnage v : gamePerso.values()) {

								if (!(v.isAccessoires() == persoGame.isAccessoires())) {
									nbr++;
								}

							}

						} else {
							lreponse.setText("Faux");

						}

						break;

					}

				}

			}
		}
	}

	@SuppressWarnings("unused")
	@Override
	public void itemStateChanged(ItemEvent e) {

		if ((e.getSource() == cquestion1 && cquestion1.isPopupVisible())) {
			String item = (String) cquestion1.getSelectedItem();
			String[] ItemCombo = Getitem(item);
			item = (String) cquestion1.getSelectedItem();
			Question q1 = new Question();
			cchoix1.removeAllItems();
			q1.setCaracteristique(item);
			ListeChoix = q1.getItemC();
			for (int i = 0; i < ListeChoix.length; i++) {
				cchoix1.addItem(ListeChoix[i]);
			}

		}

	}

}