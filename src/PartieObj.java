public class PartieObj {
	private String nom;
	private boolean save;
	public PartieObj(String nom, boolean save) {
		this.nom = nom;
		this.save = save;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public boolean isSave() {
		return save;
	}
	public void setSave(boolean save) {
		this.save = save;
	}
	
}