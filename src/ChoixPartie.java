import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class ChoixPartie extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pe, pw, ps;
	private JTextField lFileImporter;
	private JRadioButton rImporter, rChoisir;
	private JButton bImporter, bRetour, bCommencer;
	private JList<String> ListePartie;
	private JSplitPane sep;
	private String path, ch;
	private boolean JsonFile;
	private Partie p;

	public ChoixPartie() throws Exception {

		setTitle("Importer/Choisir une partie");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pe = new JPanel();
		pe.setLayout(null);
		pe.setBackground(new Color(232, 234, 246));

		pw = new JPanel();
		pw.setLayout(null);
		pw.setBackground(new Color(232, 234, 246));

		sep = new JSplitPane(SwingConstants.VERTICAL, pe, pw);
		sep.setResizeWeight(0.5);
		sep.setRequestFocusEnabled(false);
		this.add(sep, "Center");

		rImporter = new JRadioButton("Importer une partie");
		pe.add(rImporter);
		rImporter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		rImporter.setToolTipText("Cocher/decocher pour importer");
		rImporter.setBounds(30, 20, 180, 30);
		rImporter.setBackground(new Color(232, 234, 246));
		rImporter.addActionListener(this);

		rChoisir = new JRadioButton("Choisir une partie");
		pw.add(rChoisir);
		rChoisir.setCursor(new Cursor(Cursor.HAND_CURSOR));
		rChoisir.setToolTipText("Cocher/decocher pour choisir");
		rChoisir.setBounds(30, 20, 180, 30);
		rChoisir.setBackground(new Color(232, 234, 246));
		rChoisir.addActionListener(this);

		ButtonGroup bg = new ButtonGroup();
		bg.add(rChoisir);
		bg.add(rImporter);

		bImporter = new JButton(new ImageIcon(getClass().getResource("upload2.png")));
		pe.add(bImporter);
		bImporter.setFont(new Font("Montserrat", Font.BOLD, 15));
		bImporter.setBackground(new Color(121, 134, 203));
		bImporter.setForeground(Color.white);
		bImporter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bImporter.setToolTipText("Cliquez ici pour importer votre partie");
		bImporter.setOpaque(false);
		bImporter.setContentAreaFilled(false);
		bImporter.setFocusPainted(false);
		bImporter.setBorderPainted(false);
		bImporter.setBounds(170, 200, 96, 96);
		bImporter.setFocusPainted(false);
		bImporter.setEnabled(false);
		bImporter.addActionListener(this);

		lFileImporter = new JTextField();
		pe.add(lFileImporter);
		lFileImporter.setFont(new Font("Montserrat", Font.BOLD, 13));
		lFileImporter.setForeground(Color.black);
		lFileImporter.setBounds(75, 330, 290, 30);
		lFileImporter.setBackground(new Color(232, 234, 246));
		lFileImporter.setEditable(false);
		lFileImporter.setVisible(false);
		lFileImporter.setHorizontalAlignment(JTextField.CENTER);

		p = new Partie();
		DefaultListModel<String> model = new DefaultListModel<>();
		for (int i = 0; i < p.getListpartie().size(); i++) {
			if (!p.getListpartie().get(i).getNom().equals("Partie_de_base")) {
				model.addElement(p.getListpartie().get(i).getNom());
			}
		}
		ListePartie = new JList<>(model);
		ListePartie.setEnabled(false);
		JScrollPane b2 = new JScrollPane(ListePartie);
		b2.setBounds(30, 50, 380, 400);
		pw.add(b2);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		bRetour = new JButton("<< Retour");
		ps.add(bRetour);
		bRetour.setFont(new Font("Montserrat", Font.BOLD, 15));
		bRetour.setBackground(new Color(121, 134, 203));
		bRetour.setForeground(Color.white);
		bRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bRetour.setToolTipText("Cliquez ici pour se rendre au choix generateur");
		bRetour.setBounds(16, 360, 120, 30);
		bRetour.setFocusPainted(false);
		bRetour.addActionListener(this);

		bCommencer = new JButton("Commencer la partie");
		ps.add(bCommencer);
		bCommencer.setFont(new Font("Montserrat", Font.BOLD, 15));
		bCommencer.setBackground(new Color(121, 134, 203));
		bCommencer.setForeground(Color.white);
		bCommencer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bCommencer.setToolTipText("Cliquez ici pour commencer la partie importee/choisiee");
		bCommencer.setBounds(750, 360, 180, 30);
		bCommencer.setFocusPainted(false);
		bCommencer.addActionListener(this);

	}

	public static void main(String[] args) throws Exception {
		ChoixPartie cp = new ChoixPartie();
		cp.setVisible(true);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == bRetour) {
			this.dispose();
			ChoixGen a = new ChoixGen();
			a.show();
		}

		if (e.getSource() == bImporter) {

			JFileChooser file = new JFileChooser();
			file.setCurrentDirectory(new File(System.getProperty("user.home")));

			FileNameExtensionFilter filter = new FileNameExtensionFilter("Json File(.json)", "json");
			file.addChoosableFileFilter(filter);
			int result = file.showSaveDialog(null);

			if (result == JFileChooser.APPROVE_OPTION) {
				File selectedFile = file.getSelectedFile();
				path = selectedFile.getAbsolutePath();
				int cpt = 0;
				for (int i = path.length() - 1; i >= 0; i--) {

					if (path.charAt(i) == '\\') {
						break;
					}
					cpt++;
				}

				ch = path.substring(path.length() - cpt, path.length());
				lFileImporter.setVisible(true);
				lFileImporter.setText(ch);

				if (ch.substring(ch.length() - 5, ch.length()).equals(".json")) {
					JsonFile = true;
					lFileImporter.setBackground(new Color(218, 248, 188));
					File sourceFile = new File(path);
					File desFile = new File("./" + sourceFile.getName());
					try {
						Files.copy(sourceFile.toPath(), desFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (Exception e1) {
					}
				} else {
					JsonFile = false;
					lFileImporter.setText("ERROR! Veuillez inserer un fichier .json");
					lFileImporter.setBackground(new Color(255, 166, 166));
				}
			}

		}

		if (e.getSource() == rImporter) {
			ListePartie.setEnabled(false);
			bImporter.setEnabled(true);

		}

		if (e.getSource() == rChoisir) {
			ListePartie.setEnabled(true);
			bImporter.setEnabled(false);
			lFileImporter.setText("");
			lFileImporter.setVisible(false);
		}

		if (e.getSource() == bCommencer) {

			if (rChoisir.isSelected()) {
				if (ListePartie.isSelectionEmpty()) {
					JOptionPane.showMessageDialog(null, "Veuillez selectionner d'abord une partie pour commencer!");
				} else {
					try {
						for (int j = 0; j < p.getListpartie().size(); j++) {

							if (p.getListpartie().get(j).getNom().equals(ListePartie.getSelectedValue())) {

								if (p.getListpartie().get(j).isSave()) {

									int confirmed = JOptionPane.showConfirmDialog(null,
											"Vous avez deja une partie '" + ListePartie.getSelectedValue()
													+ "' sauvegardee. Voulez-vous l'ecraser ?",
											"Confirmation", JOptionPane.YES_NO_OPTION);

									if (confirmed == JOptionPane.YES_OPTION) {

										p.updateToList(new PartieObj(ListePartie.getSelectedValue(), false), j);

										Accueil.getJg().getGame(ListePartie.getSelectedValue());
										Accueil.getJg().setSave(false);
										for (int i = 0; i < Accueil.getJg().getListAG().size(); i++) {
											Accueil.getJg().getListAG().get(i).setEstCocher(false);
											Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
										}
										this.dispose();
										Niveau n = new Niveau();
										Accueil.getJg().setGame(ListePartie.getSelectedValue());
										n.show();
									}

								} else {
									Accueil.getJg().getGame(ListePartie.getSelectedValue());
									Accueil.getJg().setSave(false);
									for (int i = 0; i < Accueil.getJg().getListAG().size(); i++) {
										Accueil.getJg().getListAG().get(i).setEstCocher(false);
										Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
									}
									this.dispose();
									Niveau n = new Niveau();
									Accueil.getJg().setGame(ListePartie.getSelectedValue());
									n.show();
								}
							}
						}

					} catch (Exception e1) {
					}
				}
			}
			if (rImporter.isSelected()) {
				if (!JsonFile) {
					JOptionPane.showMessageDialog(null, "Veuillez choisir un fichier JSON");
				} else {
					try {
						try {
							p.addToList(new PartieObj(ch.substring(0, ch.length() - 5), false));
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						Accueil.getJg().getGame(p.getListpartie().get(p.getListpartie().size() - 1).getNom());
						Niveau n = new Niveau();
						dispose();
						n.show();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}

	}

}
