import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;

public class Partie {

	private JSONParser jsp;
	private JSONObject se, pos;
	private JSONArray partie;
	private ArrayList<PartieObj> listpartie;
	public Partie() throws Exception {
		jsp = new JSONParser();
		se = (JSONObject) jsp.parse(new FileReader("./Parties.json"));
		partie= (JSONArray) se.get("partie");
		listpartie=new ArrayList<>();
		for (int i = 0; i < partie.size(); i++) {
			pos = (JSONObject) partie.get(i);
			listpartie.add(new PartieObj((String)pos.get("nom"), (boolean)pos.get("save")));
		}
	}
	private void write() throws Exception {
		try (FileWriter fw = new FileWriter("./Parties.json")) {
			String json = se.toJSONString(), towrite = "", tab = "";
			int j = 0;
			for (int i = 0; i < json.length(); i++) {
				if (json.charAt(i) == '{' || json.charAt(i) == '[') {
					tab += "\t";
					towrite += json.substring(j, i + 1) + "\n" + tab;
					j = i + 1;
				}
				if (json.charAt(i) == ',') {
					towrite += json.substring(j, i + 1) + "\n" + tab;
					j = i + 1;
				}
				if (json.charAt(i) == '}' || json.charAt(i) == ']') {
					tab = tab.substring(1, tab.length());
					towrite += json.substring(j, i) + "\n" + tab;
					j = i;
				}
			}
			towrite += json.charAt(json.length() - 1);
			fw.write(towrite);
			fw.flush();
		}
	}
	public ArrayList<PartieObj> getListpartie() {
		return listpartie;
	}
	@SuppressWarnings("unchecked")
	public void addToList(PartieObj po) throws Exception {
		pos = new JSONObject();
		listpartie.add(po);
		pos.put("nom", po.getNom());
		pos.put("save", po.isSave());
		partie.add(pos);
		write();
	}
	@SuppressWarnings("unchecked")
    public void updateToList(PartieObj po, int i) throws Exception {
        pos = new JSONObject();
        listpartie.set(i, po);
        pos.put("nom", po.getNom());
        pos.put("save", po.isSave());
        partie.set(i, pos);
        write();
    }
}
