public class AtribueG {
	private String nom,ficher,attr[];
	private boolean EstCocher;

	public AtribueG(String nom, String ficher,boolean EstCocher,String[] attr) {
		this.nom = nom;
		this.ficher = ficher;
		this.attr = attr;
		this.EstCocher=EstCocher;
	}

	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getFicher() {
		return ficher;
	}

	public void setFicher(String ficher) {
		this.ficher = ficher;
	}
	
	public boolean getEstCocher() {
		return EstCocher;
	}


	public void setEstCocher(boolean estCocher) {
		EstCocher = estCocher;
		
	}


	public String[] getAtr() {
		return attr;
	}

	public void setAtr(String[] attr) {
		this.attr = attr;
	}
	
}