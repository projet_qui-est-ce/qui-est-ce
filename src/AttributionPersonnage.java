import java.io.File;
import org.json.simple.parser.ParseException;

import com.sun.tools.javac.Main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.print.DocFlavor.URL;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;


import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unused")
public class AttributionPersonnage {

	private String fichier, fichierBarre, genre, prenom, cheveux, yeux, sexe, DessinAnime;
	private boolean accessoires,save, EstCocher;;
	private long ligne, colonne,Niveau, PersoATrouver,PersoATrouver1, Page;
	private JSONArray attribut, possibilites;
	private JSONObject jsonO, perso, pos, tab;
	private ArrayList<Personnage> list = new ArrayList<>();

	public AttributionPersonnage() throws FileNotFoundException, IOException, ParseException {

		JSONParser jsonP = new JSONParser();

		jsonO = (JSONObject) jsonP.parse(new FileReader("./personnages.json"));

		ligne = (long) jsonO.get("ligne");
		colonne = (long) jsonO.get("colonne");
		save = (Boolean) jsonO.get("save");
		Page = (long) jsonO.get("Page");
		PersoATrouver = (long) jsonO.get("PersoATrouver"); 
		PersoATrouver1 = (long) jsonO.get("PersoATrouver1");
		Niveau = (long) jsonO.get("Niveau");
		attribut = (JSONArray) jsonO.get("attribut");
		possibilites = (JSONArray) jsonO.get("possibilites");
	}

	public AttributionPersonnage(int i) {

		JSONParser jsonP = new JSONParser();

		try {

			jsonO = (JSONObject) jsonP.parse(new FileReader("./personnages.json"));
			possibilites = (JSONArray) jsonO.get("possibilites");
			perso = (JSONObject) possibilites.get(i);

			fichier = (String) perso.get("fichier");
			fichierBarre = (String) perso.get("fichierBarre");
			prenom = (String) perso.get("prenom");
			genre = (String) perso.get("genre");
			cheveux = (String) perso.get("cheveux");
			yeux = (String) perso.get("yeux");
			accessoires = (Boolean) perso.get("accessoires");
			sexe = (String) perso.get("sexe");
			DessinAnime = (String) perso.get("dessinAnime");
			EstCocher = (Boolean) perso.get("EstCocher");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void write() throws Exception {
		try(FileWriter fw=new FileWriter("./personnages.json")){
			String json=jsonO.toJSONString(),towrite="",tab="";int j=0;
			for (int i = 0; i < json.length();i++) {
				if (json.charAt(i)=='{'||json.charAt(i)=='[') {
					tab+="\t";
					towrite+=json.substring(j,i+1)+"\n"+tab;
					j=i+1;
				}
				if (json.charAt(i)==',') {
					towrite+=json.substring(j,i+1)+"\n"+tab;
					j=i+1;
				}
				if (json.charAt(i)=='}'||json.charAt(i)==']') {
					tab=tab.substring(1, tab.length());
					towrite+=json.substring(j,i)+"\n"+tab;
					j=i;
				}
			}
			towrite+=json.charAt(json.length()-1);
			fw.write(towrite);
			fw.flush();
		}
	}
	
	public String getFichier() {
		return fichier;
	}

	public String getFichierBarre() {
		return fichierBarre;
	}

	public String getGenre() {
		return genre;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getCheveux() {
		return cheveux;
	}

	public String getYeux() {
		return yeux;
	}

	public String getSexe() {
		return sexe;
	}

	public String getDessinAnime() {
		return DessinAnime;
	}

	public boolean isAccessoires() {
		return accessoires;
	}

	public long getNiveau() {
		return Niveau;
	}

	public long getLigne() {
		return ligne;
	}

	@SuppressWarnings("unchecked")
	public void setLigne(long l) throws Exception {
		ligne = l;
		jsonO.put("ligne", ligne);
		write();
	}

	public long getColonne() {
		return colonne;
	}

	@SuppressWarnings("unchecked")
	public void setColonne(long c) throws Exception {
		colonne = c;
		jsonO.put("colonne", colonne);
		write();
	}

	@SuppressWarnings("unchecked")
	public void setNiveau(long niveau) throws Exception {
		Niveau = niveau;
		jsonO.put("Niveau", Niveau);
		write();
	}

	public long getPage() {
		return Page;
	}

	@SuppressWarnings("unchecked")
	public void setPage(long p) throws Exception {
		Page = p;
		jsonO.put("Page", Page);
		write();
	}

	public long getPersoATrouver() {
		return PersoATrouver;
	}

	@SuppressWarnings("unchecked")
	public void setPersoATrouver(long perso) throws Exception {
		PersoATrouver = perso;
		jsonO.put("PersoATrouver", PersoATrouver);
		write();
	}
	
	public long getPersoATrouver1() {
		return PersoATrouver1;
	}

	@SuppressWarnings("unchecked")
	public void setPersoATrouver1(long perso1) throws Exception {
		PersoATrouver1 = perso1;
		jsonO.put("PersoATrouver1", PersoATrouver1);
		write();
	}

	public boolean getSave() {
		return save;
	}

	@SuppressWarnings("unchecked")
	public void setSave(boolean s) throws Exception {
		save = s;
		jsonO.put("save", s);
		write();
	}

	public boolean isEstCocher() {
		return EstCocher;
	}

	@SuppressWarnings("unchecked")
	public void setEstCocher(boolean estCocher) throws Exception {
		this.EstCocher = estCocher;
		perso.put("EstCocher", estCocher);
		write();
	}

	public ArrayList<Personnage> getList() {

		for (int i = 0; i < possibilites.size(); i++) {
			pos = (JSONObject) possibilites.get(i);

			list.add(new Personnage((String) pos.get("fichier"), (String) pos.get("fichierBarre"),
					(String) pos.get("genre"), (String) pos.get("prenom"), (String) pos.get("cheveux"),
					(String) pos.get("yeux"), (String) pos.get("sexe"), (String) pos.get("dessinAnime"),
					(Boolean) pos.get("accessoires"), 
					(Boolean)pos.get("EstCocher")));
			// ,(boolean) pos.get("accessoires"), (boolean) pos.get("EstCocher")
		}
		return list;
	}

	@SuppressWarnings({ "unchecked" })
	public void update(Personnage ap, int i) throws Exception {
		tab = new JSONObject();
		list.set(i, ap);
			tab.put("fichier", ap.getFichier());
			tab.put("fichierBarre", ap.getFichierBarre());
			tab.put("prenom", ap.getPrenom());
			tab.put("genre", ap.getGenre());
			tab.put("cheveux", ap.getCheveux());
			tab.put("yeux", ap.getYeux()); 
			tab.put("accessoires", ap.isAccessoires());
			tab.put("sexe", ap.getSexe());
			tab.put("dessinAnime", ap.getDessinAnnime());
			tab.put("EstCocher", ap.getEstCocher());

		possibilites.set(i, tab);
		write();
	}
//	public void clearList() throws Exception {
//		list.clear();
//	}

}
