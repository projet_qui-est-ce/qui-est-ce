import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

public class Game2 extends JFrame implements ActionListener, ItemListener {

	/** 
	 *  
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pc, ps, ps1;
	private JMenuBar mb;
	private JMenu mg, mh;
	private JMenuItem nw, sv, hlp, qt;
	private static JButton quitter;
	private JButton bsAjouter1, bsEnlever, bsChoix;
	public static JButton bsValide, bvalide2;
	private JComboBox<String> cLogique;
	private JComboBox<String> cquestion1, cquestion2, cchoix1, cchoix2;
	private String[] ListeChoix;
	private JLabel aide, lnbr, lreponse, lquestion;
	private int nbr = 0;
	private JToggleButton tp[];
	private boolean question1, question2;

	private int nbrLigne = 0, nbrColonne = 0;
	private Personnage ap[];

	private static HashMap<String, Personnage> gamePerso = new HashMap<>();
	private static HashMap<String, Personnage> tquestion1 = new HashMap<>();
	private static HashMap<String, Personnage> tquestion2 = new HashMap<>();
	private HashMap<String, Personnage> dif = new HashMap<>();

	private static Personnage persoGame;
	private Personnage p;

	public Game2() throws Exception {
		setTitle("Qui est-ce ?");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));
		Accueil.getAp().setSave(false);

		mb = new JMenuBar();
		setJMenuBar(mb);
		mb.setBackground(new Color(197, 202, 233));
		mb.setOpaque(true);

		mg = new JMenu("Game");
		mb.add(mg);
		mg.setFont(new Font("Montserrat", Font.BOLD, 14));
		mg.setForeground(Color.BLACK);

		nw = new JMenuItem("New");
		mg.add(nw);
		nw.setFont(new Font("Montserrat", NORMAL, 14));
		nw.setForeground(Color.BLACK);
		nw.addActionListener(this);

		sv = new JMenuItem("Save");
		mg.add(sv);
		sv.setFont(new Font("Montserrat", NORMAL, 14));
		sv.setForeground(Color.BLACK);
		sv.addActionListener(this);
		mg.addSeparator();

		qt = new JMenuItem("Quit");
		mg.add(qt);
		qt.setFont(new Font("Montserrat", NORMAL, 14));
		qt.setForeground(Color.BLACK);
		qt.addActionListener(this);

		mh = new JMenu("Help?");
		mb.add(mh);
		mh.setFont(new Font("Montserrat", Font.BOLD, 14));
		mh.setForeground(Color.BLACK);

		hlp = new JMenuItem("savoir +");
		mh.add(hlp);
		hlp.setFont(new Font("Montserrat", NORMAL, 14));
		hlp.setForeground(Color.BLACK);
		hlp.addActionListener(this);

		pc = new JPanel();
		this.add(pc, "Center");
		tp = new JToggleButton[20];

		if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

			setNbrLigne((int) Accueil.getAp().getLigne());
			setNbrColonne((int) Accueil.getAp().getColonne());
			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {
				tp[i] = new JToggleButton(
						new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichier())));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(Accueil.getAp().getList().get(i).getPrenom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				pc.add(tp[i]);
				p = Accueil.getAp().getList().get(i);
				if (!Accueil.getAp().getList().get(i).getEstCocher()) {
					gamePerso.put(Accueil.getAp().getList().get(i).getPrenom(), p);
				}
				if (Accueil.getAp().getList().get(i).getEstCocher()) {
					tp[i].setSelected(true);
					tp[i].setIcon(
							new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichierBarre())));
				}
				persoGame = Accueil.getAp().getList().get((int) Accueil.getAp().getPersoATrouver());

				switch ((int) Accueil.getAp().getNiveau()) {
				case 1:
					Niveau.setIsTresFacile(true);
					break;
				case 2:
					Niveau.setFacile(true);
					break;
				case 3:
					Niveau.setDifficile(true);
					break;
				}
			}

		} else {
			Partie pt = new Partie();
			for (int i = 0; i < pt.getListpartie().size(); i++) {
				if (pt.getListpartie().get(i).getNom().equals("Partie_de_base")) {
					pt.updateToList(new PartieObj("Partie_de_base", false), i);
					break;
				}
			}
			ap = new Personnage[20];
			if (Mode_Player.getIsPlayer()) {
				nbrLigne = 4;
				nbrColonne = 5;
			} else if (Mode_Dev.getIsDev()) {
				nbrLigne = Mode_Dev.getNbrLigne();
				nbrColonne = Mode_Dev.getNbrColonne();
			} else {
				setNbrLigne((int) Accueil.getAp().getLigne());
				setNbrColonne((int) Accueil.getAp().getColonne());
			}

			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {

				ap[i] = Accueil.getAp().getList().get(i);
				tp[i] = new JToggleButton(new ImageIcon(getClass().getResource(ap[i].getFichier())));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(ap[i].getPrenom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				pc.add(tp[i]);
				p = new Personnage(ap[i].getFichier(), ap[i].getFichierBarre(), ap[i].getGenre(), ap[i].getPrenom(),
						ap[i].getCheveux(), ap[i].getYeux(), ap[i].getSexe(), ap[i].getDessinAnnime(),
						ap[i].isAccessoires(), false);
				gamePerso.put(ap[i].getPrenom(), ap[i]);
				ap[i].setEstCocher(false);
				Accueil.getAp().update(p, i);
				persoGame = new Personnage(((nbrColonne * nbrLigne) - 1));
				long n = persoGame.getNum();
				Accueil.getAp().setPersoATrouver(n);
			}

		}

		Accueil.getAp().setLigne(nbrLigne);
		Accueil.getAp().setColonne(nbrColonne);

		pc.setLayout(new GridLayout(nbrLigne, nbrColonne, 28, 4));

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));

		ps1 = new JPanel();
		ps.add(ps1);
		ps1.setPreferredSize(new Dimension(840, 100));
		ps1.setBackground(new Color(232, 234, 246));
		ps1.setBorder(BorderFactory.createRaisedBevelBorder());
		ps1.setLayout(null);

		quitter = new JButton("q");
		ps1.add(quitter);
		quitter.setVisible(false);
		quitter.addActionListener(this);

		bsAjouter1 = new JButton("Ajouter");
		ps1.add(bsAjouter1);
		bsAjouter1.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsAjouter1.setBackground(new Color(121, 134, 203));
		bsAjouter1.setForeground(Color.white);
		bsAjouter1.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsAjouter1.setToolTipText("Cliquez ici pour ajouter une deuxieme question");
		bsAjouter1.setFocusPainted(false);
		bsAjouter1.addActionListener(this);
		bsAjouter1.setBounds(7, 10, 100, 25);
		bsAjouter1.addActionListener(this);
		bsAjouter1.setVisible(true);

		bsEnlever = new JButton("Enlever");
		ps1.add(bsEnlever);
		bsEnlever.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsEnlever.setBackground(new Color(121, 134, 203));
		bsEnlever.setForeground(Color.white);
		bsEnlever.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsEnlever.setToolTipText("Cliquez ici pour enlever la deuxieme question");
		bsEnlever.setFocusPainted(false);
		bsEnlever.setBounds(7, 65, 100, 25);
		bsEnlever.setVisible(false);
		bsEnlever.addActionListener(this);

		bsValide = new JButton("Valider");
		ps1.add(bsValide);
		bsValide.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsValide.setBackground(new Color(121, 134, 203));
		bsValide.setForeground(Color.white);
		bsValide.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsValide.setToolTipText("Cliquez ici pour valider la/les question(s) choisie(s)");
		bsValide.setFocusPainted(false);
		bsValide.setBounds(732, 10, 100, 25);
		bsValide.addActionListener(this);
		bsValide.setVisible(false);

		bvalide2 = new JButton("Valider");
		ps1.add(bvalide2);
		bvalide2.setFont(new Font("Montserrat", Font.BOLD, 15));
		bvalide2.setBackground(new Color(121, 134, 203));
		bvalide2.setForeground(Color.white);
		bvalide2.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bvalide2.setToolTipText("Cliquez ici pour valider la/les question(s) choisie(s)");
		bvalide2.setFocusPainted(false);
		bvalide2.setBounds(732, 10, 100, 25);
		bvalide2.addActionListener(this);

		bsChoix = new JButton("Valider choix");
		ps1.add(bsChoix);
		bsChoix.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsChoix.setBackground(new Color(121, 134, 203));
		bsChoix.setForeground(Color.white);
		bsChoix.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsChoix.setToolTipText("Cliquez ici pour valider votre choix de selection de personne");
		bsChoix.setFocusPainted(false);
		bsChoix.setBounds(693, 65, 140, 25);
		bsChoix.addActionListener(this);

		cLogique = new JComboBox<String>();
		ps1.add(cLogique);
		cLogique.setFont(new Font("Montserrat", NORMAL, 16));
		cLogique.setBackground(Color.white);
		cLogique.setForeground(Color.black);
		cLogique.setBounds(138, 40, 60, 20);
		cLogique.addItem("Or");
		cLogique.addItem("And");
		cLogique.setVisible(false);
		cLogique.addItemListener(this);

		cquestion1 = new JComboBox<String>();
		ps1.add(cquestion1);
		cquestion1.addItem("Genre");
		cquestion1.addItem("Cheveux");
		cquestion1.addItem("Yeux");
		cquestion1.addItem("Sexe");
		cquestion1.addItem("Accessoires");
		cquestion1.setFont(new Font("Montserrat", NORMAL, 15));
		cquestion1.setBackground(Color.white);
		cquestion1.setForeground(Color.black);
		cquestion1.setBounds(200, 10, 100, 25);
		cquestion1.addItemListener(this);
		cquestion1.setVisible(true);

		cquestion2 = new JComboBox<String>();
		ps1.add(cquestion2);
//		cquestion2.addItem("Genre");
		cquestion2.addItem("Cheveux");
		cquestion2.addItem("Yeux");
		cquestion2.addItem("Sexe");
		cquestion2.addItem("Accessoires");
		cquestion2.setFont(new Font("Montserrat", NORMAL, 15));
		cquestion2.setBackground(Color.white);
		cquestion2.setForeground(Color.black);
		cquestion2.setBounds(200, 65, 100, 25);
		cquestion2.setVisible(false);
		cquestion2.addItemListener(this);

		cchoix1 = new JComboBox<String>();
		ps1.add(cchoix1);
		cchoix1.setBounds(330, 10, 100, 25);
		cchoix1.addActionListener(this);
		cchoix1.setVisible(true);
		cchoix1.setBackground(Color.white);
		cchoix1.setForeground(Color.black);

		String item = (String) cquestion1.getSelectedItem();
		Question q1 = new Question();
		cchoix1.removeAllItems();
		q1.setCaracteristique(item);
		ListeChoix = q1.getItemC();
		for (int i = 0; i < ListeChoix.length; i++) {
			cchoix1.addItem(ListeChoix[i]);
		}

		cchoix2 = new JComboBox<String>();
		ps1.add(cchoix2);
		cchoix2.setBounds(330, 65, 100, 25);
		cchoix2.addActionListener(this);
		cchoix2.setVisible(false);
		cchoix2.setBackground(Color.white);
		cchoix2.setForeground(Color.black);

		lquestion = new JLabel("La reponse est: ");
		ps1.add(lquestion);
		lquestion.setFont(new Font("Montserrat", Font.BOLD, 14));
		lquestion.setBounds(440, 10, 230, 35);
		lquestion.setForeground(Color.black);
		lquestion.setVisible(false);

		lreponse = new JLabel();
		ps1.add(lreponse);
		lreponse.setFont(new Font("Montserrat", Font.BOLD, 15));
		lreponse.setBounds(573, 10, 230, 35);
		lreponse.setForeground(Color.black);
		lreponse.setVisible(false);

		aide = new JLabel("Nombre de case cocher: ");
		ps1.add(aide);
		aide.setFont(new Font("Montserrat", Font.BOLD, 14));
		aide.setBounds(440, 65, 230, 35);
		aide.setForeground(new Color(215, 215, 215));
		aide.setVisible(false);

		lnbr = new JLabel();
		ps1.add(lnbr);
		lnbr.setFont(new Font("Montserrat", Font.BOLD, 14));
		lnbr.setBounds(650, 65, 235, 35);
		lnbr.setForeground(new Color(215, 215, 215));
		lnbr.setVisible(false);


	}

	public static HashMap<String, Personnage> getGamePerso() {
		return gamePerso;
	}

	public static HashMap<String, Personnage> getTquestion1() {
		return tquestion1;
	}

	public static HashMap<String, Personnage> getTquestion2() {
		return tquestion2;
	}

	public static void quitter() {
		quitter.doClick();
	}

	public static Personnage getPersoGame() {
		return persoGame;
	}

	public int getNbrLigne() {
		return nbrLigne;
	}

	public void setNbrLigne(int nbrLigne) {
		this.nbrLigne = nbrLigne;
	}

	public int getNbrColonne() {
		return nbrColonne;
	}

	public void setNbrColonne(int nbrColonne) {
		this.nbrColonne = nbrColonne;
	}

	public String[] Getitem(String q) {
		String[] list = null;
		switch (q) {
		case "Genre":

			list = new String[] { "Cheveux", "Yeux", "Sexe", "Accessoires" };
			break;

		case "Cheveux":

			list = new String[] { "Genre", "Yeux", "Sexe", "Accessoires" };
			break;

		case "Yeux":

			list = new String[] { "Genre", "Cheveux", "Sexe", "Accessoires" };
			break;

		case "Sexe":

			list = new String[] { "Genre", "Cheveux", "Yeux", "Accessoires" };
			break;
		case "Accessoires":

			list = new String[] { "Genre", "Cheveux", "Yeux", "Sexe" };
			break;

		default:
			list = new String[] { "Genre", "Cheveux", "Yeux", "Sexe", "Accessoires" };
			break;

		}
		return list;
	}

	public static void main(String[] args) throws Exception {

		Game2 jeu = new Game2();
		jeu.setVisible(true);

	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public void actionPerformed(ActionEvent e) {

		for (int i = 0; i < nbrColonne * nbrLigne; i++) {
			if (tp[i].isSelected()) {
				if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

					Accueil.getAp().getList().get(i).setEstCocher(true);
					tp[i].setIcon(
							new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichierBarre())));

					try {
						Accueil.getAp().update(Accueil.getAp().getList().get(i), i);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());

				} else {

					ap[i].setEstCocher(true);
					tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
					try {
						Accueil.getAp().update(ap[i], i);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					gamePerso.remove(ap[i].getPrenom());

				}
			} else {

				if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
					tp[i].setIcon(new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichier())));

					Accueil.getAp().getList().get(i).setEstCocher(false);

					try {
						Accueil.getAp().update(Accueil.getAp().getList().get(i), i);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					gamePerso.put(Accueil.getAp().getList().get(i).getPrenom(), Accueil.getAp().getList().get(i));
				} else {
					tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichier())));
					ap[i].setEstCocher(false);
					try {
						Accueil.getAp().update(ap[i], i);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					gamePerso.put(ap[i].getPrenom(), ap[i]);
				}

			}

		}

		if (e.getSource() == nw) {
			if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

				for (int i = 0; i < Accueil.getJg().getListAG().size(); i++) {
					Accueil.getAp().getList().get(i).setEstCocher(false);
					try {
						Accueil.getAp().update(Accueil.getAp().getList().get(i), i);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					tp[i].setIcon(new ImageIcon(getClass().getResource(Accueil.getAp().getList().get(i).getFichier())));
					;
				}
			} else {

				for (int i = 0; i < Accueil.getJg().getListAG().size(); i++) {
					ap[i].setEstCocher(false);
					try {
						Accueil.getAp().update(ap[i], i);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichier())));
					;
				}
			}

			tquestion1.clear();
			tquestion2.clear();
			dif.clear();
			persoGame = new Personnage(((nbrColonne * nbrLigne) - 1));
			long n = persoGame.getNum();
			try {
				Accueil.getAp().setPersoATrouver(n);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			this.dispose();
			try {
				Game2 g = new Game2();
				g.show();
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (e.getSource() == sv) {
			try {
				Accueil.getAp().setPage(2);
			} catch (Exception e3) {
				e3.printStackTrace();
			}
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous sauvgarder cette partie ?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) {
				try {
					Accueil.getJg().setSave(false);
				} catch (Exception e2) {

					e2.printStackTrace();
				}
				try {
					Accueil.getAp().setSave(true);
				} catch (Exception e1) {

					e1.printStackTrace();
				}
			} else {
				try {
					Accueil.getAp().setSave(false);
				} catch (Exception e1) {

					e1.printStackTrace();
				}
			}
		}
		if (e.getSource() == hlp) {

			JOptionPane.showMessageDialog(this,
					"Le but du jeu :\nIdentifier le personnage choisis aleatoirement par l'ordinateur en procedant par elimination en posant une ou deux questions");

		}
		if (e.getSource() == quitter) {
			Niveau.setFacile(false);
			Niveau.setDifficile(false);
			Niveau.setIsTresFacile(false);
			Mode_Dev.setIsDev(false);
			Mode_Player.setIsPlayer(false);
			Mode_Player.setIsDevPlayer(false);
			ChoixDev.setIsGen(false);
			Accueil.setClicked(false);
			this.dispose();
			gamePerso.clear();
			tquestion1.clear();
			tquestion2.clear();
			dif.clear();
		}
		if (e.getSource() == qt) {
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment quitter la partie?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == 0) {
				Niveau.setFacile(false);
				Niveau.setDifficile(false);
				Niveau.setIsTresFacile(false);
				Mode_Dev.setIsDev(false);
				Mode_Player.setIsPlayer(false);
				Mode_Player.setIsDevPlayer(false);
				ChoixDev.setIsGen(false);
				Accueil.setClicked(false);
				gamePerso.clear();
				tquestion1.clear();
				tquestion2.clear();
				dif.clear();
				this.dispose();
				Accueil a = null;
				try {
					a = new Accueil();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				a.show();
			}
		}

		if (e.getSource() == bsAjouter1) {
			cquestion2.setVisible(true);
			bsEnlever.setVisible(true);
			cLogique.setVisible(true);
			cchoix2.setVisible(true);
			String item1 = (String) cquestion2.getSelectedItem();
			Question q2 = new Question();
			cchoix2.removeAllItems();
			q2.setCaracteristique(item1);
			ListeChoix = q2.getItemC();
			for (int i = 0; i < ListeChoix.length; i++) {
				cchoix2.addItem(ListeChoix[i]);
			}
			bsAjouter1.setVisible(false);

		}

		if (e.getSource() == bsEnlever) {
			bsEnlever.setVisible(false);
			cLogique.setVisible(false);
			cquestion2.setVisible(false);
			cchoix2.setVisible(false);
			bsAjouter1.setVisible(true);

			String[] ItemCombos = Getitem(" ");
			cquestion1.removeAllItems();
			for (int i = 0; i < ItemCombos.length; i++) {
				cquestion1.addItem(ItemCombos[i]);
			}
			Question q1 = new Question();
			cchoix1.removeAllItems();
			String item = (String) cquestion1.getSelectedItem();
			q1.setCaracteristique(item);
			ListeChoix = q1.getItemC();
			for (int j = 0; j < ListeChoix.length; j++) {
				cchoix1.addItem(ListeChoix[j]);
			}

		}

		if (e.getSource() == bsChoix) {
			
			if (!gamePerso.isEmpty()) {
				Valider2 v = new Valider2();
				v.show();
			} else {
				JOptionPane.showMessageDialog(null,
						"Veuillez ne pas cocher tous les personnages, faut avoir au moins un qui n'est pas coche !");
			}

		}

		if (e.getSource() == bsValide || e.getSource() == bvalide2) {

			if (cquestion1.isVisible()) {
				if ((Niveau.getDifficile())) {
					lquestion.setBounds(440, 65, 230, 35);
					lreponse.setBounds(560, 65, 230, 35);
					aide.setVisible(false);
					lnbr.setVisible(false);
				} else {
					aide.setVisible(true);
					lnbr.setVisible(true);
				}
				lquestion.setVisible(true);
				lreponse.setVisible(true);
				aide.setForeground(Color.black);
				lnbr.setForeground(Color.black);
			}
			nbr = 0;
			tquestion1.clear();
			tquestion2.clear();

			if (cLogique.isVisible()) {

				if (((String) cLogique.getSelectedItem()).equals("Or")) {

					switch ((String) cquestion1.getSelectedItem()) {
					case "Genre":
						if (persoGame.getGenre().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getGenre().equals(persoGame.getGenre()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getGenre().equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}

						break;
					case "Cheveux":
						if (persoGame.getCheveux().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getCheveux().equals(persoGame.getCheveux()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getCheveux()
										.equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Yeux":
						if (persoGame.getYeux().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getYeux().equals(persoGame.getYeux()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getYeux().equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Sexe":
						if (persoGame.getSexe().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getSexe().equals(persoGame.getSexe()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getSexe().equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Accessoires":
						boolean b;
						if (((String) cchoix1.getSelectedItem()).equals("true")) {
							b = true;
						} else {
							b = false;
						}
						if (persoGame.isAccessoires() == b) {

							question2 = true;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).isAccessoires() == persoGame.isAccessoires())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).isAccessoires() == b) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						}
						break;

					}

					switch ((String) cquestion2.getSelectedItem()) {
					case "Genre":
						if (persoGame.getGenre().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getGenre().equals(persoGame.getGenre()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getGenre().equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Cheveux":
						if (persoGame.getCheveux().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getCheveux().equals(persoGame.getCheveux()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getCheveux()
										.equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Yeux":
						if (persoGame.getYeux().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getYeux().equals(persoGame.getYeux()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getYeux().equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Sexe":
						if (persoGame.getSexe().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getSexe().equals(persoGame.getSexe()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getSexe().equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Accessoires":
						boolean b;
						if (((String) cchoix2.getSelectedItem()).equals("true")) {
							b = true;
						} else {
							b = false;
						}
						if (persoGame.isAccessoires() == b) {

							question2 = true;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).isAccessoires() == persoGame.isAccessoires())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).isAccessoires() == b) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						}

						break;

					}

					if (question1 || question2) {

						lreponse.setText("Vrai");
						for (String a : tquestion1.keySet()) {
							for (String b : tquestion2.keySet()) {
								if (a.equals(b)) {
									nbr++;
								}
							}
						}

						if (bvalide2.isVisible()) {
							for (Personnage values : tquestion1.values()) {

								for (int i = 0; i < nbrColonne * nbrLigne; i++) {

									if (tp[i].getToolTipText().contentEquals(values.getPrenom())) {
										tp[i].setSelected(true);
										tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
										gamePerso.remove(ap[i].getPrenom());
									}

								}

							}

							for (Personnage values : tquestion2.values()) {
								for (int i = 0; i < nbrColonne * nbrLigne; i++) {

									if (tp[i].getToolTipText().contentEquals(values.getPrenom())) {
										tp[i].setSelected(true);
										tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
										gamePerso.remove(ap[i].getPrenom());
									}
								}
							}
//						
						}

						lnbr.setText((tquestion1.size() + tquestion2.size() - nbr) + "");

					} else {
						lreponse.setText("Faux");
						for (String a : tquestion1.keySet()) {
							for (String b : tquestion2.keySet()) {
								if (a.equals(b)) {
									nbr++;
								}
							}
						}
						if (bvalide2.isVisible()) {
							for (Personnage values : tquestion1.values()) {

								for (int i = 0; i < nbrColonne * nbrLigne; i++) {

									if (tp[i].getToolTipText().contentEquals(values.getPrenom())) {
										tp[i].setSelected(true);
										tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
										gamePerso.remove(ap[i].getPrenom());
									}
								}

							}

							for (Personnage values : tquestion2.values()) {

								for (int i = 0; i < nbrColonne * nbrLigne; i++) {

									if (tp[i].getToolTipText().contentEquals(values.getPrenom())) {
										tp[i].setSelected(true);
										tp[i].setIcon(new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
										gamePerso.remove(ap[i].getPrenom());
									}
								}
							}
						}
						lnbr.setText((tquestion1.size() + tquestion2.size() - nbr) + "");

					}

				} else {

					switch ((String) cquestion1.getSelectedItem()) {
					case "Genre":
						if (persoGame.getGenre().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getGenre().equals(persoGame.getGenre()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getGenre().equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}

						break;
					case "Cheveux":
						if (persoGame.getCheveux().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getCheveux().equals(persoGame.getCheveux()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getCheveux()
										.equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Yeux":
						if (persoGame.getYeux().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getYeux().equals(persoGame.getYeux()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getYeux().equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Sexe":
						if (persoGame.getSexe().equals((String) cchoix1.getSelectedItem())) {

							question1 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getSexe().equals(persoGame.getSexe()))) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getSexe().equals((String) cchoix1.getSelectedItem())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Accessoires":
						boolean b;
						if (((String) cchoix1.getSelectedItem()).equals("true")) {
							b = true;
						} else {
							b = false;
						}
						if (persoGame.isAccessoires() == b) {

							question2 = true;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).isAccessoires() == persoGame.isAccessoires())) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question1 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).isAccessoires() == b) {

									tquestion1.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						}
						break;

					}

					switch ((String) cquestion2.getSelectedItem()) {
					case "Genre":
						if (persoGame.getGenre().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getGenre().equals(persoGame.getGenre()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getGenre().equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Cheveux":
						if (persoGame.getCheveux().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getCheveux().equals(persoGame.getCheveux()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getCheveux()
										.equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Yeux":
						if (persoGame.getYeux().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getYeux().equals(persoGame.getYeux()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getYeux().equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Sexe":
						if (persoGame.getSexe().equals((String) cchoix2.getSelectedItem())) {

							question2 = true;

							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).getSexe().equals(persoGame.getSexe()))) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).getSexe().equals((String) cchoix2.getSelectedItem())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}
						}
						break;
					case "Accessoires":
						boolean b;
						if (((String) cchoix2.getSelectedItem()).equals("true")) {
							b = true;
						} else {
							b = false;
						}
						if (persoGame.isAccessoires() == b) {

							question2 = true;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (!(((Personnage) v.getValue()).isAccessoires() == persoGame.isAccessoires())) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						} else {

							question2 = false;
							for (Map.Entry v : gamePerso.entrySet()) {

								if (((Personnage) v.getValue()).isAccessoires() == b) {

									tquestion2.put((String) v.getKey(), (Personnage) v.getValue());

								}

							}

						}

						break;

					}

					if (question1 && question2) {

						lreponse.setText("Vrai");
						for (String a : tquestion1.keySet()) {
							for (String b : tquestion2.keySet()) {
								if (a.equals(b)) {
									nbr++;

									if (bvalide2.isVisible()) {
										for (int i = 0; i < nbrColonne * nbrLigne; i++) {
											if ((tp[i].getToolTipText().contentEquals(tquestion1.get(a).getPrenom()))
													|| (tp[i].getToolTipText()
															.contentEquals(tquestion2.get(b).getPrenom()))) {
												tp[i].setSelected(true);
												if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
													tp[i].setIcon(new ImageIcon(getClass().getResource(
															Accueil.getAp().getList().get(i).getFichierBarre())));
													gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
												} else {
													tp[i].setIcon(new ImageIcon(
															getClass().getResource(ap[i].getFichierBarre())));
													gamePerso.remove(ap[i].getPrenom());
												}

											}

										}
									}
								}
							}
						}

//						
						lnbr.setText((tquestion1.size() + tquestion2.size() - nbr) + "");

					} else {
						lreponse.setText("Faux");

						if (question1 == false) {
							if (question2) {
								for (String c : gamePerso.keySet()) {
//									

									if (!(tquestion2.containsValue(gamePerso.get(c)))) {
										dif.put(c, gamePerso.get(c));
									}
								}

								for (String a : tquestion1.keySet()) {
									for (String b : dif.keySet()) {
										if (a.equals(b)) {
											nbr++;
											if (bvalide2.isVisible()) {

												for (int i = 0; i < nbrColonne * nbrLigne; i++) {
													if (tp[i].getToolTipText()
															.contentEquals(tquestion1.get(a).getPrenom()))

													{

														tp[i].setSelected(true);
														if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
															tp[i].setIcon(new ImageIcon(getClass().getResource(Accueil
																	.getAp().getList().get(i).getFichierBarre())));
															gamePerso.remove(
																	Accueil.getAp().getList().get(i).getPrenom());
														} else {
															tp[i].setIcon(new ImageIcon(
																	getClass().getResource(ap[i].getFichierBarre())));
															gamePerso.remove(ap[i].getPrenom());
														}

													}
												}

											}
										}
									}

								}
							} else {
								for (String a : tquestion1.keySet()) {
									for (String b : tquestion2.keySet()) {
										if (a.equals(b)) {
											nbr++;
											if (bvalide2.isVisible()) {

												for (int i = 0; i < nbrColonne * nbrLigne; i++) {
													if (tp[i].getToolTipText()
															.contentEquals(tquestion1.get(a).getPrenom()))

													{

														tp[i].setSelected(true);
														if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
															tp[i].setIcon(new ImageIcon(getClass().getResource(Accueil
																	.getAp().getList().get(i).getFichierBarre())));
															gamePerso.remove(
																	Accueil.getAp().getList().get(i).getPrenom());
														} else {
															tp[i].setIcon(new ImageIcon(
																	getClass().getResource(ap[i].getFichierBarre())));
															gamePerso.remove(ap[i].getPrenom());
														}

													}
												}

											}
										}
									}
								}

							}

						}
//							

						else {
//							
							if (question2 == false) {

								for (String c : gamePerso.keySet()) {
//																				
									if (!(tquestion1.containsValue(gamePerso.get(c)))) {
										dif.put(c, gamePerso.get(c));
									}
								}
								if (bvalide2.isVisible()) {
									for (String a : tquestion2.keySet()) {
										for (String b : dif.keySet()) {
											if (a.equals(b)) {
												nbr++;

												for (int i = 0; i < nbrColonne * nbrLigne; i++) {
													if (tp[i].getToolTipText()
															.contentEquals(tquestion2.get(a).getPrenom()))

													{

														tp[i].setSelected(true);
														if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
															tp[i].setIcon(new ImageIcon(getClass().getResource(Accueil
																	.getAp().getList().get(i).getFichierBarre())));
															gamePerso.remove(
																	Accueil.getAp().getList().get(i).getPrenom());
														} else {
															tp[i].setIcon(new ImageIcon(
																	getClass().getResource(ap[i].getFichierBarre())));
															gamePerso.remove(ap[i].getPrenom());
														}

													}
												}

											}
										}

									}
								}

							}
//								
//							

						}

//						
						lnbr.setText(nbr + "");

					}

				}

			} else {
				nbr = 0;
				switch ((String) cquestion1.getSelectedItem()) {
				case "Genre":
					if (persoGame.getGenre().equals((String) cchoix1.getSelectedItem())) {
						lreponse.setText("Vrai");
						for (Personnage v : gamePerso.values()) {

							if (!(v.getGenre().equals(persoGame.getGenre()))) {
								nbr++;
								if (bvalide2.isVisible()) {
									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}

									}

								}
							}

						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}
						}

					} else {
						lreponse.setText("Faux");

						for (Personnage v : gamePerso.values()) {
							if ((v.getGenre().equals((String) cchoix1.getSelectedItem()))) {

								nbr++;
								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {

										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}

									}
								}
							}
						}

						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}

						}
					}

					break;
				case "Cheveux":
					if (persoGame.getCheveux().equals((String) cchoix1.getSelectedItem())) {
						lreponse.setText("Vrai");
						for (Personnage v : gamePerso.values()) {

							if (!(v.getCheveux().equals(persoGame.getCheveux()))) {
								nbr++;

								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}
									}
								}
							}

						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}
						}

					} else {
						lreponse.setText("Faux");

						for (Personnage v : gamePerso.values()) {
							if ((v.getCheveux().equals((String) cchoix1.getSelectedItem()))) {

								nbr++;
								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}
									}

								}

							}
						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}

						}
					}

					break;
				case "Yeux":
					if (persoGame.getYeux().equals((String) cchoix1.getSelectedItem())) {
						lreponse.setText("Vrai");
						for (Personnage v : gamePerso.values()) {

							if (!(v.getYeux().equals(persoGame.getYeux()))) {
								nbr++;
								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}
									}

								}
							}

						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}

						}
					} else {
						lreponse.setText("Faux");

						for (Personnage v : gamePerso.values()) {
							if ((v.getYeux().equals((String) cchoix1.getSelectedItem()))) {

								nbr++;

								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}
									}
								}

							}
						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}
						}
					}

					break;
				case "Sexe":
					if (persoGame.getSexe().equals((String) cchoix1.getSelectedItem())) {
						lreponse.setText("Vrai");
						for (Personnage v : gamePerso.values()) {

							if (!(v.getSexe().equals(persoGame.getSexe()))) {
								nbr++;
								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}
										}
									}

								}
							}

						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}

						}
					} else {
						lreponse.setText("Faux");

						for (Personnage v : gamePerso.values()) {
							if ((v.getSexe().equals((String) cchoix1.getSelectedItem()))) {

								nbr++;
								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}
									}

								}
							}
						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}

						}
					}

					break;
				case "Accessoires":
					boolean b;
					if (((String) cchoix1.getSelectedItem()).equals("true")) {
						b = true;
					} else {
						b = false;
					}
					if (persoGame.isAccessoires() == b) {
						lreponse.setText("Vrai");
						for (Personnage v : gamePerso.values()) {

							if (!(v.isAccessoires() == persoGame.isAccessoires())) {
								nbr++;
								if (bvalide2.isVisible()) {

									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}
									}
								}
							}

						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else {
									gamePerso.remove(ap[i].getPrenom());
								}

							}

						}
					} else {
						lreponse.setText("Faux");

						for (Personnage v : gamePerso.values()) {
							if (v.isAccessoires() == b) {

								nbr++;
								if (bvalide2.isVisible()) {
									for (int i = 0; i < nbrColonne * nbrLigne; i++) {
										if (tp[i].getToolTipText().contentEquals(v.getPrenom()))

										{

											tp[i].setSelected(true);
											if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
												tp[i].setIcon(new ImageIcon(getClass().getResource(
														Accueil.getAp().getList().get(i).getFichierBarre())));
											} else {
												tp[i].setIcon(
														new ImageIcon(getClass().getResource(ap[i].getFichierBarre())));
											}

										}
									}
								}
							}
						}
						lnbr.setText(nbr + "");
						for (int i = 0; i < nbrColonne * nbrLigne; i++) {
							if (tp[i].isSelected()) {
								if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
									gamePerso.remove(Accueil.getAp().getList().get(i).getPrenom());
								} else
									gamePerso.remove(ap[i].getPrenom());
							}

						}

					}
					break;
				}

			}

		}

	}

	@Override
	public void itemStateChanged(ItemEvent e) {

		if ((e.getSource() == cquestion1 && cquestion1.isPopupVisible())) {
			String item = (String) cquestion1.getSelectedItem();
			String[] ItemCombo = Getitem(item);
			String s = (String) cquestion2.getSelectedItem();
			cquestion2.removeAllItems();
			for (int i = 0; i < ItemCombo.length; i++) {
				cquestion2.addItem(ItemCombo[i]);
			}
			cquestion2.setSelectedItem(s);
			item = (String) cquestion1.getSelectedItem();
			Question q1 = new Question();
			cchoix1.removeAllItems();
			q1.setCaracteristique(item);
			ListeChoix = q1.getItemC();
			for (int i = 0; i < ListeChoix.length; i++) {
				cchoix1.addItem(ListeChoix[i]);
			}

		}

		if ((e.getSource() == cquestion2 && cquestion2.isPopupVisible())) {
			if (cquestion2.getSelectedItem() != null) {
				String items = (String) cquestion2.getSelectedItem();
				String[] ItemCombos = Getitem(items);
				String s = (String) cquestion1.getSelectedItem();
				cquestion1.removeAllItems();
				for (int i = 0; i < ItemCombos.length; i++) {
					cquestion1.addItem(ItemCombos[i]);
				}
				cquestion1.setSelectedItem(s);
				String item1 = (String) cquestion2.getSelectedItem();
				Question q2 = new Question();
				q2.setCaracteristique(item1);
				cchoix2.removeAllItems();
				ListeChoix = q2.getItemC();
				for (int i = 0; i < ListeChoix.length; i++) {
					cchoix2.addItem(ListeChoix[i]);
				}

				String item = (String) cquestion1.getSelectedItem();
				Question q1 = new Question();
				cchoix1.removeAllItems();
				q1.setCaracteristique(item);
				ListeChoix = q1.getItemC();
				for (int i = 0; i < ListeChoix.length; i++) {
					cchoix1.addItem(ListeChoix[i]);
				}
			}
		}

	}

}