import java.util.Random;

public class Personnage {

	private String fichier, fichierBarre, prenom, cheveux, yeux, sexe, DessinAnime, genre;
	private boolean accessoires, EstCocher;
	private Random obj;
	private int num;
	private AttributionPersonnage p;

	public Personnage(String fichier, String fichierBarre, String genre, String prenom, String cheveux, String yeux,
			String sexe, String dessinAnime, boolean accessoires, boolean EstCocher) {
		this.fichier = fichier;
		this.fichierBarre = fichierBarre;
		this.genre = genre;
		this.prenom = prenom;
		this.cheveux = cheveux;
		this.yeux = yeux;
		this.sexe = sexe;
		this.DessinAnime = dessinAnime;
		this.accessoires = accessoires;
		this.EstCocher = EstCocher;
	}

	public Personnage(int i) {
		obj = new Random();
		num = obj.nextInt(i);
		p = new AttributionPersonnage(num);
		this.fichier = p.getFichier();
		this.fichierBarre = p.getFichierBarre();
		this.genre = p.getGenre();
		this.prenom = p.getPrenom();
		this.cheveux = p.getCheveux();
		this.yeux = p.getYeux();
		this.sexe = p.getSexe();
		this.DessinAnime = p.getDessinAnime();
		this.accessoires = p.isAccessoires();
		this.EstCocher = p.isEstCocher();

	}

	public boolean getEstCocher() {
		return EstCocher;
	}
	public void setEstCocher(boolean estCocher) {
		this.EstCocher = estCocher;
	}

	public String getFichier() {
		return fichier;
	}


	public String getFichierBarre() {
		return fichierBarre;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getCheveux() {
		return cheveux;
	}

	public String getYeux() {
		return yeux;
	}

	public String getSexe() {
		return sexe;
	}

	public String getDessinAnnime() {
		return DessinAnime;
	}

	public String getGenre() {
		return genre;
	}

	public boolean isAccessoires() {
		return accessoires;
	}
	
	public Random getObj() {
		return obj;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public AttributionPersonnage getP() {
		return p;
	}

	public void setP(AttributionPersonnage p) {
		this.p = p;
	}

}