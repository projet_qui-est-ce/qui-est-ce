import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.*;
import javax.swing.*;

public class GameGenerateur2perso extends JFrame implements ActionListener, ItemListener {

	private static final long serialVersionUID = 1L;
	private JPanel pc, ps, ps1;
	private JMenuBar mb;
	private JMenu mg, mh;
	private JMenuItem nw, sv, hlp, qt;
	private static JButton quitter;
	private JButton bsValide, bsChoix;
	private JComboBox<String> cNombre;
	private JComboBox<String> cquestion1, cchoix1;
	private String[] ListeChoix;
	private JLabel lreponse, lquestion;
	private JToggleButton tp[];
	private boolean b1, b2;
	private Partie p;

	private int nbrLigne, nbrColonne;
	private ArrayList<String> choix;

	private static HashMap<String, AtribueG> gamegenerateurPerso = new HashMap<>();
	private static HashMap<String, AtribueG> tquestion1Generateur = new HashMap<>();

	private ArrayList<String> question1 = new ArrayList<>();

	private ArrayList<String> difG = new ArrayList<>();
	private static HashMap<String, Personnage> tquestion1 = new HashMap<>();

	private static AtribueG ag[];
	private static JLabel lag[];
	private HashMap<String, ArrayList<String>> hQuestionGenerateur = new HashMap<>();

	private static AtribueG persoGenerateur;
	private static AtribueG persoGenerateur1;

	public GameGenerateur2perso() throws Exception {
		setTitle("Qui est-ce ?");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));
		Accueil.getJg().setPage(2);

		mb = new JMenuBar();
		setJMenuBar(mb);
		mb.setBackground(new Color(197, 202, 233));
		mb.setOpaque(true);

		mg = new JMenu("Game");
		mb.add(mg);
		mg.setFont(new Font("Montserrat", Font.BOLD, 14));
		mg.setForeground(Color.BLACK);

		nw = new JMenuItem("New");
		mg.add(nw);
		nw.setFont(new Font("Montserrat", NORMAL, 14));
		nw.setForeground(Color.BLACK);
		nw.addActionListener(this);

		sv = new JMenuItem("Save");
		mg.add(sv);
		sv.setFont(new Font("Montserrat", NORMAL, 14));
		sv.setForeground(Color.BLACK);
		sv.addActionListener(this);
		mg.addSeparator();

		qt = new JMenuItem("Quit");
		mg.add(qt);
		qt.setFont(new Font("Montserrat", NORMAL, 14));
		qt.setForeground(Color.BLACK);
		qt.addActionListener(this);

		mh = new JMenu("Help?");
		mb.add(mh);
		mh.setFont(new Font("Montserrat", Font.BOLD, 14));
		mh.setForeground(Color.BLACK);

		hlp = new JMenuItem("savoir +");
		mh.add(hlp);
		hlp.setFont(new Font("Montserrat", NORMAL, 14));
		hlp.setForeground(Color.BLACK);
		hlp.addActionListener(this);

		pc = new JPanel();
		this.add(pc, "Center");

		tp = new JToggleButton[20];

		nbrLigne = (int) Accueil.getJg().getLigne();
		nbrColonne = (int) Accueil.getJg().getColonne();

		ag = new AtribueG[20];
		lag = new JLabel[20];

		pc.setLayout(new GridLayout(nbrLigne, nbrColonne, 28, 4));
		pc.setBackground(new Color(232, 234, 246));

		persoGenerateur = Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver());

		persoGenerateur1 = Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver1());

		System.out.println(persoGenerateur.getNom());
		System.out.println(persoGenerateur1.getNom());
		if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {

				tp[i] = new JToggleButton(new ImageIcon(Accueil.getJg().getListAG().get(i).getFicher()));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(Accueil.getJg().getListAG().get(i).getNom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				if (!Accueil.getJg().getListAG().get(i).getEstCocher()) {
					gamegenerateurPerso.put(Accueil.getJg().getListAG().get(i).getNom(),
							Accueil.getJg().getListAG().get(i));
				}
				pc.add(tp[i]);
				lag[i] = new JLabel(new ImageIcon(getClass().getResource("croix.png")));
				lag[i].setLocation(tp[i].getLocation());
				tp[i].add(lag[i]);
				lag[i].setVisible(false);

				if (Accueil.getJg().getListAG().get(i).getEstCocher()) {
					tp[i].setSelected(true);
					lag[i].setVisible(true);
				}

			}
		}

		else {
			for (int i = 0; i < (nbrColonne * nbrLigne); i++) {

				ag[i] = Accueil.getJg().getListAG().get(i);
				tp[i] = new JToggleButton(new ImageIcon(ag[i].getFicher()));
				tp[i].setBackground(Color.white);
				tp[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
				tp[i].setToolTipText(ag[i].getNom());
				tp[i].setFocusPainted(false);
				tp[i].addActionListener(this);
				gamegenerateurPerso.put(ag[i].getNom(), ag[i]);
				pc.add(tp[i]);
				lag[i] = new JLabel(new ImageIcon(getClass().getResource("croix.png")));
				lag[i].setLocation(tp[i].getLocation());
				tp[i].add(lag[i]);
				lag[i].setVisible(false);

			}
		}

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));

		ps1 = new JPanel();
		ps.add(ps1);
		ps1.setPreferredSize(new Dimension(840, 100));
		ps1.setBackground(new Color(232, 234, 246));
		ps1.setBorder(BorderFactory.createRaisedBevelBorder());
		ps1.setLayout(null);

		quitter = new JButton();
		ps1.add(quitter);
		quitter.setVisible(false);
		quitter.addActionListener(this);

		cquestion1 = new JComboBox<String>();
		ps1.add(cquestion1);
		for (int i = 0; i < Accueil.getJg().getAttribut().length; i++) {
			cquestion1.addItem(Accueil.getJg().getAttribut()[i]);
		}
		cquestion1.setFont(new Font("Montserrat", NORMAL, 15));
		cquestion1.setBackground(Color.white);
		cquestion1.setForeground(Color.black);
		cquestion1.setBounds(200, 40, 100, 25);
		cquestion1.addItemListener(this);
		cquestion1.setVisible(true);

		cchoix1 = new JComboBox<String>();
		ps1.add(cchoix1);
		cchoix1.setBounds(330, 40, 100, 25);
		cchoix1.addActionListener(this);
		cchoix1.setVisible(true);
		cchoix1.setBackground(Color.white);
		cchoix1.setForeground(Color.black);

		String item = (String) cquestion1.getSelectedItem();
		ListeChoix = Accueil.getJg().getAttribut();
		choix = new ArrayList<String>();
		for (int i = 0; i < ListeChoix.length; i++) {
			for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
				if (ListeChoix[i].equals(item)) {
					if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
						choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
						cchoix1.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
					}
					hQuestionGenerateur.put(ListeChoix[i], choix);

				}
			}
		}

		bsValide = new JButton("Valider");
		ps1.add(bsValide);
		bsValide.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsValide.setBackground(new Color(121, 134, 203));
		bsValide.setForeground(Color.white);
		bsValide.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsValide.setToolTipText("Cliquez ici pour valider la/les question(s) choisie(s)");
		bsValide.setFocusPainted(false);
		bsValide.setBounds(732, 10, 100, 25);
		bsValide.addActionListener(this);
		bsValide.setVisible(true);

		bsChoix = new JButton("Valider choix");
		ps1.add(bsChoix);
		bsChoix.setFont(new Font("Montserrat", Font.BOLD, 15));
		bsChoix.setBackground(new Color(121, 134, 203));
		bsChoix.setForeground(Color.white);
		bsChoix.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bsChoix.setToolTipText("Cliquez ici pour valider votre choix de selection de personne");
		bsChoix.setFocusPainted(false);
		bsChoix.setBounds(693, 65, 140, 25);
		bsChoix.addActionListener(this);

		cNombre = new JComboBox<String>();
		ps1.add(cNombre);
		cNombre.setFont(new Font("Montserrat", NORMAL, 16));
		cNombre.setBackground(Color.white);
		cNombre.setForeground(Color.black);
		cNombre.setBounds(7, 40, 130, 20);
		cNombre.addItem("Au moins un");
		cNombre.addItem("Les deux");
		cNombre.setVisible(true);
		cNombre.addItemListener(this);

		lquestion = new JLabel("La reponse est: ");
		ps1.add(lquestion);
		lquestion.setFont(new Font("Montserrat", Font.BOLD, 14));
		lquestion.setBounds(480, 40, 230, 35);
		lquestion.setForeground(Color.black);
		lquestion.setVisible(false);

		lreponse = new JLabel();
		ps1.add(lreponse);
		lreponse.setFont(new Font("Montserrat", Font.BOLD, 15));
		lreponse.setBounds(613, 40, 230, 35);
		lreponse.setForeground(Color.black);
		lreponse.setVisible(false);

		p = new Partie();
		if (ChoixGen.getIsCreer()) {
			JsonGenerateur jg2 = new JsonGenerateur(Accueil.getJg().getTheme(), "personnages",
					Accueil.getJg().getLigne(), Accueil.getJg().getColonne(), Accueil.getJg().getPage(),
					Accueil.getJg().getnbrPerso(), Accueil.getJg().getPersoATrouver(),
					Accueil.getJg().getPersoATrouver1(), Accueil.getJg().getNiveau(), Accueil.getJg().getSave(),
					Accueil.getJg().getSaveSaisie(), Accueil.getJg().getAttribut(), Accueil.getJg().getListAG(),
					Accueil.getJg().getValeursPossible());

			p.addToList(new PartieObj(Accueil.getJg().getTheme(), Accueil.getJg().getSave()));
		} else if (ChoixGen.getIsChoix()) {
			Random obj = new Random();

			int num = obj.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne() - 1);
			persoGenerateur = Accueil.getJg().getListAG().get(num);
			try {
				Accueil.getJg().setPersoATrouver(num);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		addWindowListener((WindowListener) new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter la partie?",
						"Confirmation", JOptionPane.YES_NO_OPTION);

				if (confirmed == JOptionPane.YES_OPTION) {
					ChoixGen.setIsCreer(false);
					ChoixGen.setIsChoix(false);
					try {
						Accueil.getJg().setGame(Accueil.getJg().getTheme());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					System.exit(0);
				}
			}
		});

	}

	public int getNbrLigne() {
		return nbrLigne;
	}

	public void setNbrLigne(int nbrLigne) {
		this.nbrLigne = nbrLigne;
	}

	public int getNbrColonne() {
		return nbrColonne;
	}

	public void setNbrColonne(int nbrColonne) {
		this.nbrColonne = nbrColonne;
	}

	public static HashMap<String, AtribueG> getGamegenerateurPerso() {
		return gamegenerateurPerso;
	}

	public static void quitter() {
		quitter.doClick();
	}

	public AtribueG[] getAg() {
		return ag;
	}

	public static void setAg(AtribueG ar, int i) {
		ag[i] = ar;
	}

	public static HashMap<String, AtribueG> getTquestion1Generateur() {
		return tquestion1Generateur;
	}

	public static JLabel[] getLag() {
		return lag;
	}

	public static AtribueG getPersoGenerateur() {
		return persoGenerateur;
	}

	public static AtribueG getPersoGenerateur1() {
		return persoGenerateur1;
	}

	public static void main(String[] args) throws Exception {
		GameGenerateur gg = new GameGenerateur();
		gg.setVisible(true);
	}

	@SuppressWarnings({ "deprecation" })
	@Override
	public void actionPerformed(ActionEvent e) {
		for (int i = 0; i < nbrColonne * nbrLigne; i++) {

			if (e.getSource() == tp[i]) {

				if (tp[i].isSelected()) {
					lag[i].setVisible(true);
					lag[i].setLocation(tp[i].getLocation());

					if (Accueil.isClicked() || ChoixGen.getIsChoix()) {

						try {
							Accueil.getJg().getListAG().get(i).setEstCocher(true);
							gamegenerateurPerso.remove(Accueil.getJg().getListAG().get(i).getNom());
							Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}

					} else {
						ag[i].setEstCocher(true);
						gamegenerateurPerso.remove(ag[i].getNom());
						try {
							Accueil.getJg().updateToList(ag[i], i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}

				} else {
					lag[i].setVisible(false);

					if (Accueil.isClicked() || ChoixGen.getIsChoix()) {
						Accueil.getJg().getListAG().get(i).setEstCocher(false);
						gamegenerateurPerso.put(Accueil.getJg().getListAG().get(i).getNom(),
								Accueil.getJg().getListAG().get(i));
						try {
							Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}

					} else {
						ag[i].setEstCocher(false);
						gamegenerateurPerso.put(ag[i].getNom(), ag[i]);
						try {
							Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
							Accueil.getJg().setGame(Accueil.getJg().getTheme());
						} catch (Exception e1) {
							e1.printStackTrace();
						}

					}

				}

			}
		}

		if (e.getSource() == nw) {
			for (int i = 0; i < Accueil.getJg().getListAG().size(); i++) {
				Accueil.getJg().getListAG().get(i).setEstCocher(false);
				try {
					Accueil.getJg().updateToList(Accueil.getJg().getListAG().get(i), i);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				lag[i].setVisible(false);
			}
			gamegenerateurPerso.clear();
			tquestion1Generateur.clear();

			Random obj = new Random();
			Random obj1 = new Random();
			int num = obj.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne());
			int num1 = obj1.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne());
			persoGenerateur = Accueil.getJg().getListAG().get(num);
			persoGenerateur1 = Accueil.getJg().getListAG().get(num1);
			while (persoGenerateur.getNom().equals(persoGenerateur1.getNom())) {
				num1 = obj1.nextInt((int) Accueil.getJg().getColonne() * (int) Accueil.getJg().getLigne());
				persoGenerateur1 = Accueil.getJg().getListAG().get(num1);
			}

			try {
				Accueil.getJg().setPersoATrouver(num);
				Accueil.getJg().setPersoATrouver1(num1);
				Accueil.getJg().setGame(Accueil.getJg().getTheme());

			} catch (Exception e1) {
				e1.printStackTrace();
			}

			this.dispose();
			GameGenerateur2perso jeu = null;
			try {
				jeu = new GameGenerateur2perso();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			jeu.setVisible(true);
		}
		if (e.getSource() == sv) {
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous sauvgarder cette partie ?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) {

				try {
					Accueil.getJg().setSave(true);
					for (int i = 0; i < p.getListpartie().size(); i++) {
						if (p.getListpartie().get(i).getNom().equals(Accueil.getJg().getTheme())) {
							p.updateToList(new PartieObj(Accueil.getJg().getTheme(), true), i);
						}
					}
					Accueil.getJg().setGame(Accueil.getJg().getTheme());
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		}
		if (e.getSource() == hlp) {

			JOptionPane.showMessageDialog(this,
					"Le but du jeu :\nIdentifier le personnage choisis aleatoirement par l'ordinateur en procedant par elimination en posant une ou deux questions");

		}
		if (e.getSource() == quitter) {
			Niveau.setFacile(false);
			Niveau.setDifficile(false);
			Niveau.setIsTresFacile(false);
			Niveau.setIsDeuxPerso(false);
			Mode_Dev.setIsDev(false);
			Mode_Player.setIsPlayer(false);
			Mode_Player.setIsDevPlayer(false);
			ChoixDev.setIsGen(false);
			ChoixGen.setIsCreer(false);
			ChoixGen.setIsChoix(false);
			Accueil.setClicked(false);
			this.dispose();
			gamegenerateurPerso.clear();
			tquestion1Generateur.clear();

		}
		if (e.getSource() == qt) {

			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment quitter la partie?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == 0) {
				Niveau.setFacile(false);
				Niveau.setDifficile(false);
				Niveau.setIsTresFacile(false);
				Niveau.setIsDeuxPerso(false);
				Mode_Dev.setIsDev(false);
				Mode_Player.setIsPlayer(false);
				Mode_Player.setIsDevPlayer(false);
				ChoixDev.setIsGen(false);
				ChoixGen.setIsCreer(false);
				ChoixGen.setIsChoix(false);
				Accueil.setClicked(false);
				gamegenerateurPerso.clear();
				tquestion1Generateur.clear();

				this.dispose();
				Accueil a = null;
				Accueil.setClicked(false);
				try {
					a = new Accueil();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				a.show();
			}
		}

		if (e.getSource() == bsChoix) {
			if (gamegenerateurPerso.size() >= 2) {
				ValiderGenerateur2perso v = new ValiderGenerateur2perso();
				v.show();
			} else {
				JOptionPane.showMessageDialog(null,
						"Veuillez ne pas cocher tous les personnages, faut avoir au moins deux qui ne sont pas coches !");
			}
		}

		if (e.getSource() == bsValide) {
			tquestion1.clear();

			bsChoix.setVisible(true);
			question1.clear();

			lquestion.setVisible(true);
			lreponse.setVisible(true);
			for (int i = 0; i < ListeChoix.length; i++) {
				if ((((String) cquestion1.getSelectedItem()).equals(ListeChoix[i]))) {
					if (((String) cchoix1.getSelectedItem()).equals(persoGenerateur.getAtr()[i])) {

						b1 = true;
						for (AtribueG k : gamegenerateurPerso.values()) {
							if (!((k.getAtr()[i]).equals(((String) cchoix1.getSelectedItem())))) {
								question1.add(k.getNom());
							}

						}
					} else {
						b1 = false;
						for (AtribueG k : gamegenerateurPerso.values()) {
							if (((k.getAtr()[i]).equals(((String) cchoix1.getSelectedItem())))) {
								question1.add(k.getNom());
							}
						}

					}

				}

			}

			for (int i = 0; i < ListeChoix.length; i++) {
				if ((((String) cquestion1.getSelectedItem()).equals(ListeChoix[i]))) {
					if (((String) cchoix1.getSelectedItem()).equals(persoGenerateur1.getAtr()[i])) {

						b2 = true;
						for (AtribueG k : gamegenerateurPerso.values()) {
							if (!((k.getAtr()[i]).equals(((String) cchoix1.getSelectedItem())))) {
								question1.add(k.getNom());
							}

						}
					} else {
						b2 = false;
						for (AtribueG k : gamegenerateurPerso.values()) {
							if (((k.getAtr()[i]).equals(((String) cchoix1.getSelectedItem())))) {
								question1.add(k.getNom());
							}
						}

					}

				}

			}

			if (((String) cNombre.getSelectedItem()).equals("Au moins un")) {
				if (b1 || b2) {
					lreponse.setText("Vrai");
				} else {
					lreponse.setText("Faux");
				}
			}

			else if (((String) cNombre.getSelectedItem()).equals("Les deux")) {
				if (b1 && b2) {
					lreponse.setText("Vrai");
				} else {
					lreponse.setText("Faux");
				}
			}
		}

	}

	// }

	@Override
	public void itemStateChanged(ItemEvent e) {

		if ((e.getSource() == cquestion1 && cquestion1.isPopupVisible())) {

			cchoix1.removeAllItems();

			ListeChoix = Accueil.getJg().getAttribut();
			choix = new ArrayList<String>();
			choix.clear();

			for (int i = 0; i < ListeChoix.length; i++) {
				for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {

					hQuestionGenerateur.put(ListeChoix[i], choix);

				}
			}

			String item = (String) cquestion1.getSelectedItem();
			ListeChoix = Accueil.getJg().getAttribut();
			choix = new ArrayList<String>();
			for (int i = 0; i < ListeChoix.length; i++) {
				for (int j = 0; j < Accueil.getJg().getListAG().size(); j++) {
					if (ListeChoix[i].equals(item)) {
						if (!choix.contains(Accueil.getJg().getListAG().get(j).getAtr()[i])) {
							choix.add(Accueil.getJg().getListAG().get(j).getAtr()[i]);
							cchoix1.addItem(Accueil.getJg().getListAG().get(j).getAtr()[i]);
						}
						hQuestionGenerateur.put(ListeChoix[i], choix);

					}
				}

			}
		}

	}
}