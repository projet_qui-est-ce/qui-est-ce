import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ChoixSave extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pn, pc, ps;
	private JLabel ln, ls;
	private JButton bCSaisie, bCPartie, bContinuer, bretour;
	private static boolean IsSaisie, IsPartie,IsCont;
	private JList<String> ListeSave;
	private Partie p;
	private JScrollPane b2;
	private DefaultListModel<String> model;

	public ChoixSave() throws Exception {

		setTitle("Continuer");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		pn = new JPanel();
		this.add(pn, "North");
		ln = new JLabel(new ImageIcon(getClass().getResource("quiRb.png")));
		pn.add(ln);
		pn.setBackground(new Color(197, 202, 233));

		pc = new JPanel();
		this.add(pc, "Center");
		pc.setLayout(null);
		pc.setBackground(new Color(232, 234, 246));

		bCSaisie = new JButton("Continuer la saisie");
		bCSaisie.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bCSaisie);
		bCSaisie.setBackground(new Color(121, 134, 203));
		bCSaisie.setForeground(Color.white);
		bCSaisie.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bCSaisie.setToolTipText("Continuer la saisie des personnages");
		bCSaisie.setBounds(300, 110, 288, 40);
		bCSaisie.setFocusPainted(false);
		bCSaisie.addActionListener(this);

		bCPartie = new JButton("Continuer une partie");
		bCPartie.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bCPartie);
		bCPartie.setBackground(new Color(121, 134, 203));
		bCPartie.setForeground(Color.white);
		bCPartie.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bCPartie.setToolTipText("Continuer une partie deja enregistrer");
		bCPartie.setBounds(300, 170, 288, 40);
		bCPartie.setFocusPainted(false);
		bCPartie.addActionListener(this);

		bContinuer = new JButton("Continuer >>");
		bContinuer.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bContinuer);
		bContinuer.setBackground(new Color(121, 134, 203));
		bContinuer.setForeground(Color.white);
		bContinuer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bContinuer.setToolTipText("Continuer la partie selectionnee");
		bContinuer.setBounds(465, 370, 170, 30);
		bContinuer.setFocusPainted(false);
		bContinuer.setVisible(false);
		bContinuer.addActionListener(this);

		bretour = new JButton("Retour");
		bretour.setFont(new Font("Montserrat", Font.BOLD, 16));
		pc.add(bretour);
		bretour.setBackground(new Color(121, 134, 203));
		bretour.setForeground(Color.white);
		bretour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bretour.setToolTipText("Retour a l'accueil");
		bretour.setBounds(300, 230, 288, 40);
		bretour.setFocusPainted(false);
		bretour.addActionListener(this);

		p = new Partie();
		if (Accueil.getAp().getSave()) {
			boolean trouve = false;
			for (int i = 0; i < p.getListpartie().size(); i++) {
				if (p.getListpartie().get(i).getNom().equals("Partie_de_base")) {
					try {
						trouve = true;
						p.updateToList(new PartieObj("Partie_de_base", true), i);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					break;
				}
			}
			if (!trouve) {
				p.addToList(new PartieObj("Partie_de_base", true));
			}
		}

		model = new DefaultListModel<>();
		for (int i = 0; i < p.getListpartie().size(); i++) {
			if (p.getListpartie().get(i).isSave()) {
				model.addElement(p.getListpartie().get(i).getNom());
			}
		}

		ListeSave = new JList<>(model);
		b2 = new JScrollPane(ListeSave);
		b2.setVisible(false);
		b2.setBounds(255, 25, 380, 330);
		pc.add(b2);

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));
		ps.setLayout(new FlowLayout(FlowLayout.CENTER));

		ls = new JLabel("@2022");
		ps.add(ls);
		ls.setFont(new Font("Montserrat", Font.BOLD, 12));
		ls.setForeground(Color.white);

	}

	public static void setIsSaisie(boolean b) {
		IsSaisie = b;
	}

	public static boolean getIsSaisie() {
		return IsSaisie;
	}

	public static void setIsPartie(boolean b) {
		IsPartie = b;
	}

	public static boolean getIsPartie() {
		return IsPartie;
	}

	public static boolean isIsCont() {
		return IsCont;
	}

	public static void setIsCont(boolean isCont) {
		IsCont = isCont;
	}

	public static void main(String[] args) throws Exception {
		ChoixSave cs = new ChoixSave();
		cs.setVisible(true);
	}

	@SuppressWarnings({ "deprecation" })
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == bCSaisie) {

			if (!Accueil.getJg().getSaveSaisie()) {
				JOptionPane.showMessageDialog(null, "Aucune saisie sauvgardee !");
			} else {
				IsSaisie = true;
				IsPartie = false;
				IsCont=true;
				ChoixDev.setIsGen(true);
				this.dispose();
				PersonnageFormTest g = null;
				try {
					g = new PersonnageFormTest();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				g.show();
			}
		}

		if (e.getSource() == bCPartie) {

			if (model.size() == 0) {
				JOptionPane.showMessageDialog(null, "Aucune partie sauvgardee !");
			} else {

				IsPartie = true;
				IsSaisie = false;

				bretour.setText("<< Retour");
				bretour.setToolTipText("Retour");
				bretour.setBounds(255, 370, 130, 30);

				bContinuer.setVisible(true);
				bCPartie.setVisible(false);
				bCSaisie.setVisible(false);
				b2.setVisible(true);
			}
		}

		if (e.getSource() == bretour) {
			if (bContinuer.isVisible()) {
				bretour.setText("Retour");
				bretour.setToolTipText("Retour a l'accueil");
				bretour.setBounds(300, 230, 288, 40);

				bContinuer.setVisible(false);
				bCPartie.setVisible(true);
				bCSaisie.setVisible(true);
				b2.setVisible(false);
			}

			else {

				IsPartie = false;
				IsSaisie = false;
				this.dispose();
				Accueil a = null;
				try {
					a = new Accueil();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				a.show();
			}
		}

		if (e.getSource() == bContinuer) {
			if (ListeSave.isSelectionEmpty()) {
				JOptionPane.showMessageDialog(null, "Veuillez selectionner d'abord une partie pour continuer !");
			} else {

				if (ListeSave.getSelectedValue().equals("Partie_de_base")) {
					Accueil.setClicked(true);

					switch ((int) Accueil.getAp().getPage()) {
					case 1:
						try {
							this.dispose();
							Game g = new Game();
							g.show();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						break;

					case 2:
						try {
							this.dispose();
							Game2 g = new Game2();
							g.show();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						break;

					case 3:
						try {
							this.dispose();
							GameDeuxPerso g = new GameDeuxPerso();
							g.show();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						break;

					}
				} else {
					Accueil.setClicked(true);
					for (int i = 0; i < p.getListpartie().size(); i++) {

						if (p.getListpartie().get(i).getNom().equals(ListeSave.getSelectedValue())) {

							try {
								Accueil.getJg().getGame(p.getListpartie().get(i).getNom());
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
					}
					try {
						switch ((int) Accueil.getJg().getPage()) {
						case 1:

							GameGenerateur g = new GameGenerateur();
							dispose();
							g.show();
							break;

						case 2:
									GameGenerateur2perso g2 = new GameGenerateur2perso();
									dispose();
									g2.show();
									break;
						}

					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}

	}

}