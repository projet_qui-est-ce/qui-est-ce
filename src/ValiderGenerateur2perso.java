import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;

import javax.swing.*;

public class ValiderGenerateur2perso extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HashMap<String, AtribueG> hgenerateur = new HashMap<>();

	private JPanel pc, ps, ps1;
	private JLabel lreponse, lperso, lphoto, lphoto2, lperso2;
	private JButton p[], bcancel, bquitter, bquitter2, bContinuer;
	private AtribueG ag[], perso;
	private JMenuBar mb;
	private JMenu mg, mh;
	private JMenuItem qt, hlp;
	private int i = 0, grid, nbrLigne = 0, nbrColonne = 0;
	private String reponse;

	public ValiderGenerateur2perso() {
		setTitle("Valider choix");
		setSize(900, 600);
		setLocationRelativeTo(this);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images.png")));

		mb = new JMenuBar();
		setJMenuBar(mb);
		mb.setBackground(new Color(197, 202, 233));
		mb.setOpaque(true);
		mb.setOpaque(true);

		mg = new JMenu("Game");
		mb.add(mg);
		mg.setFont(new Font("Montserrat", Font.BOLD, 14));
		mg.setForeground(Color.BLACK);

		qt = new JMenuItem("Quit");
		mg.add(qt);
		qt.setFont(new Font("Montserrat", NORMAL, 14));
		qt.setForeground(Color.BLACK);
		qt.addActionListener(this);

		mh = new JMenu("Help?");
		mb.add(mh);
		mh.setFont(new Font("Montserrat", Font.BOLD, 14));
		mh.setForeground(Color.BLACK);

		hlp = new JMenuItem("savoir +");
		mh.add(hlp);
		hlp.setFont(new Font("Montserrat", NORMAL, 14));
		hlp.setForeground(Color.BLACK);
		hlp.addActionListener(this);

		hgenerateur = GameGenerateur2perso.getGamegenerateurPerso();

		pc = new JPanel();
		add(pc, "Center");

		grid = hgenerateur.size();

		switch (grid) {
		case 1:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 2:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 3:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 4:
			nbrColonne = 2;
			nbrLigne = 2;
			break;
		case 5:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 6:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 7:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 8:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 9:
			nbrColonne = 3;
			nbrLigne = 3;
			break;
		case 10:
			nbrColonne = 4;
			nbrLigne = 3;
			break;
		case 11:
			nbrColonne = 4;
			nbrLigne = 3;
			break;
		case 12:
			nbrColonne = 4;
			nbrLigne = 3;
			break;
		case 13:
			nbrColonne = 3;
			nbrLigne = 5;
			break;
		case 14:
			nbrColonne = 3;
			nbrLigne = 5;
			break;
		case 15:
			nbrColonne = 3;
			nbrLigne = 5;
			break;
		case 16:
			nbrColonne = 4;
			nbrLigne = 4;
			break;
		case 17:
			nbrColonne = 4;
			nbrLigne = 5;
			break;
		case 18:
			nbrColonne = 4;
			nbrLigne = 5;
			break;
		case 19:
			nbrColonne = 4;
			nbrLigne = 5;
			break;
		case 20:
			nbrColonne = 4;
			nbrLigne = 5;
			break;

		}

		pc.setLayout(new GridLayout(nbrColonne, nbrLigne, 28, 4));
		pc.setBackground(new Color(197, 202, 233));

		p = new JButton[20];
		ag = new AtribueG[20];

		for (String e : hgenerateur.keySet()) {

			p[i] = new JButton(new ImageIcon(hgenerateur.get(e).getFicher()));
			p[i].setBackground(Color.white);
			p[i].setFocusPainted(false);
			p[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
			p[i].setToolTipText(e);
			ag[i] = hgenerateur.get(e);
			pc.add(p[i]);
			p[i].addActionListener(this);
			i++;
		}

		ps = new JPanel();
		this.add(ps, "South");
		ps.setBackground(new Color(197, 202, 233));

		ps1 = new JPanel();
		ps.add(ps1);
		ps1.setPreferredSize(new Dimension(840, 150));
		ps1.setBackground(new Color(232, 234, 246));
		ps1.setBorder(BorderFactory.createRaisedBevelBorder());
		ps1.setLayout(null);

		lreponse = new JLabel();
		lreponse.setBounds(258, 10, 450, 25);
		ps1.add(lreponse);

		lperso = new JLabel("Le personnage a deviner est :");
		lperso.setFont(new Font("Montserrat", NORMAL, 15));
		lperso.setVisible(false);
		lperso.setBounds(10, 90, 230, 25);
		ps1.add(lperso);

		lperso2 = new JLabel("Les personnages a deviner sont :");
		lperso2.setFont(new Font("Montserrat", NORMAL, 15));
		lperso2.setVisible(false);
		lperso2.setBounds(10, 90, 270, 25);
		ps1.add(lperso2);

		lphoto = new JLabel(
				new ImageIcon((Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver()).getFicher())));
		lphoto.setVisible(false);
		lphoto.setBounds(240, 57, 137, 85);
		ps1.add(lphoto);

		lphoto2 = new JLabel(new ImageIcon(
				(Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver1()).getFicher())));
		lphoto2.setVisible(false);
		lphoto2.setBounds(410, 57, 137, 85);
		ps1.add(lphoto2);

		bcancel = new JButton("Annuler");
		bcancel.setFont(new Font("Montserrat", Font.BOLD, 15));
		bcancel.setBackground(new Color(121, 134, 203));
		bcancel.setForeground(Color.white);
		bcancel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bcancel.setToolTipText("Retour a la partie");
		bcancel.setFocusPainted(false);
		bcancel.setBounds(695, 10, 137, 25);
		bcancel.addActionListener(this);
		ps1.add(bcancel);

		bquitter = new JButton("Quitter", new ImageIcon(getClass().getResource("Exit.png")));
		bquitter.setFont(new Font("Montserrat", Font.BOLD, 15));
		bquitter.setBackground(new Color(121, 134, 203));
		bquitter.setForeground(Color.white);
		bquitter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bquitter.setToolTipText("Quitter la partie");
		bquitter.setFocusPainted(false);
		bquitter.setBounds(695, 110, 137, 25);
		bquitter.addActionListener(this);
		ps1.add(bquitter);

		bquitter2 = new JButton("Quitter", new ImageIcon(getClass().getResource("Exit.png")));
		bquitter2.setFont(new Font("Montserrat", Font.BOLD, 15));
		bquitter2.setBackground(new Color(121, 134, 203));
		bquitter2.setForeground(Color.white);
		bquitter2.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bquitter2.setToolTipText("Nouvelle partie/Quitter la partie");
		bquitter2.setFocusPainted(false);
		bquitter2.setBounds(695, 110, 137, 25);
		bquitter2.setVisible(false);
		bquitter2.addActionListener(this);
		ps1.add(bquitter2);

		bContinuer = new JButton("Contiuer");
		bContinuer.setFont(new Font("Montserrat", Font.BOLD, 15));
		bContinuer.setBackground(new Color(121, 134, 203));
		bContinuer.setForeground(Color.white);
		bContinuer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		bContinuer.setToolTipText("Trouver le second personnage");
		bContinuer.setFocusPainted(false);
		bContinuer.setBounds(695, 110, 137, 25);
		bContinuer.setVisible(false);
		bContinuer.addActionListener(this);
		ps1.add(bContinuer);
		
		addWindowListener((WindowListener) new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

					try {
						Accueil.getJg().setSaveSaisie(false);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						Accueil.getJg().setGame(Accueil.getJg().getTheme());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					GameGenerateur2perso.quitter();
					System.exit(0);
				}
		});

	}

	public static void main(String[] args) {
		ValiderGenerateur v = new ValiderGenerateur();
		v.setVisible(true);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == hlp) {
			JOptionPane.showMessageDialog(this, "Cliquer sur une photo pour le resultat final ");
		}
		if (e.getSource() == (bquitter) || e.getSource() == (qt)) {
			int option = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment quitter la partie?", "Confirmation",
					JOptionPane.YES_NO_OPTION);
			if (option == 0) {
				GameGenerateur2perso.getGamegenerateurPerso().clear();
				GameGenerateur2perso.quitter();
				Niveau.setFacile(false);
				Niveau.setDifficile(false);
				Niveau.setIsTresFacile(false);
				Niveau.setIsDeuxPerso(false);
				Mode_Dev.setIsDev(false);
				Mode_Player.setIsPlayer(false);
				Mode_Player.setIsDevPlayer(false);
				ChoixDev.setIsGen(false);
				ChoixGen.setIsCreer(false);
				ChoixGen.setIsChoix(false);
				Accueil.setClicked(false);
				this.dispose();
				Accueil a;
				try {
					a = new Accueil();
					a.show();
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		}
		if (e.getSource() == bquitter2) {
			String[] options = { "Nouvelle Partie", "Quitter" };

			int x = JOptionPane.showOptionDialog(null, reponse, "Qui est-ce ?", JOptionPane.DEFAULT_OPTION,
					JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

			if (x == 0) {
				this.dispose();
				GameGenerateur2perso.getGamegenerateurPerso().clear();
				GameGenerateur2perso.getTquestion1Generateur().clear();

				GameGenerateur2perso.quitter();
				GameGenerateur2perso g = null;
				try {
					g = new GameGenerateur2perso();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				g.show();
			} else {
				this.dispose();
				GameGenerateur2perso.getGamegenerateurPerso().clear();
				GameGenerateur2perso.getTquestion1Generateur().clear();

				GameGenerateur2perso.quitter();
				Niveau.setFacile(false);
				Niveau.setDifficile(false);
				Niveau.setIsTresFacile(false);
				Niveau.setIsDeuxPerso(false);
				Mode_Dev.setIsDev(false);
				Mode_Player.setIsPlayer(false);
				Mode_Player.setIsDevPlayer(false);
				ChoixDev.setIsGen(false);
				ChoixGen.setIsCreer(false);
				ChoixGen.setIsChoix(false);
				Accueil.setClicked(false);
				Accueil a;
				try {
					a = new Accueil();
					a.show();
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		}

		if (e.getSource() == bcancel) {
			this.dispose();
		}

		if (e.getSource() == bContinuer) {
			ps1.setBackground(new Color(215, 215, 215));
			bContinuer.setEnabled(false);

			for (int j = 0; j < hgenerateur.size(); j++) {

				if (p[j].isSelected() == false) {
					p[j].setEnabled(true);
				}

			}

		}

		for (int i = 0; i < hgenerateur.size(); i++) {

			if (bContinuer.isVisible() == false) {
				if (e.getSource() == p[i]) {
					Partie pt = null;
					try {
						pt = new Partie();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						Accueil.getJg().setSave(false);
						for (int j = 0; j < pt.getListpartie().size(); j++) {
							if(pt.getListpartie().get(i).getNom().equals(Accueil.getJg().getTheme())) {
								pt.updateToList(new PartieObj(Accueil.getJg().getTheme(), false), i);
								break;
							}
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					bcancel.setVisible(false);
					bquitter.setVisible(false);
					bquitter2.setVisible(true);

					if ((ag[i].getNom().equals(
							(Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver()).getNom())))
							|| (ag[i].getNom().equals((Accueil.getJg().getListAG()
									.get((int) Accueil.getJg().getPersoATrouver1()).getNom())))) {
						p[i].setSelected(true);
						bquitter2.setVisible(false);
						bContinuer.setVisible(true);
						reponse = "Felicitations ! vous avez trouve le 1er personnage ";
						lreponse.setText(reponse);
						lreponse.setFont(new Font("Montserrat", Font.BOLD, 16));
						ps1.setBackground(new Color(218, 248, 188));
						lperso.setVisible(true);
						if (ag[i].getNom().equals(
								(Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver()).getNom()))) {
							perso = GameGenerateur2perso.getPersoGenerateur();
							lphoto.setVisible(true);
						} else {
							lphoto2.setVisible(true);
							perso = GameGenerateur2perso.getPersoGenerateur1();
						}

						for (int j = 0; j < hgenerateur.size(); j++) {
							p[j].setEnabled(false);

						}
					} else {

						reponse = "Game over !";
						lreponse.setText(reponse);
						lreponse.setFont(new Font("Montserrat", Font.BOLD, 16));
						ps1.setBackground(new Color(255, 166, 166));
						lperso.setVisible(false);
						lperso2.setVisible(true);
						lphoto.setVisible(true);
						lphoto2.setVisible(true);

						for (int j = 0; j < hgenerateur.size(); j++) {
							p[j].setEnabled(false);
							p[j].setBackground(new Color(215, 215, 215));
						}

					}
				}
			} else if (bContinuer.isVisible() == true) {
				if (e.getSource() == p[i]) {
					try {
						Accueil.getJg().setSave(false);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					bcancel.setVisible(false);
					bquitter.setVisible(false);
					bquitter2.setVisible(true);
					bContinuer.setVisible(false);
					if ((ag[i].getNom().equals(
							(Accueil.getJg().getListAG().get((int) Accueil.getJg().getPersoATrouver()).getNom())))
							|| (ag[i].getNom().equals((Accueil.getJg().getListAG()
									.get((int) Accueil.getJg().getPersoATrouver1()).getNom())))) {

						reponse = "Felicitations ! vous avez gagne";
						lreponse.setText(reponse);
						lreponse.setFont(new Font("Montserrat", Font.BOLD, 16));
						ps1.setBackground(new Color(218, 248, 188));
						lperso.setVisible(false);
						lperso2.setVisible(true);
						lphoto.setVisible(true);
						lphoto2.setVisible(true);
						for (int j = 0; j < hgenerateur.size(); j++) {
							p[j].setEnabled(false);
							p[j].setBackground(new Color(215, 215, 215));
						}
					} else {

						reponse = "Game over !";
						lreponse.setText(reponse);
						lreponse.setFont(new Font("Montserrat", Font.BOLD, 16));
						ps1.setBackground(new Color(255, 166, 166));
						lperso.setVisible(false);
						lperso2.setVisible(true);
						lphoto.setVisible(true);
						lphoto2.setVisible(true);

						for (int j = 0; j < hgenerateur.size(); j++) {
							p[j].setEnabled(false);
							p[j].setBackground(new Color(215, 215, 215));
						}

					}
				}

			}

		}

	}

}
