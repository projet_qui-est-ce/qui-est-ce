# Qui est-ce ?

![](src/Logo-QECAnimé1.gif)

## **Description du projet**

Dans le cadre de l’unité d’enseignement (UE) projet de programmation, nous avions à réaliser un projet de développement de jeu  avec les outils de programmation informatique et les notions vues en cours. Nous avons reprie le Jeu de société « Qui est-ce ?», dont le but du jeu est de se montrer rusé en posant des questions qui se répondent que par oui ou non pour découvrir le personnage mystère sélectionné.

## **Technologies utilisées**

Le langage de programmation utilisé pour développer cette application est le langage de programmation
orienté objet JAVA (Full-stack), avec du JavaScript Object Notation (JSON) qui est un format de données textuelles dérivé de la notation
des objets du langage JavaScript. Il permet de représenter l’information structurée comme le permet XML.

D’autre part, nous avons utilisé les bibliothèques suivantes :
_- javax.swing.*
- java.awt.*
- java.o.*
- java.util.*
- java.nio.*
- json-simple-1.1.1.jar pour l’interaction avec les différents fichiers json._


## **Lancement du jeu**

1. **Installation**

    - Clonage du projet:
<code><pre>https://gitlab.com/projet_qui-est-ce/qui-est-ce.git</code></pre>

    - Emplacement sur le dossier src & lancememnt des deux commandes suivantes:
<code><pre>javac -cp json-simple-1.1.1.jar *.java</code></pre>
<code><pre>java -cp json-simple-1.1.1.jar:. Accueil</code></pre>

**Et BOOM 💥💥 le jeu est lancé**
 
2. **Fonctionnalités & fonctionnement de l’application :**  

    - L’utilisateur s’offre à lui plusieurs fonctionnalités :
        - Un mode Player comporte le concept de base du jeu (qui-est-ce) qui permet d’identifier un personnage qui
        est choisi aléatoirement par l’ordinateur, parmi 20 personnages au tour d’un thème de « dessin animés
        », un thème qui touche toutes catégories d’utilisateurs.
        - Un mode développeur ou l’utilisateur pourra changer le nombre de personnage et continuer avec la branche
        du  thème du jeu de base, ou bien changer de thème et cela en trois façons :

            1. Choisir un thème parmi une liste qui contient des anciens thèmes créés par l’utilisateur.
            2. Importer un fichier json depuis son ordinateur.
            3. Créer son propre fichier json.
        - Pour chaque partie, l’utilisateur à la possibilité de :

            - Choisir d’identifier un ou deux personnages.
            - Choisir un niveau de difficulté parmi : très facile-facile-difficile.
            - Très facile : l’ordinateur cochera automatiquement les personnages après la validation de la
            question en donnant le nombre de case qui ont été cochées ainsi qu’une réponse booléenne.
            - Facile : l’ordinateur donnera après la validation de la question, le nombre de personnage qu’il
            faut cocher ainsi qu’une réponse booléenne.
            - Difficile : l’ordinateur donnera uniquement une réponse booléenne après la validation de la ques￾tion.
            - Sauvegarder une partie et la retrouver parmi la liste des parties non achevées.
            - Interruption de la saisie et reprendre cette dernière ultérieurement.

        **- C'est bien détaillé dans le Rapport (PDF) présent dans ce repo git (jetez un coup d'œil 🧐)**
<br>



## **Auteurs**

- Saddek OUYAHIA  ([saddek.ouyahia](https://gitlab.com/saddek.ouyahia)) : Responsable du développement initial
- Melissa OUADA  ([melissa.ouada](https://gitlab.com/melissa.ouada)) : Responsable du développement initial
- Fadia ALLANI  ([FadiaA](https://gitlab.com/FadiaA)) : Responsable du développement initial
- Ranya KARMANI  : Responsable du développement initial
